﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using DataAccess.Core.Extensions;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Nh.Types;
using NHibernate;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// Represents a basic implementation for query descriptors.
    /// </summary>
    /// <seealso cref="QueryDescriptor" />
    [DebuggerDisplay("name: {this.Name}")]
    public class NhQueryDescriptor
        : QueryDescriptor
    {
        private readonly IQuery query;

        /// <summary>
        /// Initializes a new instance of the <see cref="NhQueryDescriptor"/> class.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="name">The name.</param>
        /// <param name="queryArguments">The parameters.</param>
        protected NhQueryDescriptor(IQuery query, string name, IEnumerable<IQueryArgument> queryArguments)
            : base(name, query.QueryString, queryArguments ?? query.NamedParameters.Select(n => QueryArgument.MakeInstance(n)))
        {
            this.query = query;
        }

        /// <inheritdoc/>
        public override IQueryDescriptor SetParameter(IQueryArgument parameter, object value)
        {
            if (value == null)
            {
                this.query.SetCustomParameter(parameter.Name, DBNull.Value);
            }
            else
            {
                var dataTable = value.AsDataTable();

                if (dataTable != null)
                {
                    parameter.TypeName.ThrowException(string.IsNullOrWhiteSpace, s => new DataAccess.Core.Exceptions.QueryParameterException("The current parameter is a complex data, so the type name of this instance cannot be empty or null.", parameter.Name));

                    this.query.SetCustomParameter(parameter.Name, dataTable, new SqlTypeDescriptor { TypeName = parameter.TypeName, SqlDbType = SqlDbType.Structured });
                }
                else
                {
                    this.query.SetParameter(parameter.Name, value);
                }
            }

            return this;
        }
    }
}
