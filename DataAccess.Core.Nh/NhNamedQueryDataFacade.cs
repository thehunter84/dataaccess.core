﻿using System.Transactions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using NHibernate;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// Manage named queries registered into ISessionFactory instance.
    /// </summary>
    /// <seealso cref="IQueryDataFacade" />
    public class NhNamedQueryDataFacade
        : IQueryDataFacade
    {
        private readonly ITransactionProvider<ISession> transactionProvider;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="NhNamedQueryDataFacade"/> class.
        /// </summary>
        /// <param name="transactionProvider">The transaction provider.</param>
        public NhNamedQueryDataFacade(ITransactionProvider<ISession> transactionProvider)
        {
            this.transactionProvider = transactionProvider;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(string name, IsolationLevel? isolation = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(new TransactionDescriptor { Name = name, Isolation = isolation });
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(TransactionDescriptor descriptor = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(descriptor);
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public IQueryCommand GetQueryCommand(string namedQuery)
        {
            var query = this.transactionProvider.ContextProvider.GetCurrentContext().GetNamedQuery(namedQuery);

            return new NhQueryCommand(query, namedQuery);
        }

        /// <inheritdoc/>
        public IQueryMaterializer GetQueryMaterializer(string namedQuery)
        {
            var session = this.transactionProvider.ContextProvider.GetCurrentContext();
            var query = session.GetNamedQuery(namedQuery);

            return new NhQueryMaterializer(query, namedQuery, type => session.SessionFactory.GetClassMetadata(type));
        }
    }
}
