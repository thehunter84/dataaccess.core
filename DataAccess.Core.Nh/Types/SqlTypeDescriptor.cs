﻿using System.Data;

namespace DataAccess.Core.Nh.Types
{
    /// <summary>
    /// 
    /// </summary>
    public class SqlTypeDescriptor
    {
        /// <summary>
        /// 
        /// </summary>
        public SqlTypeDescriptor()
        {
            this.ColumnSpan = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SqlDbType? SqlDbType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ColumnSpan { get; set; }
    }
}
