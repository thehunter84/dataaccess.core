﻿#if NETSTANDARD2_0
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.Type;
#else
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.Type;
#endif

namespace DataAccess.Core.Nh.Types
{
    /// <summary>
    /// Represents a custom Sql Type for injecting custom values into Sql Statements.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class CustomSqlType<TValue> : AbstractType
    {
        private readonly SqlTypeDescriptor sqlTypeDescriptor;
        private readonly SqlType[] types = { new SqlType(DbType.Object) };

        /// <summary>
        /// Creates a new <see cref="CustomSqlType{TValue}"/> instance.
        /// </summary>
        /// <param name="sqlTypeDescriptor"></param>
        public CustomSqlType(SqlTypeDescriptor sqlTypeDescriptor = null)
        {
            this.sqlTypeDescriptor = sqlTypeDescriptor ?? new SqlTypeDescriptor();
            this.Name = this.GetType().Name;
            this.ReturnedClass = null;
            this.IsMutable = false;
        }

        /// <inheritdoc />
        public override string Name { get; }

        /// <inheritdoc />
        public override Type ReturnedClass { get; }

        /// <inheritdoc />
        public override bool IsMutable { get; }
        
        /// <inheritdoc />
        public override string ToLoggableString(object value, ISessionFactoryImplementor factory)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override bool IsDirty(object old, object current, bool[] checkable, ISessionImplementor session)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override bool[] ToColumnNullness(object value, IMapping mapping)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override object Replace(object original, object current, ISessionImplementor session, object owner, IDictionary copiedAlready)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override SqlType[] SqlTypes(IMapping mapping)
        {
            return types;
        }

        /// <inheritdoc />
        public override int GetColumnSpan(IMapping mapping)
        {
            return this.sqlTypeDescriptor.ColumnSpan;
        }

#if NETSTANDARD2_0
        /// <inheritdoc />
        public override void NullSafeSet(DbCommand st, object value, int index, ISessionImplementor session)
        {
            if (st is SqlCommand sqlCommand)
            {
                this.SetNullSafe(sqlCommand, value, index);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <inheritdoc />
        public override object DeepCopy(object val, ISessionFactoryImplementor factory)
        {
            throw new NotSupportedException();
        }
        
        /// <inheritdoc />
        public override object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override object NullSafeGet(DbDataReader rs, string name, ISessionImplementor session, object owner)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override void NullSafeSet(DbCommand st, object value, int index, bool[] settable, ISessionImplementor session)
        {
            throw new NotSupportedException();
        }
        
        /// <inheritdoc />
        public override Task<object> ReplaceAsync(object original, object current, ISessionImplementor session, object owner, IDictionary copiedAlready,
            CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override Task<object> NullSafeGetAsync(DbDataReader rs, string[] names, ISessionImplementor session, object owner,
            CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override Task<object> NullSafeGetAsync(DbDataReader rs, string name, ISessionImplementor session, object owner,
            CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override Task NullSafeSetAsync(DbCommand st, object value, int index, bool[] settable, ISessionImplementor session,
            CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override Task NullSafeSetAsync(DbCommand st, object value, int index, ISessionImplementor session,
            CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override Task<bool> IsDirtyAsync(object old, object current, bool[] checkable, ISessionImplementor session,
            CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }
#else
        /// <inheritdoc />
        public override object DeepCopy(object val, EntityMode entityMode, ISessionFactoryImplementor factory)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override void SetToXMLNode(XmlNode node, object value, ISessionFactoryImplementor factory)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override object FromXMLNode(XmlNode xml, IMapping factory)
        {
            throw new NotSupportedException();
        }
        
        /// <inheritdoc />
        public override object NullSafeGet(IDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override object NullSafeGet(IDataReader rs, string name, ISessionImplementor session, object owner)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override void NullSafeSet(IDbCommand st, object value, int index, bool[] settable, ISessionImplementor session)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override void NullSafeSet(IDbCommand st, object value, int index, ISessionImplementor session)
        {
            if (st is SqlCommand sqlCommand)
            {
                this.SetNullSafe(sqlCommand, value, index);
            }
            else
            {
                throw new NotSupportedException();
            }
        }
#endif
        private void SetNullSafe(SqlCommand sqlCommand, object value, int index)
        {
            var sqlParameter = sqlCommand.Parameters[index];

            sqlParameter.SqlDbType = this.sqlTypeDescriptor.SqlDbType.GetValueOrDefault(sqlParameter.SqlDbType);
            sqlParameter.TypeName = this.sqlTypeDescriptor.TypeName ?? sqlParameter.TypeName;

            if (typeof(TValue) != value.GetType())
            {
                throw new InvalidOperationException($"The current value for this custom type is invalid because It's incompatible, type value: {value.GetType()}, type declared: {typeof(TValue)}");
            }

            sqlParameter.Value = value;
        }
    }
}
