﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Xml;
using System.Xml.Serialization;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Nh.Types;
using NHibernate;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Core.Nh.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class CommonExtensions
    {
        private static Type ConformistMappingType => typeof(IConformistHoldersProvider);
        
        /// <summary>
        /// Parses the given query definition in IQuery instance.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        internal static IQuery AsIQuery(this QueryDefinition query, ISession session)
        {
            IQuery nhQuery;

            var qq = query.Statement.NormalizeNhStatement(query.Parameters);

            try
            {
                nhQuery = session.CreateQuery(qq);
            }
            catch (Exception)
            {
                nhQuery = session.CreateSQLQuery(qq);
            }

            return nhQuery;
        }

        /// <summary>
        /// Build <seealso cref="HbmMapping"/> instance from the given fluent mapping types.
        /// </summary>
        /// <returns></returns>
        internal static HbmMapping MakeHbmMapping(this IEnumerable<Type> fluentMappings)
        {
            var modelMapper = new ModelMapper();
            modelMapper.AddMappings(fluentMappings);
            
            return modelMapper.CompileMappingForAllExplicitlyAddedEntities();
        }

        /// <summary>
        /// Builds the internal HbmMapping into Xml format.
        /// </summary>
        /// <returns></returns>
        internal static string BuildHbm(this IEnumerable<Type> fluentMappings)
        {
            var mapping = fluentMappings.MakeHbmMapping();

            var xmlSerializer = new XmlSerializer(mapping.GetType());

            var builder = new StringBuilder();
            using (var writer = XmlWriter.Create(builder))
            {
                xmlSerializer.Serialize(writer, mapping);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Normalizes the SQL statement replacing the standard prefix ":" to "@" prefix.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        internal static string NormalizeNhStatement(this string statement, IEnumerable<IQueryArgument> parameters)
        {
            foreach (var arg in parameters)
            {
                statement = Regex.Replace(statement, $":{arg.Name}", $":{arg.Name}", RegexOptions.IgnoreCase);
            }

            return statement;
        }

        /// <summary>
        /// Synchronizes dirty objects in memory with data source.
        /// </summary>
        /// <param name="session"></param>
        internal static void FlushOnActiveTransaction(this ISession session)
        {
            var transaction = Transaction.Current;
            
            if (transaction != null && transaction.CanBeCompleted())
            {
                try
                {
                    session.Flush();
                }
                catch (Exception ex)
                {
                    var exception = new FlushFailedException($"Error when the current flush action was synchronized with data source.", nameof(FlushOnActiveTransaction), ex);
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Gets all conformist mappings from the current assembly.
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        internal static IEnumerable<Type> GetAllConformistMappings(this Assembly assembly, Func<Type, bool> predicate = null)
        {
            predicate = predicate ?? (type => true);
            
            var allTypes = assembly.GetTypes()
                .Where(type => !type.IsGenericTypeDefinition && ConformistMappingType.IsAssignableFrom(type) && predicate(type))
                .ToList();
            
            return allTypes;
        }

        /// <summary>
        /// Set a custom parameter which will be applied a <see cref="CustomSqlType{TValue}"/>.
        /// </summary>
        /// <typeparam name="TParameter"></typeparam>
        /// <param name="query"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="sqlTypeDescriptor"></param>
        /// <returns></returns>
        internal static IQuery SetCustomParameter<TParameter>(this IQuery query, string name, TParameter value, SqlTypeDescriptor sqlTypeDescriptor = null)
        {
            query.SetParameter(name, value, new CustomSqlType<TParameter>(sqlTypeDescriptor));

            return query;
        }
    }
}
