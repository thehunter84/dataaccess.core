﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;

namespace DataAccess.Core.Nh.Extensions
{
    /// <summary>
    /// Extension methods for <seealso cref="Configuration"/> instance.
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Adds the given hbm file paths into the given <seealso cref="Configuration"/> instance.
        /// </summary>
        /// <param name="configuration">The current configuration</param>
        /// <param name="hbmFiles">The hbm path files to add</param>
        /// <returns>The current configuration</returns>
        public static Configuration AddHbmFiles(this Configuration configuration, params string[] hbmFiles)
        {
            var fileInfos = hbmFiles.Select(s => new FileInfo(s)).ToArray();

            return configuration.AddHbmFiles(fileInfos);
        }

        /// <summary>
        /// Adds the given hbm files into the given <seealso cref="Configuration"/> instance.
        /// </summary>
        /// <param name="configuration">The current configuration</param>
        /// <param name="hbmFiles">The hbm files to add</param>
        /// <returns>The current configuration</returns>
        public static Configuration AddHbmFiles(this Configuration configuration, params FileInfo[] hbmFiles)
        {
            foreach (var fileInfo in hbmFiles)
            {
                configuration.AddFile(fileInfo);
            }

            return configuration;
        }

        /// <summary>
        /// Adds the given hbm directory paths into the given <seealso cref="Configuration"/> instance.
        /// </summary>
        /// <param name="configuration">The current configuration</param>
        /// <param name="hbmDirectories">The hbm directory path to add</param>
        /// <returns>The current configuration</returns>
        public static Configuration AddHbmDirectories(this Configuration configuration, params string[] hbmDirectories)
        {
            var directoryInfos = hbmDirectories.Select(s => new DirectoryInfo(s)).ToArray();

            return configuration.AddHbmDirectories(directoryInfos);
        }

        /// <summary>
        /// Adds the given hbm directories into the given <seealso cref="Configuration"/> instance.
        /// </summary>
        /// <param name="configuration">The current configuration</param>
        /// <param name="hbmDirectories">The hbm directories to add</param>
        /// <returns>The current configuration</returns>
        /// <returns></returns>
        public static Configuration AddHbmDirectories(this Configuration configuration, params DirectoryInfo[] hbmDirectories)
        {
            foreach (var directoryInfo in hbmDirectories)
            {
                configuration.AddDirectory(directoryInfo);
            }

            return configuration;
        }

        /// <summary>
        /// Adds the given fluent mappings into the given <seealso cref="Configuration"/> instance.
        /// </summary>
        /// <param name="configuration">The current configuration</param>
        /// <param name="mappingTypes">The fluent types to add</param>
        /// <returns>The current configuration</returns>
        public static Configuration AddFluentMappings(this Configuration configuration, params Type[] mappingTypes)
        {
            configuration.AddMapping(mappingTypes.MakeHbmMapping());

            return configuration;
        }

        /// <summary>
        /// Adds all fluent mappings defined into the given assembly.
        /// </summary>
        /// <param name="configuration">The current configuration.</param>
        /// <param name="assembly">The assembly which contains mappings for the current Configuration.</param>
        /// <param name="predicate">The given criteria to apply for filtering logic.</param>
        /// <returns>The current configuration</returns>
        public static Configuration AddFluentMappings(this Configuration configuration, Assembly assembly, Func<Type, bool> predicate = null)
        {
            var mappingTypes = assembly.GetAllConformistMappings(predicate);

            return configuration.AddFluentMappings(mappingTypes.ToArray());
        }

        /// <summary>
        /// Register all mappings defined by rules established by the given <see cref="ModelMapper"/> instance.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="modelMapper"></param>
        /// <returns>The current configuration</returns>
        public static Configuration UseModelMapper(this Configuration configuration, ModelMapper modelMapper)
        {
            var mapping = modelMapper.CompileMappingForAllExplicitlyAddedEntities();
            configuration.AddMapping(mapping);

            return configuration;
        }

        /// <summary>
        /// Register all mappings defined by rules established by the function returns a model mapper instance.
        /// </summary>
        /// <typeparam name="TModelMapper">The type of Model mapper</typeparam>
        /// <param name="configuration">The current configuration.</param>
        /// <param name="modelMapperFunc"></param>
        /// <returns></returns>
        public static Configuration UseModelMapper<TModelMapper>(this Configuration configuration, Func<TModelMapper> modelMapperFunc)
            where TModelMapper : ModelMapper
        {
            var mapping = modelMapperFunc().CompileMappingForAllExplicitlyAddedEntities();
            configuration.AddMapping(mapping);

            return configuration;
        }
    }
}
