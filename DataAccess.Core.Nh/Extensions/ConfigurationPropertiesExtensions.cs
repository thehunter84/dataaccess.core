﻿using System.Data;
using NHibernate;
using NHibernate.AdoNet;
using NHibernate.Bytecode;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Exceptions;
using NHibernate.Linq.Visitors;
using NHibernate.Transaction;

namespace DataAccess.Core.Nh.Extensions
{
    /// <summary>
    /// Extension methods for <seealso cref="Configuration"/> instance.
    /// </summary>
    public static class ConfigurationPropertiesExtensions
    {
        /// <summary>
        /// The name of the connection string (defined in &lt;connectionStrings&gt; configuration file element) to use to obtain the connection
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public static Configuration ConnectionName(this Configuration cfg, string connectionName)
        {
            return cfg.SetProperty(CfgProperties.ConnectionName, connectionName);
        }

        /// <summary>
        /// Connection string to use to obtain the connection.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static Configuration ConnectionString(this Configuration cfg, string connectionString)
        {
            return cfg.SetProperty(CfgProperties.ConnectionString, connectionString);
        }

        /// <summary>
        /// Automatically import reserved/keywords from the database when the <seealso cref="ISessionFactory"/> is created.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="ddlKeyWords">
        /// </param>
        /// <returns></returns>
        public static Configuration KeywordsAutoImport(this Configuration cfg, Hbm2DDLKeyWords ddlKeyWords)
        {
            return cfg.SetProperty(CfgProperties.Hbm2DdlKeywords, ddlKeyWords.ToString());
        }

        /// <summary>
        /// Write all SQL statements to console. Defaults to false.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="showSql"></param>
        /// <returns></returns>
        public static Configuration ShowSql(this Configuration cfg, bool showSql = true)
        {
            return cfg.SetProperty(CfgProperties.ShowSql, showSql.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// Log formatted SQL. Defaults to false.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="formatSql"></param>
        /// <returns></returns>
        public static Configuration FormatSql(this Configuration cfg, bool formatSql = true)
        {
            return cfg.SetProperty(CfgProperties.FormatSql, formatSql.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// The class name of a NHibernate Dialect, enables certain platform dependent features.
        /// </summary>
        /// <typeparam name="TDialect"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration Dialect<TDialect>(this Configuration cfg)
            where TDialect : Dialect
        {
            return cfg.SetProperty(CfgProperties.Dialect, typeof(TDialect).AssemblyQualifiedName);
        }

        /// <summary>
        /// The type of a custom <seealso cref="IConnectionProvider"/> implementation
        /// </summary>
        /// <typeparam name="TProvider"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration ConnectionProvider<TProvider>(this Configuration cfg)
            where TProvider : IConnectionProvider
        {
            return cfg.SetProperty(CfgProperties.ConnectionProvider, typeof(TProvider).AssemblyQualifiedName);
        }

        /// <summary>
        /// The type of a custom <seealso cref="IDriver"/>, if using <seealso cref="DriverConnectionProvider"/>
        /// <para>This is usually not needed, most of the time the dialect will take care of setting the <seealso cref="IDriver"/> using a sensible default.
        /// See the API documentation of the specific dialect for the defaults.</para>
        /// </summary>
        /// <typeparam name="TDriver"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration Driver<TDriver>(this Configuration cfg)
            where TDriver : IDriver
        {
            return cfg.SetProperty(CfgProperties.Driver, typeof(TDriver).AssemblyQualifiedName);
        }

        /// <summary>
        /// Set the ADO.NET transaction isolation level. Check System.Data.IsolationLevel for meaningful values and the database's documentation to ensure that level is supported.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="isolation"></param>
        /// <returns></returns>
        public static Configuration IsolationLevel(this Configuration cfg, IsolationLevel isolation)
        {
            return cfg.SetProperty(CfgProperties.Isolation, isolation.ToString());
        }

        /// <summary>
        /// Specify when NHibernate should release ADO.NET connections.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="releaseMode"></param>
        /// <returns></returns>
        public static Configuration ConnectionReleaseMode(this Configuration cfg, ConnectionReleaseMode releaseMode)
        {
            return cfg.SetProperty(CfgProperties.ConnectionReleaseMode, ConnectionReleaseModeParser.ToString(releaseMode));
        }

        /// <summary>
        /// The class name of a <seealso cref="IBatcherFactory"/> implementation.
        /// <para>This is usually not needed, most of the time the driver will take care of setting the <see cref="IBatcherFactory"/> using a sensible default according to the database capabilities.</para>
        /// <para>eg. classname.of.BatcherFactory, assembly</para>
        /// </summary>
        /// <typeparam name="TFactory"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration BatcherFactory<TFactory>(this Configuration cfg)
            where TFactory : IBatcherFactory
        {
            return cfg.SetProperty(CfgProperties.BatcherFactory, typeof(TFactory).AssemblyQualifiedName);
        }

        /// <summary>
        /// Specify the batch size to use when batching update statements. Setting this to 0 (the default) disables the functionality.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Configuration BatchSize(this Configuration cfg, short size = 0)
        {
            return cfg.SetProperty(CfgProperties.BatchSize, size.ToString());
        }

        /// <summary>
        /// Enable ordering of insert statements for the purpose of more efficient batching. Defaults to true if batching is enabled, false otherwise.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="orderInsert"></param>
        /// <returns></returns>
        public static Configuration OrderInserts(this Configuration cfg, bool orderInsert = false)
        {
            return cfg.SetProperty(CfgProperties.OrderInserts, orderInsert.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// Enable ordering of update statements for the purpose of more efficient batching. Defaults to true if batching is enabled, false otherwise.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="orderUpdate"></param>
        /// <returns></returns>
        public static Configuration OrderUpdates(this Configuration cfg, bool orderUpdate = false)
        {
            return cfg.SetProperty(CfgProperties.OrderUpdates, orderUpdate.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// The class name of a custom <seealso cref="ITransactionFactory"/> implementation. Defaults to the built-in AdoNetWithSystemTransactionFactory.
        /// </summary>
        /// <typeparam name="TFactory"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration TransactionFactory<TFactory>(this Configuration cfg)
            where TFactory : ITransactionFactory
        {
            return cfg.SetProperty(CfgProperties.TransactionFactory, typeof(TFactory).AssemblyQualifiedName);
        }

        /// <summary>
        /// Specify to prepare DbCommands generated by NHibernate. Defaults to false.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="prepare"></param>
        /// <returns></returns>
        public static Configuration PrepareSql(this Configuration cfg, bool prepare = true)
        {
            return cfg.SetProperty(CfgProperties.PrepareSql, prepare.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// Specify the default timeout in seconds of DbCommands generated by NHibernate. Negative values disable it.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static Configuration Timeout(this Configuration cfg, byte timeout)
        {
            return cfg.SetProperty(CfgProperties.CommandTimeout, timeout.ToString());
        }

        /// <summary>
        /// The class name of a custom <seealso cref="ISQLExceptionConverter"/> implementation. Defaults to <seealso cref="Dialect.BuildSQLExceptionConverter()"/>.
        /// </summary>
        /// <typeparam name="TExceptionConverter"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration SqlExceptionConverter<TExceptionConverter>(this Configuration cfg)
            where TExceptionConverter : ISQLExceptionConverter
        {
            return cfg.SetProperty(CfgProperties.SqlExceptionConverter, typeof(TExceptionConverter).AssemblyQualifiedName);
        }

        /// <summary>
        /// Generate SQL with comments. Defaults to false.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="autoComment"></param>
        /// <returns></returns>
        public static Configuration AutoCommentSql(this Configuration cfg, bool autoComment = true)
        {
            return cfg.SetProperty(CfgProperties.AutoCommentSql, autoComment.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// Mapping from tokens in NHibernate queries to SQL tokens (tokens might be function or literal names, for example).
        /// <para>eg. hqlLiteral=SQL_LITERAL, hqlFunction=SqlFunction</para>
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static Configuration HqlToSqlSubstitutions(this Configuration cfg, string val)
        {
            return cfg.SetProperty(CfgProperties.HqlToSqlSubstitutions, val);
        }

        /// <summary>
        /// Set a maximum "depth" for the outer join fetch tree for single-ended associations (one-to-one, many-to-one). A 0 disables default outer join fetching.
        /// <para>eg. recommended values between 0 and 3</para>
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        public static Configuration MaxFetchDepth(this Configuration cfg, byte depth = 3)
        {
            return cfg.SetProperty(CfgProperties.MaxFetchDepth, depth.ToString());
        }

        /// <summary>
        /// Automatically export schema DDL to the database when the <seealso cref="ISessionFactory"/> is created. With create-drop, the database schema will be dropped when the <seealso cref="ISessionFactory"/> is closed explicitly.
        /// <para>eg. create | create-drop</para>
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="schemaAutoAction"></param>
        /// <returns></returns>
        public static Configuration SchemaAction(this Configuration cfg, SchemaAutoAction schemaAutoAction)
        {
            return cfg.SetProperty(CfgProperties.SchemaAction, schemaAutoAction.ToString());
        }

        /// <summary>
        /// The class name of a custom <seealso cref="IQueryModelRewriterFactory"/> implementation (LINQ query model rewriter factory). Defaults to null (no rewriter).
        /// </summary>
        /// <typeparam name="TFactory"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration QueryModelRewriterFactory<TFactory>(this Configuration cfg)
            where TFactory : IQueryModelRewriterFactory
        {
            return cfg.SetProperty(CfgProperties.QueryRewriterFactory, typeof(TFactory).AssemblyQualifiedName);
        }

        /// <summary>
        /// Enable statistics collection within <seealso cref="ISessionFactory"/>.Statistics property. Defaults to false.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="generate"></param>
        /// <returns></returns>
        public static Configuration GenerateStatistics(this Configuration cfg, bool generate = true)
        {
            return cfg.SetProperty(CfgProperties.GenerateStatistics, generate.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// Enables or disables validation of interfaces or classes specified as proxies. Enabled by default.
        /// </summary>
        /// <param name="cfg"></param>
        /// <param name="use"></param>
        /// <returns></returns>
        public static Configuration UseProxyValidator(this Configuration cfg, bool use = false)
        {
            return cfg.SetProperty(CfgProperties.UseProxyValidator, use.ToString().ToLowerInvariant());
        }

        /// <summary>
        /// The class name of a custom <seealso cref="IProxyFactoryFactory"/> implementation.>.
        /// </summary>
        /// <typeparam name="TFactory"></typeparam>
        /// <param name="cfg"></param>
        /// <returns></returns>
        public static Configuration ProxyFactory<TFactory>(this Configuration cfg)
            where TFactory : IProxyFactoryFactory
        {
            return cfg.SetProperty(CfgProperties.ProxyFactoryFactory, typeof(TFactory).AssemblyQualifiedName);
        }
    }
}
