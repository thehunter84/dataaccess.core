﻿#if NETSTANDARD2_0
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Linq;
using NHibernate.SqlTypes;
using NHibernate.Type;
#else
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.Type;
#endif

namespace DataAccess.Core.Nh.Extensions
{
    internal static class QueryableExtensions
    {
#if NETSTANDARD2_0
        internal static IQueryable<TEntity> SetOptions<TEntity>(this IQueryable<TEntity> queryable)
        {
            return queryable//.WithLock(LockMode.Read)
                .WithOptions(options =>
                {
                    // 
                    options.SetReadOnly(true);

                });
            //return queryable;
        }
#else
        internal static IQueryable<TEntity> SetOptions<TEntity>(this IQueryable<TEntity> queryable)
        {
            return queryable;
        }
#endif
    }
}
