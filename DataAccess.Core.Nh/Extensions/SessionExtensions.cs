﻿#if NETSTANDARD2_0
using System.Linq;
using NHibernate;
#else
using System.Linq;
using NHibernate;
using NHibernate.Linq;
#endif

namespace DataAccess.Core.Nh.Extensions
{
    internal static class SessionExtensions
    {
        internal static bool Exists(this IStatelessSession session, string entityName, object id)
        {
            var query = session.CreateQuery($"select count(*) from {entityName} en where en.id=:id");
            query.SetParameter("id", id);

            return query.UniqueResult<long>() > 0;
        }

        internal static bool Exists(this ISession session, string entityName, object id)
        {
            var query = session.CreateQuery($"select count(*) from {entityName} en where en.id=:id");
            query.SetParameter("id", id);

            return query.UniqueResult<long>() > 0;
        }

        internal static IQueryable<TEntity> GetQuery<TEntity>(this ISession session)
            where TEntity: class
        {
            return session.Query<TEntity>();
        }
    }
}
