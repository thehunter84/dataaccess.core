﻿using System;
using DataAccess.Core.Providers;
using NHibernate;
using NHibernate.Impl;

namespace DataAccess.Core.Nh.Providers
{
    /// <summary>
    /// An <see cref="ISession"/> provider for NHibernate facades.
    /// <para>
    /// This provider uses a custom <see cref="ISession"/> implementation which decorates an <see cref="IStatelessSession"/> implementation.
    /// </para>
    /// </summary>
    public class StatelessSessionContextProvider : IContextProvider<ISession>
    {
        private ISession currentSession;
        private bool isDisposed;
        private readonly ISessionFactory sessionFactory;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionFactory"></param>
        public StatelessSessionContextProvider(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;

            using (var statelessSession = sessionFactory.OpenStatelessSession())
            {
                var session = statelessSession as StatelessSessionImpl;
                if (session == null)
                {
                    throw new NotSupportedException($"The current implementation of this provider needs {nameof(StatelessSessionImpl)} instance provided by the calling OpenStatelessSession() method from ISessionFactory instance, other kinds of types aren't supported.");
                }
            }
        }

        /// <inheritdoc/>
        public ISession GetCurrentContext()
        {
            this.EnsureNotDisposed();
            
            return this.currentSession ??= new StatelessSessionDecorator(sessionFactory.OpenStatelessSession() as StatelessSessionImpl);
        }

        /// <inheritdoc/>
        public void RenewContext()
        {
            this.EnsureNotDisposed();

            try
            {
                this.currentSession?.Dispose();
            }
            catch
            {
            }

            this.currentSession = this.sessionFactory.OpenSession();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            if (this.isDisposed)
                return;

            this.isDisposed = true;

            try
            {
                this.currentSession?.Dispose();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Ensures the not disposed.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">the current ContextProvider was disposed.</exception>
        private void EnsureNotDisposed()
        {
            if (this.isDisposed)
                throw new ObjectDisposedException("the current ContextProvider was disposed.");
        }
    }
}
