﻿#if NETSTANDARD2_0
using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Nh.Stats;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Impl;
using NHibernate.Stat;
using NHibernate.Type;
#else
using System;
using System.Data;
using System.Linq.Expressions;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Nh.Stats;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Impl;
using NHibernate.Stat;
using NHibernate.Type;
#endif

namespace DataAccess.Core.Nh.Providers
{
    /// <summary>
    /// A custom implementation of <see cref="ISession"/> used to decorate <see cref="IStatelessSession"/> implementation.
    /// </summary>
    internal class StatelessSessionDecorator : ISession
    {
        private readonly StatelessSessionImpl session;

        public StatelessSessionDecorator(StatelessSessionImpl session)
        {
            this.session = session;
        }

        /// <inheritdoc />
        public FlushMode FlushMode
        {
            get => this.session.FlushMode;
            set => throw new NotSupportedException();
        }

        /// <inheritdoc />
        public CacheMode CacheMode
        {
            get => this.session.CacheMode;
            set => throw new NotSupportedException();
        }

        /// <inheritdoc />
        public ISessionFactory SessionFactory => this.session.Factory;

        /// <inheritdoc />
        public bool IsOpen => this.session.IsOpen;

        /// <inheritdoc />
        public bool IsConnected => this.session.IsConnected;

        /// <inheritdoc />
        public bool DefaultReadOnly
        {
            get => this.session.PersistenceContext.DefaultReadOnly;
            set => this.session.PersistenceContext.DefaultReadOnly = value;
        }
        
        /// <inheritdoc />
        public ISessionStatistics Statistics => new CommonSessionStatistics(this.session.PersistenceContext);

#if NETSTANDARD2_0
        /// <inheritdoc />
        public ITransaction Transaction => this.session.GetCurrentTransaction();

        /// <inheritdoc />
        public DbConnection Connection => this.session.Connection;
#else
         public ITransaction Transaction => this.session.Transaction;

        /// <inheritdoc />
        public EntityMode ActiveEntityMode => this.session.EntityMode;

        /// <inheritdoc />
        public IDbConnection Connection => this.session.Connection;
#endif

        /// <inheritdoc />
        public void Flush()
        {
            this.session.Flush();
        }
        
        /// <inheritdoc />
        public void Reconnect()
        {
            this.session.ConnectionManager.Reconnect();
        }
        
        /// <inheritdoc />
        public void CancelQuery()
        {
            // nothing to do!!
        }

        /// <inheritdoc />
        public bool IsDirty()
        {
            return false;
        }

        /// <inheritdoc />
        public bool IsReadOnly(object entityOrProxy)
        {
            return false;
        }

        /// <inheritdoc />
        public void SetReadOnly(object entityOrProxy, bool readOnly)
        {
            // nothing to do!!
        }
        
        /// <inheritdoc />
        public bool Contains(object obj)
        {
            var entry = this.session.PersistenceContext.GetEntry(obj);
            return entry != null && entry.Status != Status.Deleted && entry.Status != Status.Gone;
        }

        /// <inheritdoc />
        public void Evict(object obj)
        {
            // nothing to do!!
        }

        /// <inheritdoc />
        public object Load(Type theType, object id, LockMode lockMode)
        {
            return this.session.Get(theType.FullName, id, lockMode);
        }

        /// <inheritdoc />
        public object Load(string entityName, object id, LockMode lockMode)
        {
            return this.session.Get(entityName, id, lockMode);
        }

        /// <inheritdoc />
        public object Load(Type theType, object id)
        {
            return this.Load(theType, id, LockMode.None);
        }

        /// <inheritdoc />
        public T Load<T>(object id, LockMode lockMode)
        {
            return this.session.Get<T>(id, lockMode);
        }

        /// <inheritdoc />
        public T Load<T>(object id)
        {
            return this.session.Get<T>(id);
        }

        /// <inheritdoc />
        public object Load(string entityName, object id)
        {
            return this.session.Get(entityName, id);
        }

        /// <inheritdoc />
        public void Load(object obj, object id)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Replicate(object obj, ReplicationMode replicationMode)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Replicate(string entityName, object obj, ReplicationMode replicationMode)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public object Save(object obj)
        {
            return this.session.Insert(obj);
        }

        /// <inheritdoc />
        public void Save(object obj, object id)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public object Save(string entityName, object obj)
        {
            return this.session.Insert(entityName, obj);
        }

        /// <inheritdoc />
        public void Save(string entityName, object obj, object id)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void SaveOrUpdate(object obj)
        {
            var entityName = this.session.BestGuessEntityName(obj);
            this.SaveOrUpdate(entityName, obj);
        }
        
        /// <inheritdoc />
        public void SaveOrUpdate(string entityName, object obj, object id)
        {
            // this.session.ExecuteUpdate();
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Update(object obj)
        {
            this.session.Update(obj);
        }

        /// <inheritdoc />
        public void Update(object obj, object id)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Update(string entityName, object obj)
        {
            this.session.Update(entityName, obj);
        }

        /// <inheritdoc />
        public void Update(string entityName, object obj, object id)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public object Merge(object obj)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public object Merge(string entityName, object obj)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public T Merge<T>(T entity) where T : class
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public T Merge<T>(string entityName, T entity) where T : class
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Persist(object obj)
        {
            this.SaveOrUpdate(obj);
        }

        /// <inheritdoc />
        public void Persist(string entityName, object obj)
        {
            this.SaveOrUpdate(entityName, obj);
        }

        /// <inheritdoc />
        public void Delete(object obj)
        {
            this.session.Delete(obj);
        }

        /// <inheritdoc />
        public void Delete(string entityName, object obj)
        {
            this.session.Delete(entityName, obj);
        }

        /// <inheritdoc />
        public int Delete(string query)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public int Delete(string query, object value, IType type)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public int Delete(string query, object[] values, IType[] types)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Lock(object obj, LockMode lockMode)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Lock(string entityName, object obj, LockMode lockMode)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Refresh(object obj)
        {
            this.session.Refresh(obj);
        }

        /// <inheritdoc />
        public void Refresh(object obj, LockMode lockMode)
        {
            this.session.Refresh(obj, lockMode);
        }

        /// <inheritdoc />
        public LockMode GetCurrentLockMode(object obj)
        {
            var entry = this.session.PersistenceContext.GetEntry(obj);
            return entry?.LockMode;
        }

        /// <inheritdoc />
        public ITransaction BeginTransaction()
        {
            return this.session.BeginTransaction();
        }

        /// <inheritdoc />
        public ITransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return this.session.BeginTransaction(isolationLevel);
        }

        /// <inheritdoc />
        public ICriteria CreateCriteria<T>() where T : class
        {
            return this.session.CreateCriteria<T>();
        }

        /// <inheritdoc />
        public ICriteria CreateCriteria<T>(string alias) where T : class
        {
            return this.session.CreateCriteria<T>(alias);
        }

        /// <inheritdoc />
        public ICriteria CreateCriteria(Type persistentClass)
        {
            return this.session.CreateCriteria(persistentClass);
        }

        /// <inheritdoc />
        public ICriteria CreateCriteria(Type persistentClass, string alias)
        {
            return this.session.CreateCriteria(persistentClass, alias);
        }

        /// <inheritdoc />
        public ICriteria CreateCriteria(string entityName)
        {
            return this.session.CreateCriteria(entityName);
        }

        /// <inheritdoc />
        public ICriteria CreateCriteria(string entityName, string alias)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public IQueryOver<T, T> QueryOver<T>() where T : class
        {
            return this.session.QueryOver<T>();
        }

        /// <inheritdoc />
        public IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias) where T : class
        {
            return this.session.QueryOver(alias);
        }

        /// <inheritdoc />
        public IQueryOver<T, T> QueryOver<T>(string entityName) where T : class
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public IQueryOver<T, T> QueryOver<T>(string entityName, Expression<Func<T>> alias) where T : class
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public IQuery CreateQuery(string queryString)
        {
            return this.session.CreateQuery(queryString);
        }

        /// <inheritdoc />
        public IQuery CreateFilter(object collection, string queryString)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public IQuery GetNamedQuery(string queryName)
        {
            return this.session.GetNamedQuery(queryName);
        }

        /// <inheritdoc />
        public ISQLQuery CreateSQLQuery(string queryString)
        {
            return this.session.CreateSQLQuery(queryString);
        }

        /// <inheritdoc />
        public void Clear()
        {
            //this.session.PersistenceContext.Clear();
            // nothing to do!
        }

        /// <inheritdoc />
        public object Get(Type clazz, object id)
        {
            return this.Get(clazz, id, LockMode.None);
        }

        /// <inheritdoc />
        public object Get(Type clazz, object id, LockMode lockMode)
        {
            return this.session.Get(clazz.FullName, id, lockMode);
        }

        /// <inheritdoc />
        public object Get(string entityName, object id)
        {
            return this.session.Get(entityName, id);
        }

        /// <inheritdoc />
        public T Get<T>(object id)
        {
            return this.session.Get<T>(id);
        }

        /// <inheritdoc />
        public T Get<T>(object id, LockMode lockMode)
        {
            return this.session.Get<T>(id, lockMode);
        }

        /// <inheritdoc />
        public string GetEntityName(object obj)
        {
            var entry = this.session.PersistenceContext.GetEntry(obj);
            return entry?.EntityName;
        }

        /// <inheritdoc />
        public IFilter EnableFilter(string filterName)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public IFilter GetEnabledFilter(string filterName)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void DisableFilter(string filterName)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
#pragma warning disable 618
        public IMultiQuery CreateMultiQuery()
#pragma warning restore 618
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public ISession SetBatchSize(int batchSize)
        {
            this.session.SetBatchSize(batchSize);
            return this;
        }

        /// <inheritdoc />
        public ISessionImplementor GetSessionImplementation()
        {
            return this.session.GetSessionImplementation();
        }

        /// <inheritdoc />
#pragma warning disable 618
        public IMultiCriteria CreateMultiCriteria()
#pragma warning restore 618
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public ISession GetSession(EntityMode entityMode)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.session.Dispose();
        }

#if NETSTANDARD2_0
        /// <inheritdoc />
        public object GetIdentifier(object obj)
        {
            var entityPersister = this.session.GetEntityPersister(this.session.GuessEntityName(obj), obj);
            return entityPersister.GetIdentifier(obj);
        }

        /// <inheritdoc />
        public DbConnection Disconnect()
        {
            return this.session.ConnectionManager.Disconnect();
        }

        /// <inheritdoc />
        public void Reconnect(DbConnection connection)
        {
            this.session.ConnectionManager.Reconnect(connection);
        }

        /// <inheritdoc />
        public void SaveOrUpdate(string entityName, object obj)
        {
            var classMetadata = this.session.GetEntityPersister(entityName, obj).ClassMetadata;
            var id = classMetadata.GetIdentifier(obj);

            if (this.session.Exists(entityName, id))
            {
                this.session.Update(obj);
            }
            else
            {
                this.session.Insert(obj);
            }
        }

        /// <inheritdoc />
        public DbConnection Close()
        {
            this.session.Close();
            return this.Connection;
        }

        /// <inheritdoc />
        public Task FlushAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.FlushAsync(cancellationToken);
        }

        /// <inheritdoc />
        public Task<bool> IsDirtyAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.FromResult(false);
        }

        /// <inheritdoc />
        public Task EvictAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.FromResult(false);
        }

        /// <inheritdoc />
        public Task<object> LoadAsync(Type theType, object id, LockMode lockMode,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return this.LoadAsync(theType.FullName, id, lockMode, cancellationToken);
        }

        /// <inheritdoc />
        public Task<object> LoadAsync(string entityName, object id, LockMode lockMode,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync(entityName, id, lockMode, cancellationToken);
        }

        /// <inheritdoc />
        public Task<object> LoadAsync(Type theType, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync(theType.FullName, id, cancellationToken);
        }

        /// <inheritdoc />
        public Task<T> LoadAsync<T>(object id, LockMode lockMode, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync<T>(id, lockMode, cancellationToken);
        }

        /// <inheritdoc />
        public Task<T> LoadAsync<T>(object id, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync<T>(id, cancellationToken);
        }

        /// <inheritdoc />
        public Task<object> LoadAsync(string entityName, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync(entityName, id, cancellationToken);
        }

        /// <inheritdoc />
        public Task LoadAsync(object obj, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task ReplicateAsync(object obj, ReplicationMode replicationMode,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task ReplicateAsync(string entityName, object obj, ReplicationMode replicationMode,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<object> SaveAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.InsertAsync(obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task SaveAsync(object obj, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<object> SaveAsync(string entityName, object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.InsertAsync(entityName, obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task SaveAsync(string entityName, object obj, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task SaveOrUpdateAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            var entityName = this.session.BestGuessEntityName(obj);
            return this.SaveOrUpdateAsync(entityName, obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task SaveOrUpdateAsync(string entityName, object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            this.SaveOrUpdate(entityName, obj);

            return Task.FromResult(0);
        }

        /// <inheritdoc />
        public Task SaveOrUpdateAsync(string entityName, object obj, object id,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task UpdateAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.UpdateAsync(obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task UpdateAsync(object obj, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            // this.session.ExecuteUpdate(new NhLinqExpression(null, this.session.Factory), null);
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task UpdateAsync(string entityName, object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.UpdateAsync(entityName, obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task UpdateAsync(string entityName, object obj, object id,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<object> MergeAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<object> MergeAsync(string entityName, object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<T> MergeAsync<T>(T entity, CancellationToken cancellationToken = new CancellationToken()) where T : class
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<T> MergeAsync<T>(string entityName, T entity, CancellationToken cancellationToken = new CancellationToken()) where T : class
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task PersistAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            var entityName = this.session.BestGuessEntityName(obj);
            return this.PersistAsync(entityName, obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task PersistAsync(string entityName, object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.SaveOrUpdateAsync(entityName, obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task DeleteAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.DeleteAsync(obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task DeleteAsync(string entityName, object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.DeleteAsync(entityName, obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task<int> DeleteAsync(string query, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<int> DeleteAsync(string query, object value, IType type, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<int> DeleteAsync(string query, object[] values, IType[] types,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task LockAsync(object obj, LockMode lockMode, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task LockAsync(string entityName, object obj, LockMode lockMode,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task RefreshAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.RefreshAsync(obj, cancellationToken);
        }

        /// <inheritdoc />
        public Task RefreshAsync(object obj, LockMode lockMode, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.RefreshAsync(obj, lockMode, cancellationToken);
        }

        /// <inheritdoc />
        public Task<IQuery> CreateFilterAsync(object collection, string queryString,
            CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Task<object> GetAsync(Type clazz, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.GetAsync(clazz.FullName, id, cancellationToken);
        }

        /// <inheritdoc />
        public Task<object> GetAsync(Type clazz, object id, LockMode lockMode, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync(clazz.FullName, id, lockMode, cancellationToken);
        }

        /// <inheritdoc />
        public Task<object> GetAsync(string entityName, object id, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync(entityName, id, cancellationToken);
        }

        /// <inheritdoc />
        public Task<T> GetAsync<T>(object id, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync<T>(id, cancellationToken);
        }

        /// <inheritdoc />
        public Task<T> GetAsync<T>(object id, LockMode lockMode, CancellationToken cancellationToken = new CancellationToken())
        {
            return this.session.GetAsync<T>(id, lockMode, cancellationToken);
        }

        /// <inheritdoc />
        public Task<string> GetEntityNameAsync(object obj, CancellationToken cancellationToken = new CancellationToken())
        {
            var entry = this.session.PersistenceContext.GetEntry(obj);
            return Task.FromResult(entry?.EntityName);
        }

        /// <inheritdoc />
        public ISharedSessionBuilder SessionWithOptions()
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public void JoinTransaction()
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public IQueryable<T> Query<T>()
        {
            return this.session.Query<T>();
        }

        /// <inheritdoc />
        public IQueryable<T> Query<T>(string entityName)
        {
            return this.session.Query<T>(entityName);
        }
#else
        /// <inheritdoc />
        public object GetIdentifier(object obj)
        {
            var entityPersister = this.session.GetEntityPersister(this.session.GuessEntityName(obj), obj);
            return entityPersister.GetIdentifier(obj, this.session.EntityMode);
        }

        /// <inheritdoc />
        public IDbConnection Disconnect()
        {
            return this.session.ConnectionManager.Disconnect();
        }

        /// <inheritdoc />
        public void Reconnect(IDbConnection connection)
        {
            this.session.ConnectionManager.Reconnect(connection);
        }

        /// <inheritdoc />
        public void SaveOrUpdate(string entityName, object obj)
        {
            var classMetadata = this.session.GetEntityPersister(entityName, obj).ClassMetadata;
            var id = classMetadata.GetIdentifier(obj, this.ActiveEntityMode);

            if (this.session.Exists(entityName, id))
            {
                this.session.Update(obj);
            }
            else
            {
                this.session.Insert(obj);
            }
        }

        /// <inheritdoc />
        public IDbConnection Close()
        {
            this.session.Close();
            return this.Connection;
        }
#endif
    }
}
