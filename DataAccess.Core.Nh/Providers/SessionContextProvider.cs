﻿using System;
using DataAccess.Core.Providers;
using NHibernate;

namespace DataAccess.Core.Nh.Providers
{
    /// <summary>
    /// An <see cref="ISession"/> provider for NHibernate facades.
    /// </summary>
    /// <seealso cref="ISession" />
    public class SessionContextProvider : IContextProvider<ISession>
    {
        private ISession currentSession;
        private bool isDisposed;
        private readonly ISessionFactory sessionFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionContextProvider"/> class.
        /// </summary>
        /// <param name="sessionFactory">The session factory.</param>
        public SessionContextProvider(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        /// <inheritdoc/>
        public ISession GetCurrentContext()
        {
            this.EnsureNotDisposed();
            
            return this.currentSession ??= this.sessionFactory.OpenSession();
        }

        /// <inheritdoc/>
        public void RenewContext()
        {
            this.EnsureNotDisposed();

            try
            {
                this.currentSession?.Dispose();
            }
            catch
            {
            }

            this.currentSession = this.sessionFactory.OpenSession();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            if (this.isDisposed)
                return;

            this.isDisposed = true;

            try
            {
                this.currentSession?.Dispose();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Ensures the not disposed.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">the current ContextProvider was disposed.</exception>
        private void EnsureNotDisposed()
        {
            if (this.isDisposed)
                throw new ObjectDisposedException("the current ContextProvider was disposed.");
        }
    }
}
