﻿using System;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using NHibernate;

namespace DataAccess.Core.Nh.Providers
{
    /// <summary>
    /// A transaction provider for NHibernate.
    /// </summary>
    /// <seealso cref="ISession" />
    public class NhTransactionProvider : TransactionProviderBase<ISession>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NhTransactionProvider"/> class.
        /// </summary>
        /// <param name="sessionProvider">The session provider.</param>
        public NhTransactionProvider(IContextProvider<ISession> sessionProvider)
            : base(sessionProvider)
        {
        }

        /// <inheritdoc/>
        protected override ITransactionWorker OnBuildTransactionWorker(TransactionDescriptor descriptor, ISession context)
        {
            ITransactionWorker info;

            if (this.ExistsRootTransaction())
            {
                /* Inner Transaction */
                info = new TransactionWorkerImpl(descriptor,
                    // ReSharper disable once RedundantArgumentName
                    onBegin: (caller, desc) =>
                    {
                    },
                    // ReSharper disable once RedundantArgumentName
                    onCommit: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);

                        try
                        {
                            ISession session = caller.Context;
                            session.Flush();
                        }
                        catch (Exception ex)
                        {
                            var exception = new CommitFailedException($"Error when the current session tries to commit the current transaction (name: {desc.Name}).", "CommitTransaction", ex);
                            throw exception;
                        }
                    },
                    // ReSharper disable once RedundantArgumentName
                    onRollback: (caller, desc) =>
                    {
                        var tran = this.RemoveTransaction(desc.Name);

                        Transaction.Current?.Rollback();

                        throw new InnerRollBackException("An inner rollback transaction has occurred.", tran);
                    },
                    // ReSharper disable once RedundantArgumentName
                    onDispose: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);
                    },
                    // ReSharper disable once RedundantArgumentName
                    initializer: caller =>
                    {
                        caller.Context = context;
                    },
                    // ReSharper disable once RedundantArgumentNameForLiteralExpression
                    begin: false);
            }
            else
            {
                /* First transaction */
                info = new TransactionWorkerImpl(descriptor,
                    // ReSharper disable once RedundantArgumentName
                    onBegin: (caller, desc) =>
                    {
                        caller.Transaction = descriptor.AsTransactionScope();
                    },
                    // ReSharper disable once RedundantArgumentName
                    onCommit: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);

                        try
                        {
                            ISession session = caller.Context;
                            session.Flush();

                            TransactionScope scope = caller.Transaction;
                            
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            var exception = new CommitFailedException($"Error when the current session tries to commit the current transaction (name: {desc.Name}).", "CommitTransaction", ex);
                            throw exception;
                        }
                    },
                    // ReSharper disable once RedundantArgumentName
                    onRollback: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);
                    },
                    // ReSharper disable once RedundantArgumentName
                    onDispose: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);

                        TransactionScope scope = caller.Transaction;
                        scope.Dispose();
                    },
                    // ReSharper disable once RedundantArgumentName
                    initializer: caller =>
                    {
                        caller.Context = context;
                    },
                    // ReSharper disable once RedundantArgumentNameForLiteralExpression
                    begin: false);
            }

            return info;
        }
    }
}
