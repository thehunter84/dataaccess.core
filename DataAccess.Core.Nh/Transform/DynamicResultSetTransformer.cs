﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using NHibernate.Transform;

namespace DataAccess.Core.Nh.Transform
{
    /// <summary>
    /// 
    /// </summary>
    public class DynamicResultSetTransformer : IResultTransformer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public IList TransformList(IList collection)
        {
            return collection;
        }

        /// <inheritdoc/>
        public object TransformTuple(object[] tuple, string[] aliases)
        {
            var expando = new ExpandoObject();

            var dictionary = (IDictionary<string, object>)expando;

            for (var i = 0; i < tuple.Length; i++)
            {
                var alias = aliases[i];

                if (alias != null)
                {
                    dictionary[alias] = tuple[i];
                }
            }

            return expando;
        }
    }
}
