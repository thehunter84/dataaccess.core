﻿using NHibernate.Transform;

namespace DataAccess.Core.Nh.Transform
{
    /// <summary>
    /// 
    /// </summary>
    public static class NhTransformers
    {
        /// <summary>
        /// 
        /// </summary>
        public static IResultTransformer ExpandoObject { get; }

        /// <summary>
        /// 
        /// </summary>
        static NhTransformers()
        {
            ExpandoObject = new DynamicResultSetTransformer();
        }
    }
}
