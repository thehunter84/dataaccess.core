﻿using System;
using NHibernate.Transform;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// 
    /// </summary>
    internal class MaterializerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        internal Type Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal IResultTransformer Transformer { get; set; }
    }
}
