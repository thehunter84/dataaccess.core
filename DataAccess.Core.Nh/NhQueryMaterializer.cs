﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.Core.Nh.Transform;
using NHibernate;
using NHibernate.Metadata;
using NHibernate.Transform;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="NhQueryDescriptor" />
    /// <seealso cref="IQueryMaterializer" />
    public class NhQueryMaterializer : NhQueryDescriptor, IQueryMaterializer
    {
        private readonly IQuery query;
        private readonly Func<Type, IClassMetadata> metadataFunc;
        private readonly IList<MaterializerInfo> materializers;

        /// <summary>
        /// Initializes a new instance of the <see cref="NhQueryMaterializer"/> class.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="name">The name.</param>
        /// <param name="metadataFunc">The metadata function.</param>
        /// <param name="queryArguments">The query arguments.</param>
        public NhQueryMaterializer(IQuery query, string name, Func<Type, IClassMetadata> metadataFunc, IEnumerable<IQueryArgument> queryArguments = null)
            : base(query, name, queryArguments)
        {
            this.query = query;
            this.metadataFunc = metadataFunc;
            this.materializers = new List<MaterializerInfo>();
        }
        
        /// <inheritdoc /> 
        public IEnumerable<TResult> Execute<TResult>()
        {
            this.SetTransformerOrEntity<TResult>();

            return this.query.List<TResult>();
        }

        //public IEnumerable<TResult> Execute<TResult>(int pageIndex, int maxResult)
        //{
        //    this.SetTransformerOrEntity<TResult>();

        //    this.query.SetMaxResults(maxResult)
        //        .SetFirstResult(maxResult * pageIndex);

        //    return this.query.List<TResult>();
        //}

        /// <inheritdoc /> 
        public TResult UniqueResult<TResult>()
        {
            this.SetTransformerOrEntity<TResult>();

            return this.query.UniqueResult<TResult>();
        }

        private void SetTransformerOrEntity<TResult>()
        {
            if (this.query is ISQLQuery)
            {
                var type = typeof(TResult);

                if (type.IsClass && type != typeof(string))
                {
                    ISQLQuery qq = this.query as dynamic;
                    
                    var metadata = this.metadataFunc(type);

                    if (metadata != null)
                    {
                        qq.AddEntity(type);
                    }
                    else
                    {
                        var materializeInfo = this.materializers.FirstOrDefault(info => info.Type == type);
                        if (materializeInfo == null)
                        {
                            materializeInfo = new MaterializerInfo
                            {
                                Type = type,
                                Transformer = type == typeof(object) ? NhTransformers.ExpandoObject : Transformers.AliasToBean<TResult>()
                            };

                            this.materializers.Add(materializeInfo);
                        }

                        qq.SetResultTransformer(materializeInfo.Transformer);
                    }
                }
            }
        }
    }
}
