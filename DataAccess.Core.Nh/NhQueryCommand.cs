﻿using System.Collections.Generic;
using NHibernate;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="NhQueryDescriptor" />
    /// <seealso cref="IQueryCommand" />
    public class NhQueryCommand : NhQueryDescriptor, IQueryCommand
    {
        private readonly IQuery query;

        /// <summary>
        /// Initializes a new instance of the <see cref="NhQueryCommand"/> class.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="name">The name.</param>
        /// <param name="queryArguments">The parameters.</param>
        public NhQueryCommand(IQuery query, string name, IEnumerable<IQueryArgument> queryArguments = null)
            : base(query, name, queryArguments)
        {
            this.query = query;
        }
        
        /// <inheritdoc />
        public int Execute()
        {
            var numRows = this.query.ExecuteUpdate();
            return numRows;
        }
    }
}
