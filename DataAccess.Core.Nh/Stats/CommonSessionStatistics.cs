﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Engine;
using NHibernate.Stat;

namespace DataAccess.Core.Nh.Stats
{
    /// <summary>
    /// A common session statistics for <see cref="NHibernate.ISession"/> instances.
    /// </summary>
    public class CommonSessionStatistics : ISessionStatistics
    {
        /// <summary>
        /// Creates a new <see cref="CommonSessionStatistics"/> instance.
        /// </summary>
        /// <param name="persistenceContext"></param>
        public CommonSessionStatistics(IPersistenceContext persistenceContext)
        {
            this.EntityCount = persistenceContext.EntityEntries.Count;
            this.CollectionCount = persistenceContext.CollectionEntries.Count;

            this.EntityKeys = persistenceContext.EntitiesByKey.Keys
                .ToList()
                .AsReadOnly();

            this.CollectionKeys = persistenceContext.CollectionsByKey.Keys
                .ToList()
                .AsReadOnly();
        }

        /// <inheritdoc />
        public int EntityCount { get; }

        /// <inheritdoc />
        public int CollectionCount { get; }

        /// <inheritdoc />
        public IList<EntityKey> EntityKeys { get; }

        /// <inheritdoc />
        public IList<CollectionKey> CollectionKeys { get; }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"SessionStatistics[entity count={this.EntityCount}, collection count={this.CollectionCount}]";
        }
    }
}
