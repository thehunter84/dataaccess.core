﻿using NHibernate.Cfg;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// Represents almost all properties implemented by extension methods related to <seealso cref="Configuration"/> instance.
    /// <para>
    /// See the documentation : <see href="https://nhibernate.info/doc/nhibernate-reference/session-configuration.html"/>
    /// </para>
    /// </summary>
    public class CfgProperties
    {
        /// <summary>
        /// [dialect] <see cref="Configuration"/> property.
        /// </summary>
        public const string Dialect = "dialect";

        /// <summary>
        /// [hbm2ddl.keywords] <see cref="Configuration"/> property.
        /// </summary>
        public const string Hbm2DdlKeywords = "hbm2ddl.keywords";

        /// <summary>
        /// [show_sql] <see cref="Configuration"/> property.
        /// </summary>
        public const string ShowSql = "show_sql";

        /// <summary>
        /// [format_sql] <see cref="Configuration"/> property.
        /// </summary>
        public const string FormatSql = "format_sql";

        /// <summary>
        /// [connection.provider] <see cref="Configuration"/> property.
        /// </summary>
        public const string ConnectionProvider = "connection.provider";

        /// <summary>
        /// [connection.driver_class] <see cref="Configuration"/> property.
        /// </summary>
        public const string Driver = "connection.driver_class";

        /// <summary>
        /// [connection.isolation] <see cref="Configuration"/> property.
        /// </summary>
        public const string Isolation = "connection.isolation";

        /// <summary>
        /// [connection.release_mode] <see cref="Configuration"/> property.
        /// </summary>
        public const string ConnectionReleaseMode = "connection.release_mode";

        /// <summary>
        /// [connection.connection_string] <see cref="Configuration"/> property.
        /// </summary>
        public const string ConnectionString = "connection.connection_string";

        /// <summary>
        /// [connection.connection_string_name] <see cref="Configuration"/> property.
        /// </summary>
        public const string ConnectionName = "connection.connection_string_name";

        /// <summary>
        /// [adonet.factory_class] <see cref="Configuration"/> property.
        /// </summary>
        public const string BatcherFactory = "adonet.factory_class";

        /// <summary>
        /// [adonet.batch_size] <see cref="Configuration"/> property.
        /// </summary>
        public const string BatchSize = "adonet.batch_size";

        /// <summary>
        /// [order_inserts] <see cref="Configuration"/> property.
        /// </summary>
        public const string OrderInserts = "order_inserts";

        /// <summary>
        /// [order_updates] <see cref="Configuration"/> property.
        /// </summary>
        public const string OrderUpdates = "order_updates";

        /// <summary>
        /// [transaction.factory_class] <see cref="Configuration"/> property.
        /// </summary>
        public const string TransactionFactory = "transaction.factory_class";

        /// <summary>
        /// [prepare_sql] <see cref="Configuration"/> property.
        /// </summary>
        public const string PrepareSql = "prepare_sql";

        /// <summary>
        /// [command_timeout] <see cref="Configuration"/> property.
        /// </summary>
        public const string CommandTimeout = "command_timeout";

        /// <summary>
        /// [sql_exception_converter] <see cref="Configuration"/> property.
        /// </summary>
        public const string SqlExceptionConverter = "sql_exception_converter";

        /// <summary>
        /// [use_sql_comments] <see cref="Configuration"/> property.
        /// </summary>
        public const string AutoCommentSql = "use_sql_comments";

        /// <summary>
        /// [query.substitutions] <see cref="Configuration"/> property.
        /// </summary>
        public const string HqlToSqlSubstitutions = "query.substitutions";

        /// <summary>
        /// [max_fetch_depth] <see cref="Configuration"/> property.
        /// </summary>
        public const string MaxFetchDepth = "max_fetch_depth";

        /// <summary>
        /// [hbm2ddl.auto] <see cref="Configuration"/> property.
        /// </summary>
        public const string SchemaAction = "hbm2ddl.auto";

        /// <summary>
        /// [query.query_model_rewriter_factory] <see cref="Configuration"/> property.
        /// </summary>
        public const string QueryRewriterFactory = "query.query_model_rewriter_factory";

        /// <summary>
        /// [use_proxy_validator] <see cref="Configuration"/> property.
        /// </summary>
        public const string UseProxyValidator = "use_proxy_validator";

        /// <summary>
        /// [proxyfactory.factory_class] <see cref="Configuration"/> property.
        /// </summary>
        public const string ProxyFactoryFactory = "proxyfactory.factory_class";

        /// <summary>
        /// [generate_statistics] <see cref="Configuration"/> property.
        /// </summary>
        public const string GenerateStatistics = "generate_statistics";

        //default_catalog
        //default_schema
        //adonet.batch_versioned_data
        //adonet.wrap_result_sets
        //cache.use_second_level_cache
        //cache.provider_class
        //cache.use_minimal_puts
        //cache.use_query_cache
        //cache.query_cache_factory
        //cache.region_prefix
        //cache.default_expiration
        //query.default_cast_length = 4000
        //query.default_cast_precision = 28
        //query.default_cast_scale
        //query.startup_check
        //query.factory_class
        //query.linq_provider_class
        //linqtohql.generatorsregistry

        //collectiontype.factory_class
        //transaction.use_connection_on_system_prepare
        //transaction.system_completion_lock_timeout
        //default_flush_mode = Manual | Commit | Auto | Always
        //default_batch_fetch_size = 1
        //current_session_context_class
        //id.optimizer.pooled.prefer_lo
        //track_session_id
        //sql_types.keep_datetime
        //firebird.disable_parameter_casting
        //oracle.use_n_prefixed_types_for_unicode
        //odbc.explicit_datetime_scale
        //nhibernate-logger
    }
}
