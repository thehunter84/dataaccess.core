﻿using System;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using NHibernate;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IQueryDataFacade" />
    public class NhQueryDefinitionDataFacade
        : IQueryDataFacade
    {
        private readonly ITransactionProvider<ISession> transactionProvider;
        private readonly INamedQueryProvider namedQueryProvider;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="NhQueryDefinitionDataFacade"/> class.
        /// </summary>
        /// <param name="transactionProvider">The transaction provider.</param>
        /// <param name="namedQueryProvider">The named query provider.</param>
        public NhQueryDefinitionDataFacade(ITransactionProvider<ISession> transactionProvider, INamedQueryProvider namedQueryProvider)
        {
            this.transactionProvider = transactionProvider;
            this.namedQueryProvider = namedQueryProvider;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(string name, IsolationLevel? isolation = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(new TransactionDescriptor { Name = name, Isolation = isolation });
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(TransactionDescriptor descriptor = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(descriptor);
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public IQueryCommand GetQueryCommand(string namedQuery)
        {
            var query = this.namedQueryProvider.GetNamedQuery(namedQuery);

            query.ThrowExceptionIfNull(() => new NoQueryDefinitionException(namedQuery, "No query command was found with the given query."));
            query.ThrowException(definition => definition.QueryType != QueryType.Command, definition => new NoQueryDefinitionException(namedQuery, "The given named query isn't a Command type."));

            var query0 = query.AsIQuery(this.transactionProvider.ContextProvider.GetCurrentContext());

            return new NhQueryCommand(query0, namedQuery);
        }

        /// <inheritdoc/>
        public IQueryMaterializer GetQueryMaterializer(string namedQuery)
        {
            var query = this.namedQueryProvider.GetNamedQuery(namedQuery);

            query.ThrowExceptionIfNull(() => new NoQueryDefinitionException(namedQuery, "No query materializer was found with the given name."));
            query.ThrowException(definition => definition.QueryType != QueryType.Materializer, definition => new NoQueryDefinitionException(namedQuery, "The given named query isn't a Materializer type."));
            
            var session = this.transactionProvider.ContextProvider.GetCurrentContext();
            var query0 = query.AsIQuery(session);
            
            return new NhQueryMaterializer(query0, namedQuery, type => session.SessionFactory.GetClassMetadata(type), query.Parameters);
        }
    }
}
