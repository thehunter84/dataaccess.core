﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using NHibernate;

namespace DataAccess.Core.Nh
{
    /// <inheritdoc cref="IQueryableDataFacadeService" />
    public class NhQueryableDataFacadeService
        : NhQueryableDataFacade, IQueryableDataFacadeService
    {
        private readonly ITransactionProvider<ISession> transactionProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="NhQueryableDataFacadeService"/> class.
        /// </summary>
        /// <param name="transactionProvider">The transaction provider.</param>
        public NhQueryableDataFacadeService(ITransactionProvider<ISession> transactionProvider)
            : base(transactionProvider.ContextProvider)
        {
            this.transactionProvider = transactionProvider;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(string name, IsolationLevel? isolation = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(new TransactionDescriptor { Name = name, Isolation = isolation });
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(TransactionDescriptor descriptor = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(descriptor);
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public bool HasChanges()
        {
            var session = this.ContextProvider.GetCurrentContext();

            return session.IsDirty();
        }

        /// <inheritdoc/>
        public bool Cached(params object[] instances)
        {
            var session = this.ContextProvider.GetCurrentContext();

            return instances.All(instance => session.Contains(instance));
        }

        /// <inheritdoc/>
        public void Evict(params object[] instances)
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var instance in instances)
            {
                session.Evict(instance);
            }
        }

        /// <inheritdoc/>
        public void Restore(params object[] instances)
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var instance in instances)
            {
                session.Refresh(instance);
            }
        }

        /// <inheritdoc/>
        public void Clear()
        {
            var session = this.ContextProvider.GetCurrentContext();

            session.Clear();
        }

        /// <inheritdoc/>
        public TEntity Insert<TEntity>(TEntity entity) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            session.Save(entity);

            return entity;
        }

        /// <inheritdoc/>
        public void Insert<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var entity in entities)
            {
                session.SaveOrUpdate(entity);
            }
        }

        /// <inheritdoc/>
        public object Insert(object entity)
        {
            var session = this.ContextProvider.GetCurrentContext();
            session.Save(entity);

            return entity;
        }

        /// <inheritdoc/>
        public void Insert(IEnumerable<object> entities)
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var entity in entities)
            {
                session.SaveOrUpdate(entity);
            }
        }

        /// <inheritdoc/>
        public TEntity MakePersistent<TEntity>(TEntity entity) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            session.SaveOrUpdate(entity);

            return entity;
        }

        /// <inheritdoc/>
        public void MakePersistent<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var entity in entities)
            {
                session.SaveOrUpdate(entity);
            }

            //Transaction.Current.FlushSessionOnComplete(session);
        }

        /// <inheritdoc/>
        public object MakePersistent(object entity)
        {
            var session = this.ContextProvider.GetCurrentContext();

            session.SaveOrUpdate(entity);

            return entity;
        }

        /// <inheritdoc/>
        public void MakePersistent(IEnumerable<object> entities)
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var entity in entities)
            {
                session.SaveOrUpdate(entity);
            }

            //Transaction.Current.FlushSessionOnComplete(session);
        }

        /// <inheritdoc/>
        public void MakeTransient<TEntity>(object identifier) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();

            /*
            // TODO: verify if instance cached is set to detached
            var metadata = session.SessionFactory.GetClassMetadata(typeof(TEntity));

            var hql = $"delete {metadata.EntityName} where id = :id";
            var results = session.CreateQuery(hql)
                .SetParameter("id", identifier)
                .ExecuteUpdate();
                */

            var instance = session.Get<TEntity>(identifier, LockMode.None);

            if (instance != null)
            {
                session.Delete(instance);
            }
        }

        /// <summary>
        /// Makes the persistent the given instance into underlying storage.
        /// </summary>
        /// <param name="entity">The entity to make persistent.</param>
        /// <returns>Returns the given instance with a persistence status.</returns>
        public TEntity MakeTransient<TEntity>(TEntity entity) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            session.Delete(entity);

            return entity;
        }

        /// <summary>
        /// Makes transient the given entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entities.</param>
        public void MakeTransient<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();

            foreach (var entity in entities)
            {
                session.Delete(entity);
            }
        }

        /// <summary>
        /// Makes transient the given entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="condition">The condition.</param>
        public void MakeTransient<TEntity>(Expression<Func<TEntity, bool>> condition) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            var instances = session.GetQuery<TEntity>().Where(condition).ToList();

            foreach (var instance in instances)
            {
                session.Delete(instance);
            }
        }
    }
}
