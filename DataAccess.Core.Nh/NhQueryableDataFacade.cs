﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Paging;
using DataAccess.Core.Providers;
using NHibernate;
using NHibernate.Linq;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IQueryableDataFacade" />
    public class NhQueryableDataFacade : IQueryableDataFacade
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NhQueryableDataFacade"/> class.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        public NhQueryableDataFacade(IContextProvider<ISession> contextProvider)
        {
            this.ContextProvider = contextProvider;
        }

        /// <summary>
        /// Gets the context provider.
        /// </summary>
        /// <value>
        /// The context provider.
        /// </value>
        protected IContextProvider<ISession> ContextProvider { get; }

        /// <inheritdoc/>
        public TEntity Get<TEntity>(object identifier) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            return session.Get<TEntity>(identifier, LockMode.None);
        }

        /// <inheritdoc/>
        public bool Exists<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            return session.Query<TEntity>().Count(expression) > 0;
        }

        /// <inheritdoc/>
        public TEntity UniqueResult<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            return session.Query<TEntity>().SingleOrDefault(expression);
        }

        /// <inheritdoc/>
        public IEnumerable<TEntity> ApplyWhere<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            var query = session.Query<TEntity>();

            return query.Where(expression).ToList();
        }

        /// <inheritdoc/>
        public IEnumerable<TEntity> ExecuteExpression<TEntity>(Func<IQueryable<TEntity>, IQueryable<TEntity>> queryExpression) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            var query = session.Query<TEntity>();
            
            return queryExpression.Invoke(query).ToList();
        }

        /// <inheritdoc/>
        public TResult ExecuteExpression<TEntity, TResult>(Func<IQueryable<TEntity>, TResult> queryExpression) where TEntity : class
        {
            var session = this.ContextProvider.GetCurrentContext();
            
            var query = session.Query<TEntity>()
                .SetOptions();

            return queryExpression.Invoke(query);
        }

        /// <inheritdoc/>
        public IQueryPaging<TEntity> MakeQueryPaging<TEntity>(Func<IQueryable<TEntity>, IQueryable<TEntity>> queryExpression, int pageSize = 10) where TEntity : class
        {
            return new QueryablePaging<TEntity>(() =>
            {
                var query = this.ContextProvider.GetCurrentContext().Query<TEntity>();
                return queryExpression(query);
            }, pageSize);
        }

        /// <inheritdoc/>
        public IQueryPaging<TResult> MakeQueryPaging<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryExpression, int pageSize = 10) where TEntity : class
        {
            return new QueryablePaging<TResult>(() =>
            {
                var query = this.ContextProvider.GetCurrentContext().Query<TEntity>();
                return queryExpression(query);
            }, pageSize);
        }

        /// <inheritdoc/>
        public IFutureQueryBatch MakeFutureQueryBatch()
        {
            return new NhFutureQueryBatch(this.ContextProvider);
        }
    }
}
