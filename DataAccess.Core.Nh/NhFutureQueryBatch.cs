﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Providers;
using NHibernate;
using NHibernate.Linq;

namespace DataAccess.Core.Nh
{
    /// <summary>
    /// Represents a common way to register queries to execute in a batch way, in one trip.
    /// </summary>
    public class NhFutureQueryBatch : IFutureQueryBatch
    {
        private readonly IContextProvider<ISession> contextProvider;
        private readonly IList<FutureQueryItem> futureCollection;
        private static readonly Type genericDefFutureValue = typeof(IFutureValue<>);
        private static readonly Type CollectionBaseType = typeof(System.Collections.IEnumerable);

        /// <summary>
        /// Creates a new <see cref="NhFutureQueryBatch"/> instance.
        /// </summary>
        /// <param name="contextProvider"></param>
        public NhFutureQueryBatch(IContextProvider<ISession> contextProvider)
        {
            this.contextProvider = contextProvider;
            futureCollection = new List<FutureQueryItem>();
        }

        /// <inheritdoc/>
        public string AddFuture<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryFunc)
            where TEntity : class
        {
            var key = Guid.NewGuid().ToString("D");

            this.AddFuture(queryFunc, key);

            return key;
        }

        /// <inheritdoc/>
        public IFutureQueryBatch AddFuture<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryFunc, string key)
            where TEntity : class
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("The given future query cannot be associated to a null or empty string key.", nameof(key));
            }

            if (this.futureCollection.Any(descriptor => descriptor.Key.Equals(key)))
            {
                throw new InvalidOperationException($"The given key is already used by another future query, key: {key}");
            }

            var session = this.contextProvider.GetCurrentContext();

            var query = queryFunc.Invoke(session.GetQuery<TEntity>())
                .ToFuture();

            this.futureCollection.Add(new FutureQueryItem
            {
                Key = key,
                ResultType = typeof(IEnumerable<TResult>),
                FutureQuery = query
            });

            return this;
        }
        
        /// <inheritdoc/>
        public string AddFutureValue<TEntity, TResult>(Expression<Func<IQueryable<TEntity>, TResult>> expression)
            where TEntity : class
        {
            var key = Guid.NewGuid().ToString("D");

            this.AddFutureValue(expression, key);

            return key;
        }

        /// <inheritdoc/>
        public IFutureQueryBatch AddFutureValue<TEntity, TResult>(Expression<Func<IQueryable<TEntity>, TResult>> expression, string key)
            where TEntity : class
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("The given future query cannot be associated to a null or empty string key.", nameof(key));
            }

            if (this.futureCollection.Any(descriptor => descriptor.Key.Equals(key)))
            {
                throw new InvalidOperationException($"The given key is already used by another future query, key: {key}");
            }

            var session = this.contextProvider.GetCurrentContext();
            
            var query = session.GetQuery<TEntity>()
                .ToFutureValue(expression);

            this.futureCollection.Add(new FutureQueryItem
            {
                Key = key,
                ResultType = typeof(IFutureValue<TResult>),
                FutureQuery = query
            });

            return this;
        }

        /// <inheritdoc/>
        public IList<TResult> GetResult<TResult>(string key)
            where TResult : class
        {
            var future = this.futureCollection.FirstOrDefault(descriptor => descriptor.Key.Equals(key));

            if (future == null)
            {
                throw new ArgumentException($"A future collection result with the given key doesn't exist, key: {key}", nameof(key));
            }

            if (future.FutureQuery is IEnumerable<TResult> futureValue)
            {
                return futureValue.ToList();
            }

            throw new InvalidOperationException("The given result type doesn't match with future result type.");
        }

        /// <inheritdoc/>
        public TResult GetValueResult<TResult>(string key)
        {
            var future = this.futureCollection.FirstOrDefault(descriptor => descriptor.Key.Equals(key));

            if (future == null)
            {
                throw new ArgumentException($"A future result with the given key doesn't exist, key: {key}", nameof(key));
            }

            if (future.FutureQuery is IFutureValue<TResult> futureValue)
            {
                return futureValue.Value;
            }

            throw new InvalidOperationException("The given result type doesn't match with future result type.");
        }

        /// <inheritdoc/>
        public IList<string> GetKeys()
        {
            return this.futureCollection.Select(descriptor => descriptor.Key)
                .ToList();
        }

        /// <inheritdoc/>
        public void Execute()
        {
            var valueItem = futureCollection.FirstOrDefault(item => item.ResultType.GetGenericTypeDefinition() == genericDefFutureValue);

            if (valueItem != null)
            {
                var value = GetResult(valueItem.FutureQuery);

                return;
            }

            var collectionItem = futureCollection.FirstOrDefault(item => CollectionBaseType.IsAssignableFrom(item.ResultType));

            if (collectionItem != null)
            {
                var futureQuery = collectionItem.FutureQuery;
                var futureValue = Enumerable.ToList(futureQuery);
            }
        }

        /// <inheritdoc/>
        public void Reset()
        {
            this.futureCollection.Clear();
        }

        /// <summary>
        /// A workaround for avoiding dynamic value resolution.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="futureValue"></param>
        /// <returns></returns>
        private static TResult GetResult<TResult>(IFutureValue<TResult> futureValue)
        {
            return futureValue.Value;
        }
    }
}
