﻿using System.Collections.Generic;

namespace DataAccess.Core
{
    /// <summary>
    /// 
    /// </summary>
    // todo: to implement
    public interface IQueryPageMaterializer<out TResult>
        : IQueryDescriptor
    {
        /// <summary>
        /// Gets the page size for this instance.
        /// </summary>
        int PageSize { get; }

        /// <summary>
        /// Executes the underlying query and return a collection of TResult type, considering the pageIndex parameters for paging results.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        IEnumerable<TResult> Execute(int pageIndex);
    }
}
