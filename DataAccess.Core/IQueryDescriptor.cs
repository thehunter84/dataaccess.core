﻿using System.Collections.Generic;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a generic executable query.
    /// </summary>
    public interface IQueryDescriptor
    {
        /// <summary>
        /// Gets the query name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets the parameters defined in this instance.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        IEnumerable<IQueryArgument> Parameters { get; }

        /// <summary>
        /// Determines whether exists a query argument with the given name..
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <returns></returns>
        bool Contains(string parameterName);

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        IQueryDescriptor SetParameter<TValue>(string parameterName, TValue value);

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        IQueryDescriptor SetParameter(string parameterName, object value);

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="parameter">The parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        IQueryDescriptor SetParameter<TValue>(IQueryArgument parameter, TValue value);

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        IQueryDescriptor SetParameter(IQueryArgument parameter, object value);
    }
}
