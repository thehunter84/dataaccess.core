﻿using System;
using System.Diagnostics;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a future query related to unique key.
    /// </summary>
    [DebuggerDisplay("key: {Key}, futureQuery: {FutureQuery}")]
    public class FutureQueryItem
    {
        /// <summary>
        /// Gets or sets the key for this instance
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the kind of type of future value
        /// </summary>
        public Type ResultType { get; set; }

        /// <summary>
        /// Gets or sets the real instance related to Future instance
        /// </summary>
        public dynamic FutureQuery { get; set; }
    }
}
