﻿namespace DataAccess.Core
{
    /// <summary>
    /// Represents a query executable command.
    /// </summary>
    /// <seealso cref="IQueryDescriptor" />
    public interface IQueryCommand
        : IQueryDescriptor
    {
        /// <summary>
        /// Executes the internal command, and return a number of instances were affected.
        /// </summary>
        /// <returns></returns>
        int Execute();
    }
}
