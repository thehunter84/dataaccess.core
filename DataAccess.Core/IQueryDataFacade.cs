﻿namespace DataAccess.Core
{
    /// <summary>
    /// Represents a generic facade used as engine query executor.
    /// </summary>
    /// <seealso cref="ITransactionalDataFacade" />
    /// <seealso cref="System.IDisposable" />
    public interface IQueryDataFacade
        : ITransactionalDataFacade
    {
        /// <summary>
        /// Gets the query command.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <returns></returns>
        IQueryCommand GetQueryCommand(string namedQuery);

        /// <summary>
        /// Gets the query materializer.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <returns></returns>
        IQueryMaterializer GetQueryMaterializer(string namedQuery);
    }
}
