﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a query definition used for compiling <see cref="IQueryDescriptor"/> instances.
    /// </summary>
    [DebuggerDisplay("name: {Name}, type: {QueryType}")]
    public class QueryDefinition
    {
        //private const string ParameterSearcherPattern = @"\@\w+";
        private const string ParameterSearcherPattern = @"\:\w+";

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryDefinition"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="statement">The statement.</param>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="parameters">The parameters.</param>
        public QueryDefinition(string name, string statement, QueryType queryType, IEnumerable<IQueryArgument> parameters = null)
        {
            name.ThrowException(string.IsNullOrWhiteSpace, s => new QueryParameterException("The given query name cannot be null or empty.", "name"));
            statement.ThrowException(string.IsNullOrWhiteSpace, s => new QueryParameterException("The query statement cannot be null or empty.", "statement"));
            
            var statementArguments = GetParametersOnStatement(statement);
            var currentParameters = GetArguments(InitAndVerifyArguments(statementArguments, StringComparer.InvariantCultureIgnoreCase),
                InitAndVerifyArguments(parameters ?? Enumerable.Empty<IQueryArgument>(), new QueryArgumentEqualityComparer()));

            this.Name = name;
            this.Statement = statement;
            this.QueryType = queryType;
            this.Parameters = currentParameters;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets the statement.
        /// </summary>
        /// <value>
        /// The statement.
        /// </value>
        public string Statement { get; }

        /// <summary>
        /// Gets the type of the query.
        /// </summary>
        /// <value>
        /// The type of the query.
        /// </value>
        public QueryType QueryType { get; }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IEnumerable<IQueryArgument> Parameters { get; }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj is QueryDefinition) return this.GetHashCode() == obj.GetHashCode();

            return false;
        }

        /// <summary>
        /// Gets the parameters on statement.
        /// </summary>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <returns></returns>
        private static IEnumerable<string> GetParametersOnStatement(string sqlStatement)
        {
            return Regex.Matches(sqlStatement, ParameterSearcherPattern, RegexOptions.CultureInvariant)
                .Cast<Match>()
                .Select(match => match.Value)
                .Distinct(StringComparer.CurrentCultureIgnoreCase)
                .ToList();
        }

        /// <summary>
        /// Gets the arguments.
        /// </summary>
        /// <param name="statementArguments">The statement arguments.</param>
        /// <param name="queryArguments">The query arguments.</param>
        /// <returns></returns>
        private static List<IQueryArgument> GetArguments(IEnumerable<string> statementArguments, List<IQueryArgument> queryArguments)
        {
            var arguments = new List<IQueryArgument>();

            foreach (var statementParam in statementArguments)
            {
                //var current = statementParam.Replace("@", string.Empty);
                var current = statementParam.Replace(":", string.Empty);

                var queryParam =
                    queryArguments.FirstOrDefault(
                        argument => argument.Name.Equals(current, StringComparison.InvariantCultureIgnoreCase));

                arguments.Add(queryParam ?? QueryArgument.MakeInstance(current));
            }

            return arguments;
        }

        /// <summary>
        /// Initializes the and verify arguments.
        /// </summary>
        /// <typeparam name="TInput">The type of the input.</typeparam>
        /// <param name="parameters">The parameters.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns></returns>
        private static List<TInput> InitAndVerifyArguments<TInput>(IEnumerable<TInput> parameters, IEqualityComparer<TInput> comparer)
        {
            var listParams = parameters.ToList();

            if (!listParams.Any())
            {
                return new List<TInput>();
            }

            var counter = listParams.Count();
            var querySet = new HashSet<TInput>(listParams, comparer);

            var numDuplicates = counter - querySet.Count();
            numDuplicates.ThrowException(i => i > 0, i => new QueryParameterException(
                $"There are some equal parameters in input, num. duplicates: {numDuplicates}", "parameters"));

            return querySet.ToList();
        }
    }
}
