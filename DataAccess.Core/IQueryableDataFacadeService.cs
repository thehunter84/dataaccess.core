﻿namespace DataAccess.Core
{
    /// <summary>
    /// Represents an aggregation of data extraction and persistent logic.
    /// </summary>
    /// <seealso cref="IQueryableDataFacade" />
    /// <seealso cref="IPersisterDataFacade" />
    public interface IQueryableDataFacadeService
        : IQueryableDataFacade, IPersisterDataFacade
    {
    }
}
