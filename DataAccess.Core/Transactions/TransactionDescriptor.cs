﻿using System.Diagnostics;
using System.Transactions;

namespace DataAccess.Core.Transactions
{
    /// <summary>
    /// Represents a generic transaction descriptor.
    /// </summary>
    [DebuggerDisplay("Name: {Name}, Isolation: {Isolation}")]
    public class TransactionDescriptor : ITransactionInfo
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the isolation level to use.
        /// </summary>
        /// <value>
        /// The isolation.
        /// </value>
        public IsolationLevel? Isolation { get; set; }
    }
}
