﻿using System;
using System.Diagnostics;
using System.Dynamic;
using System.Transactions;
using DataAccess.Core.Exceptions;

namespace DataAccess.Core.Transactions
{
    /// <summary>
    /// Represents a generic default transaction.
    /// </summary>
    /// <seealso cref="ITransactionWorker" />
    [DebuggerDisplay("Target: {Name}, Status: {Status}, Disposed: {disposed}")]
    public class TransactionWorkerImpl
        : ITransactionWorker
    {
        private readonly TransactionDescriptor descriptor;
        private readonly Action<dynamic, TransactionDescriptor> onBegin;
        private readonly Action<dynamic, TransactionDescriptor> onCommit;
        private readonly Action<dynamic, TransactionDescriptor> onRollback;
        private readonly Action<dynamic, TransactionDescriptor> onDispose;
        private readonly dynamic dynamicField = new ExpandoObject();
        
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionWorkerImpl"/> class.
        /// </summary>
        /// <param name="descriptor">The current descriptor instance for this given transaction.</param>
        /// <param name="onBegin">The begin action to execute.</param>
        /// <param name="onCommit">The commit action to execute.</param>
        /// <param name="onRollback">The rollback action to execute.</param>
        /// <param name="onDispose">The dispose action to execute.</param>
        /// <param name="initializer">The initializer action for this instance.</param>
        /// <param name="begin">if set to <c>true</c> begins the current transaction.</param>
        public TransactionWorkerImpl(TransactionDescriptor descriptor,
            Action<dynamic, TransactionDescriptor> onBegin,
            Action<dynamic, TransactionDescriptor> onCommit,
            Action<dynamic, TransactionDescriptor> onRollback,
            Action<dynamic, TransactionDescriptor> onDispose,
            Action<dynamic> initializer = null,
            bool begin = true)
        {
            this.descriptor = descriptor;
            this.onBegin = onBegin;
            this.onCommit = onCommit;
            this.onRollback = onRollback;
            this.onDispose = onDispose;
            this.disposed = false;

            initializer?.Invoke(this.dynamicField);

            if (begin)
            {
                this.Begin();
            }
        }

        /// <summary>
        /// Gets the current status of this transaction.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public TransactionStatus Status { get; private set; } = TransactionStatus.WaitingFor;

        /// <summary>
        /// Gets the target oif this transaction.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name => this.descriptor.Name;

        /// <summary>
        /// Gets or sets the isolation level used into underlying transaction.
        /// </summary>
        /// <value>
        /// The isolation.
        /// </value>
        public IsolationLevel? Isolation => this.descriptor.Isolation;

        /// <summary>
        /// Begins this instance.
        /// </summary>
        /// <exception cref="TransactionStatusException">Impossible to commit the given transaction due to its status.</exception>
        public void Begin()
        {
            this.EnsureNoDisposed(nameof(this.Begin));

            if (this.Status == TransactionStatus.WaitingFor)
            {
                this.onBegin(this.dynamicField, this.descriptor);
                this.Status = TransactionStatus.InProgress;
            }
            else
            {
                throw new TransactionStatusException("Impossible to begin the given transaction due to its status.",
                    this.Status);
            }
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <exception cref="TransactionStatusException">Impossible to commit the given transaction due to its status.</exception>
        public virtual void Commit()
        {
            this.EnsureNoDisposed(nameof(this.Commit));

            if (this.Status == TransactionStatus.InProgress)
            {
                try
                {
                    this.onCommit(this.dynamicField, this.descriptor);
                    this.Status = TransactionStatus.Committed;
                }
                catch (Exception)
                {
                    this.Status = TransactionStatus.CommitError;
                    throw;
                }
            }
            else
            {
                throw new TransactionStatusException("Impossible to commit the given transaction due to its status.",
                    this.Status);
            }
            
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        /// <exception cref="TransactionStatusException">Impossible to make a rollback the the current transaction due to its status.</exception>
        public virtual void Rollback()
        {
            this.EnsureNoDisposed(nameof(this.Rollback));
            
            switch (this.Status)
            {
                case TransactionStatus.InProgress:
                case TransactionStatus.CommitError:
                {
                    this.InternalRollback();
                    break;
                }
                default:
                {
                    throw new TransactionStatusException("Impossible to make a rollback the the current transaction due to its status.", this.Status);
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.EnsureNoDisposed(nameof(this.Dispose));

            try
            {
                this.onDispose(this.dynamicField, this.descriptor);
            }
            catch (Exception)
            {
                // nothing to do...
            }

            try
            {
                if (this.Status == TransactionStatus.InProgress)
                {
                    this.InternalRollback();
                }
                
                this.disposed = true;
            }
            catch (Exception)
            {
                this.disposed = true;
                throw;
            }
        }

        /// <summary>
        /// Ensures if the current transaction isn't disposed.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <exception cref="System.ObjectDisposedException"></exception>
        private void EnsureNoDisposed(string operation)
        {
            if (this.disposed)
                throw new ObjectDisposedException(
                    $"The current transaction cannot be used anymore because It was disposed, operation: {operation}");
        }

        private void InternalRollback()
        {
            try
            {
                this.onRollback(this.dynamicField, this.descriptor);
                this.Status = TransactionStatus.RolledBack;
            }
            catch (Exception)
            {
                this.Status = TransactionStatus.RollbackError;
                throw;
            }
        }
    }
}
