﻿using System.Transactions;

namespace DataAccess.Core.Transactions
{
    /// <summary>
    /// Represents a generic information about a transaction.
    /// </summary>
    public interface ITransactionInfo
    {
        /// <summary>
        /// Gets the target.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets or sets the isolation level to use.
        /// </summary>
        /// <value>
        /// The isolation.
        /// </value>
        IsolationLevel? Isolation { get; }
    }
}
