﻿using System;

namespace DataAccess.Core.Transactions
{
    /// <summary>
    /// Represents a generic transaction.
    /// </summary>
    public interface ITransactionWorker
        : ITransactionInfo, IDisposable
    {
        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        TransactionStatus Status { get; }

        /// <summary>
        /// Begins this instance.
        /// </summary>
        void Begin();

        /// <summary>
        /// Commits this instance.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        void Rollback();
    }
}
