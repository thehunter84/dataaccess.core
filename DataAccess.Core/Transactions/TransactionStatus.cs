﻿namespace DataAccess.Core.Transactions
{
    /// <summary>
    /// Represents an specific transaction status.
    /// </summary>
    public enum TransactionStatus
    {
        /// <summary>
        /// The waiting for being executed.
        /// </summary>
        WaitingFor = 0,

        /// <summary>
        /// Indicates a transaction is in progress
        /// </summary>
        InProgress = 1,

        /// <summary>
        /// Indicates a transaction was rolled-back.
        /// </summary>
        RolledBack = 3,

        /// <summary>
        /// Indicates a transaction was committed.
        /// </summary>
        Committed = 4,

        /// <summary>
        /// Indicates when a transaction was committed, but It has thrown an error.
        /// </summary>
        CommitError = 8,

        /// <summary>
        /// Indicates when a transaction was rollbacked, but It has thrown an error.
        /// </summary>
        RollbackError = 10
    }
}
