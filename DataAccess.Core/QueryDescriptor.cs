﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a base implementation for query descriptors.
    /// </summary>
    /// <seealso cref="IQueryDescriptor" />
    public abstract class QueryDescriptor
        : IQueryDescriptor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryDescriptor"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="statement">The statement.</param>
        /// <param name="parameters">The parameters.</param>
        protected QueryDescriptor(string name, string statement, IEnumerable<IQueryArgument> parameters)
        {
            this.Name = name;
            this.Statement = statement;
            this.Parameters = parameters.ToList();
        }

        /// <summary>
        /// Gets the query name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets the statement.
        /// </summary>
        /// <value>
        /// The statement.
        /// </value>
        protected string Statement { get; }

        /// <summary>
        /// Gets the parameters defined in this instance.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IEnumerable<IQueryArgument> Parameters { get; }

        /// <summary>
        /// Determines whether exists a query argument with the given name..
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <returns></returns>
        public bool Contains(string parameterName)
        {
            return
                this.Parameters.Any(
                    argument => argument.Name.Equals(parameterName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public virtual IQueryDescriptor SetParameter<TValue>(string parameterName, TValue value)
        {
            return this.SetParameter(parameterName, value as object);
        }

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public virtual IQueryDescriptor SetParameter(string parameterName, object value)
        {
            var parameter = this.Parameters.FirstOrDefault(
                argument => argument.Name.Equals(parameterName, StringComparison.InvariantCultureIgnoreCase));

            return this.SetParameter(parameter, value);
        }

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="parameter">The parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public virtual IQueryDescriptor SetParameter<TValue>(IQueryArgument parameter, TValue value)
        {
            return this.SetParameter(parameter, value as object);
        }

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public abstract IQueryDescriptor SetParameter(IQueryArgument parameter, object value);
    }
}
