﻿using System;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// A generic exception that describe reason about generic error on facade layer.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class DataAccessException
        : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataAccessException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public DataAccessException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataAccessException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        public DataAccessException(string message, string invokerName)
            : base(message)
        {
            this.InvokerName = invokerName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataAccessException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public DataAccessException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataAccessException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="innerException">The inner exception.</param>
        public DataAccessException(string message, string invokerName, Exception innerException)
            : base(message, innerException)
        {
            this.InvokerName = invokerName;
        }

        /// <summary>
        /// Gets the name of the invoker.
        /// </summary>
        /// <value>
        /// The name of the invoker.
        /// </value>
        public string InvokerName { get; }
    }
}
