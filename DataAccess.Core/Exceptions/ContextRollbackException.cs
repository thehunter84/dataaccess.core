﻿using System;
using DataAccess.Core.Transactions;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an exception whenever there's a error on rollback action. 
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class ContextRollbackException
        : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContextRollbackException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        public ContextRollbackException(string message, ITransactionInfo transactionInfo)
            : base(message, "RollbackTransaction")
        {
            this.TransactionInfo = transactionInfo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextRollbackException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cause">The cause.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        public ContextRollbackException(string message, Exception cause, ITransactionInfo transactionInfo)
            : base(message, "RollbackTransaction")
        {
            this.TransactionInfo = transactionInfo;
            this.Cause = cause;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextRollbackException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        /// <param name="innerException">The inner exception.</param>
        public ContextRollbackException(string message, ITransactionInfo transactionInfo, Exception innerException)
            : base(message, "RollbackTransaction", innerException)
        {
            this.TransactionInfo = transactionInfo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextRollbackException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cause">The cause.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        /// <param name="innerException">The inner exception.</param>
        public ContextRollbackException(string message, Exception cause, ITransactionInfo transactionInfo, Exception innerException)
            : base(message, "RollbackTransaction", innerException)
        {
            this.TransactionInfo = transactionInfo;
            this.Cause = cause;
        }

        /// <summary>
        /// Gets the transaction information.
        /// </summary>
        /// <value>
        /// The transaction information.
        /// </value>
        public ITransactionInfo TransactionInfo { get; }

        /// <summary>
        /// Gets the cause.
        /// </summary>
        /// <value>
        /// The cause.
        /// </value>
        public Exception Cause { get; }
    }
}
