﻿using System;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an exception whenever a commit fails.
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class CommitFailedException
        : FlushFailedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommitFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        public CommitFailedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        /// <param name="invokerName">The name of invoker</param>
        public CommitFailedException(string message, string invokerName)
            : base(message, invokerName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        /// <param name="innerException">The inner exception belongs to this current exception.</param>
        public CommitFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:DataAccess.Core.Exceptions.CommitFailedException" /> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        /// <param name="invokerName">The name of invoker</param>
        /// <param name="innerException">The inner exception belongs to this current exception.</param>
        public CommitFailedException(string message, string invokerName, Exception innerException)
            : base(message, invokerName, innerException)
        {
        }
    }
}
