﻿using System;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an exception whenever there's no query definition to manage.
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class NoQueryDefinitionException
        : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoQueryDefinitionException"/> class.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <param name="message">The message.</param>
        public NoQueryDefinitionException(string namedQuery, string message)
            : base(message)
        {
            this.NamedQuery = namedQuery;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoQueryDefinitionException"/> class.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        public NoQueryDefinitionException(string namedQuery, string message, string invokerName)
            : base(message, invokerName)
        {
            this.NamedQuery = namedQuery;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoQueryDefinitionException"/> class.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public NoQueryDefinitionException(string namedQuery, string message, Exception innerException)
            : base(message, innerException)
        {
            this.NamedQuery = namedQuery;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoQueryDefinitionException"/> class.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="innerException">The inner exception.</param>
        public NoQueryDefinitionException(string namedQuery, string message, string invokerName, Exception innerException)
            : base(message, invokerName, innerException)
        {
            this.NamedQuery = namedQuery;
        }

        /// <summary>
        /// Gets the named query.
        /// </summary>
        /// <value>
        /// The named query.
        /// </value>
        public string NamedQuery { get; }
    }
}
