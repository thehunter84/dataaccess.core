﻿using System;
using DataAccess.Core.Transactions;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an exception whenever a rollback fails.
    /// </summary>
    /// <seealso cref="ContextRollbackException" />
    public class RollbackFailedException
        : ContextRollbackException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RollbackFailedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        public RollbackFailedException(string message, ITransactionInfo transactionInfo)
            : base(message, transactionInfo)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RollbackFailedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cause">The cause.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        public RollbackFailedException(string message, Exception cause, ITransactionInfo transactionInfo)
            : base(message, cause, transactionInfo)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RollbackFailedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        /// <param name="innerException">The inner exception.</param>
        public RollbackFailedException(string message, ITransactionInfo transactionInfo, Exception innerException)
            : base(message, transactionInfo, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RollbackFailedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cause">The cause.</param>
        /// <param name="transactionInfo">The transaction information.</param>
        /// <param name="innerException">The inner exception.</param>
        public RollbackFailedException(string message, Exception cause, ITransactionInfo transactionInfo, Exception innerException)
            : base(message, cause, transactionInfo, innerException)
        {
        }
    }
}
