﻿using System;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents a generic exception for named queries.
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class InvalidQueryException
        : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidQueryException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public InvalidQueryException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidQueryException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        public InvalidQueryException(string message, string invokerName)
            : base(message, invokerName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidQueryException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public InvalidQueryException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidQueryException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="innerException">The inner exception.</param>
        public InvalidQueryException(string message, string invokerName, Exception innerException)
            : base(message, invokerName, innerException)
        {
        }
    }
}
