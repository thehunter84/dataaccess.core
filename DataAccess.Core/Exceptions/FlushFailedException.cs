﻿using System;
using System.Collections.Generic;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an exception whenever a flush action fails.
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class FlushFailedException
        : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FlushFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        public FlushFailedException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlushFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        /// <param name="invokerName">The name of invoker</param>
        public FlushFailedException(string message, string invokerName) : base(message, invokerName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlushFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        /// <param name="innerException">The inner exception belongs to this current exception.</param>
        public FlushFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlushFailedException"/> class.
        /// </summary>
        /// <param name="message">The message for this exception</param>
        /// <param name="invokerName">The name of invoker</param>
        /// <param name="innerException">The inner exception belongs to this current exception.</param>
        public FlushFailedException(string message, string invokerName, Exception innerException) : base(message, invokerName, innerException)
        {
        }

        /// <summary>
        /// Gets or sets the validation errors.
        /// </summary>
        /// <value>
        /// The validation errors.
        /// </value>
        public IList<string> ValidationErrors { get; set; }
    }
}
