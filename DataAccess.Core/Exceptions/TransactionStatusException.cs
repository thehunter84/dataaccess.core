﻿using System;
using DataAccess.Core.Transactions;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an error due to a wrong transaction status.
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class TransactionStatusException
        : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionStatusException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="status">The status.</param>
        public TransactionStatusException(string message, TransactionStatus status) : base(message)
        {
            this.Status = status;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionStatusException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="status">The status.</param>
        public TransactionStatusException(string message, string invokerName, TransactionStatus status)
            : base(message, invokerName)
        {
            this.Status = status;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionStatusException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="status">The status.</param>
        public TransactionStatusException(string message, Exception innerException, TransactionStatus status)
            : base(message, innerException)
        {
            this.Status = status;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionStatusException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="status">The status.</param>
        public TransactionStatusException(string message, string invokerName, Exception innerException, TransactionStatus status)
            : base(message, invokerName, innerException)
        {
            this.Status = status;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public TransactionStatus Status { get; }
    }
}
