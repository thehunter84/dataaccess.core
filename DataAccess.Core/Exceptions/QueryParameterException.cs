﻿using System;

namespace DataAccess.Core.Exceptions
{
    /// <summary>
    /// Represents an exception about query parameter.
    /// </summary>
    /// <seealso cref="DataAccessException" />
    public class QueryParameterException
         : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParameterException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="parameter">The parameter.</param>
        public QueryParameterException(string message, string parameter)
            : base(message)
        {
            this.Parameter = parameter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParameterException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="parameter">The parameter.</param>
        public QueryParameterException(string message, string invokerName, string parameter)
            : base(message, invokerName)
        {
            this.Parameter = parameter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParameterException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="parameter">The parameter.</param>
        public QueryParameterException(string message, Exception innerException, string parameter)
            : base(message, innerException)
        {
            this.Parameter = parameter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParameterException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="invokerName">Name of the invoker.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="parameter">The parameter.</param>
        public QueryParameterException(string message, string invokerName, Exception innerException, string parameter)
            : base(message, invokerName, innerException)
        {
            this.Parameter = parameter;
        }

        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <value>
        /// The parameter.
        /// </value>
        public string Parameter { get; }
    }
}
