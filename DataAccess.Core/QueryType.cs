﻿namespace DataAccess.Core
{
    /// <summary>
    /// Represents the kind of query can be executed.
    /// </summary>
    public enum QueryType
    {
        /// <summary>
        /// Indicates that a query materializes the given result into Enumerable.
        /// </summary>
        Materializer = 0,

        /// <summary>
        /// Indicates that a query executes a command into underlying provider.
        /// </summary>
        Command = 3
    }
}
