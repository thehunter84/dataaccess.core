﻿using System;
using System.Diagnostics;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a query argument used by named queries.
    /// </summary>
    /// <seealso cref="IQueryArgument" />
    [DebuggerDisplay("name: {Name}, clrType: {ClrType.FullName}, typename: {TypeName}")]
    public class QueryArgument
        : IQueryArgument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryArgument"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="clrClrType">The type.</param>
        /// <param name="typeName"></param>
        private QueryArgument(string name, Type clrClrType, string typeName)
        {
            name.ThrowException(string.IsNullOrWhiteSpace, s => new QueryParameterException("The given query argument cannot be null or empty", "name"));
            clrClrType.ThrowExceptionIfNull(() => new QueryParameterException("The type of this parameter cannot be null.", "type"));

            this.Name = name.Replace("@", string.Empty);
            this.ClrType = clrClrType;
            this.TypeName = string.IsNullOrWhiteSpace(typeName) ? string.Empty : typeName;
        }

        /// <summary>
        /// Gets the name of this argument.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the CLR type about this argument.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public Type ClrType { get; private set; }

        /// <summary>
        /// Gets the underlying name of this type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; private set; }

        /// <summary>
        /// Makes the instance.
        /// </summary>
        /// <typeparam name="TVal">The type of the value.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static IQueryArgument MakeInstance<TVal>(string name, string typeName = null)
        {
            return new QueryArgument(name, typeof(TVal), typeName);
        }

        /// <summary>
        /// Makes the instance.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="clrType">Name of the type.</param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static IQueryArgument MakeInstance(string name, Type clrType, string typeName = null)
        {
            name.ThrowException(string.IsNullOrWhiteSpace, s => new QueryParameterException("The given query argument cannot be null or empty.", "name"));
            clrType.ThrowExceptionIfNull(() => new QueryParameterException("The given query argument cannot be null or empty.", "clrType"));

            return new QueryArgument(name, clrType, typeName);
        }

        /// <summary>
        /// Makes the instance.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static IQueryArgument MakeInstance(string name, string typeName = null)
        {
            return new QueryArgument(name, typeof(object), typeName);
        }
    }
}
