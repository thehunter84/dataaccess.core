﻿namespace DataAccess.Core
{
    /// <summary>
    /// Represents a custom underlying observer able to inspect instances status.
    /// </summary>
    public interface IContextObserver
    {
        /// <summary>
        /// Determines whether this instance has changes.
        /// </summary>
        /// <returns></returns>
        bool HasChanges();
        
        /// <summary>
        /// Determines whether the given instances are cached into underlying context.
        /// </summary>
        /// <param name="instances">The instances.</param>
        /// <returns></returns>
        bool Cached(params object[] instances);

        /// <summary>
        /// Evicts the specified instances.
        /// </summary>
        /// <param name="instances">The instances.</param>
        void Evict(params object[] instances);

        /// <summary>
        /// Restores the specified instances taking original values from underlying data source.
        /// </summary>
        /// <param name="instances">The instances.</param>
        void Restore(params object[] instances);

        /// <summary>
        /// Removes all cached instance from underlying context.
        /// </summary>
        void Clear();
    }
}
