﻿using System;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents an argument used by named queries.
    /// </summary>
    public interface IQueryArgument
    {
        /// <summary>
        /// Gets the name of ths query.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets the type of this query
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        Type ClrType { get; }

        /// <summary>
        /// Gets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        string TypeName { get; }
    }
}
