﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Core.Extensions
{
    /// <summary>
    /// Custom extensions for Type reference.
    /// </summary>
    public static class TypeExtensions
    {
        private static readonly IDictionary<string, Type> FriendlyNames;

        static TypeExtensions()
        {
            FriendlyNames = new Dictionary<string, Type>
                                {
                                    { "object", typeof(object) },
                                    { "string", typeof(string) },
                                    { "bool", typeof(bool) },
                                    { "byte", typeof(byte) },
                                    { "char", typeof(char) },
                                    { "decimal", typeof(decimal) },
                                    { "double", typeof(double) },
                                    { "short", typeof(short) },
                                    { "int", typeof(int) },
                                    { "long", typeof(long) },
                                    { "float", typeof(float) }
                                };

        }

        /// <summary>
        /// Determines whether this instance is collection.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static bool IsCollection(this object instance)
        {
            if (instance == null)
                return false;

            var type = instance.GetType();

            return type.GetInterfaces().Any(type1 => type1 == typeof (IEnumerable));
        }

        /// <summary>
        /// Determines whether this instance is array.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static bool IsArray(this object instance)
        {
            if (instance == null)
                return false;

            var type = instance.GetType();

            return type.IsArray;
        }

        /// <summary>
        /// Determines whether this instance is collection.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static bool IsCollection(this Type type)
        {
            return type.GetInterfaces().Any(type1 => type1 == typeof(IEnumerable)) || type.IsArray;
        }

        /// <summary>
        /// Gets the item type from collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static Type GetItemTypeFromCollection(this object collection)
        {
            var collectionType = collection.GetType();
            Type entityType = null;

            if (collection is string)
            {
                return null;
            }

            if (collectionType.IsArray)
            {
                entityType = collectionType.GetElementType();
            }
            else if (collectionType.IsCollection())
            {
                entityType = collectionType.GetGenericArguments()[0];
            }

            return entityType;
        }

        /// <summary>
        /// Determines whether this instance is complex.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static bool IsComplex(this Type type)
        {
            return (type.IsClass || type.IsInterface) && type != typeof(string);
        }

        /// <summary>
        /// Gets the type from.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Type GetTypeFrom(this string name)
        {
            return FriendlyNames.ContainsKey(name) ? FriendlyNames[name] : Type.GetType(name, true);
        }
    }
}
