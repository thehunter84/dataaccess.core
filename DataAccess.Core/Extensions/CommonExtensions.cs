﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Data;
using System.Transactions;
using DataAccess.Core.Transactions;

namespace DataAccess.Core.Extensions
{
    using TransactionStatus = System.Transactions.TransactionStatus;

    /// <summary>
    /// Class for generic extension methods used in this component.
    /// </summary>
    public static class CommonExtensions
    {
        private static readonly ConcurrentDictionary<Type, PropertyDescriptorCollection> PropertyTypeDesc;

        static CommonExtensions()
        {
            PropertyTypeDesc = new ConcurrentDictionary<Type, PropertyDescriptorCollection>();
        }

        /// <summary>
        /// Throws the exception if the given instance is null.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="exceptionFunc">The exception function.</param>
        public static void ThrowExceptionIfNull<TInstance, TException>(this TInstance instance, Func<TException> exceptionFunc)
            where TException : Exception
        {
            if (instance == null)
            {
                throw exceptionFunc.Invoke();
            }
        }

        /// <summary>
        /// Throws the exception if the given instance isn't null.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="exceptionFunc">The exception function.</param>
        public static void ThrowExceptionIfNotNull<TInstance, TException>(this TInstance instance, Func<TException> exceptionFunc)
            where TException : Exception
        {
            if (instance != null)
            {
                throw exceptionFunc.Invoke();
            }
        }

        /// <summary>
        /// Throws the defined exception if the condition is true.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="condition">The condition.</param>
        /// <param name="exceptionFunc">The exception function.</param>
        public static void ThrowException<TInstance, TException>(this TInstance instance, Func<TInstance, bool> condition, Func<TInstance, TException> exceptionFunc)
            where TException : Exception
        {
            instance.ThrowExceptionIfNull(() => exceptionFunc.Invoke(instance));

            if (condition.Invoke(instance))
            {
                throw exceptionFunc.Invoke(instance);
            }
        }

        /// <summary>
        /// Transforms the given parameter as sql parameter name.
        /// </summary>
        /// <example>
        /// string sqlpar = "companyId";
        /// Console.Writeline(sqlpar.AsSqlParameterName());
        /// // Prints "@companyId"
        /// </example>
        /// <param name="sqlParameter">The SQL parameter.</param>
        /// <returns></returns>
        public static string AsSqlParameterName(this string sqlParameter)
        {
            sqlParameter.ThrowException(string.IsNullOrWhiteSpace, s => new ArgumentException("", s));

            return sqlParameter.StartsWith("@") ? sqlParameter : $"@{sqlParameter}";
        }

        /// <summary>
        /// Determines whether this instance [can be completed] the specified transaction scope.
        /// </summary>
        /// <param name="transactionScope">The transaction scope.</param>
        /// <returns></returns>
        public static bool CanBeCompleted(this TransactionScope transactionScope)
        {
            var transaction = Transaction.Current;
            return transaction != null && transaction.TransactionInformation.Status == TransactionStatus.Active;
        }

        /// <summary>
        /// Determines whether this instance [can be completed] the specified transaction.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public static bool CanBeCompleted(this Transaction transaction)
        {
            return transaction != null && transaction.TransactionInformation.Status == TransactionStatus.Active;
        }

        /// <summary>
        /// Creates a transaction scope from <see cref="TransactionDescriptor"/> instance.
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <returns></returns>
        public static TransactionScope AsTransactionScope(this TransactionDescriptor descriptor)
        {
            return descriptor.Isolation.HasValue
                ? new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = descriptor.Isolation.Value })
                : new TransactionScope(TransactionScopeOption.Required);
        }

        /// <summary>
        /// Returns a data table if the given instance is a collection of complex data.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns>returns a datatable if the given collection has a complex data.</returns>
        /// <remarks>
        /// A complex data can be an anonymous type.
        /// </remarks>
        public static DataTable AsDataTable(this object list)
        {
            if (list is DataTable)
            {
                return list as dynamic;
            }
            
            var entityType = list.GetItemTypeFromCollection();

            if (entityType == null)
            {
                return null;
            }

            var properties = PropertyDescriptors(entityType);
            DataTable table = properties.CreateTable();
            
            IEnumerable col = list as dynamic;

            foreach (var item in col)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        private static DataTable CreateTable(this PropertyDescriptorCollection properties)
        {
            // DataTable table = new DataTable(entityType.Name);
            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            return table;
        }

        private static PropertyDescriptorCollection PropertyDescriptors(Type entityType)
        {
            PropertyDescriptorCollection properties;

            if (!PropertyTypeDesc.TryGetValue(entityType, out properties))
            {
                properties = TypeDescriptor.GetProperties(entityType);
                PropertyTypeDesc.TryAdd(entityType, properties);
            }

            return properties;
        }
    }
}
