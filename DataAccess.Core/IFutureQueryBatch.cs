﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a common contract used to register queries to execute in a batch way, in one trip.
    /// </summary>
    public interface IFutureQueryBatch
    {
        /// <summary>
        /// Adds a new future query which returns a future collection values.
        /// </summary>
        /// <typeparam name="TEntity">The entity type related to the given queryable function</typeparam>
        /// <typeparam name="TResult">The type of <see cref="IQueryable{TResult}"/> instance used to future evaluation</typeparam>
        /// <param name="queryFunc">A common function used to retrieve futures instances</param>
        /// <returns>the unique identifier related to future query</returns>
        string AddFuture<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryFunc)
            where TEntity : class;

        /// <summary>
        /// Adds a new future query which returns a future collection values.
        /// </summary>
        /// <typeparam name="TEntity">The entity type related to the given queryable function</typeparam>
        /// <typeparam name="TResult">The type of <see cref="IQueryable{TResult}"/> instance used to future evaluation</typeparam>
        /// <param name="queryFunc">A common function used to retrieve futures instances</param>
        /// <param name="key">An unique identifier for this future query</param>
        /// <returns>this instance</returns>
        IFutureQueryBatch AddFuture<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryFunc, string key)
            where TEntity : class;

        /// <summary>
        /// Adds a new future query which returns a future value.
        /// </summary>
        /// <typeparam name="TEntity">The entity type related to the given queryable function</typeparam>
        /// <typeparam name="TResult">The type of result used to future evaluation</typeparam>
        /// <param name="expression">A common expression used to retrieve a future result</param>
        /// <returns>the unique identifier related to future query</returns>
        string AddFutureValue<TEntity, TResult>(Expression<Func<IQueryable<TEntity>, TResult>> expression)
            where TEntity : class;

        /// <summary>
        /// Adds a new future query which returns a future value.
        /// </summary>
        /// <typeparam name="TEntity"> entity type related to the given queryable function</typeparam>
        /// <typeparam name="TResult">The type of result used to future evaluation</typeparam>
        /// <param name="expression">A common expression used to retrieve a future result</param>
        /// <param name="key">An unique identifier for this future query</param>
        /// <returns>this instance</returns>
        IFutureQueryBatch AddFutureValue<TEntity, TResult>(Expression<Func<IQueryable<TEntity>, TResult>> expression, string key)
            where TEntity : class;

        /// <summary>
        /// Gets the future collection result related to the given identifier.
        /// </summary>
        /// <typeparam name="TResult">The type of collection result</typeparam>
        /// <param name="key">An unique identifier for this future result</param>
        /// <returns>A list of future query result</returns>
        IList<TResult> GetResult<TResult>(string key)
            where TResult : class;

        /// <summary>
        /// Gets the future result related to the given identifier.
        /// </summary>
        /// <typeparam name="TResult">The future result</typeparam>
        /// <param name="key">An unique identifier for this future result</param>
        /// <returns>Get a future result</returns>
        TResult GetValueResult<TResult>(string key);

        /// <summary>
        /// Gets all future query keys.
        /// </summary>
        /// <returns>all future query keys</returns>
        IList<string> GetKeys();

        /// <summary>
        /// Executes all queries inside this batch in one trip. 
        /// </summary>
        void Execute();

        /// <summary>
        /// Removes all future queries.
        /// </summary>
        void Reset();
    }
}
