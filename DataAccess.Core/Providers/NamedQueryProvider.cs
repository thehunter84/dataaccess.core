﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;

namespace DataAccess.Core.Providers
{
    /// <summary>
    /// A basic implementation for INamedQueryProvider.
    /// </summary>
    /// <seealso cref="INamedQueryProvider" />
    public class NamedQueryProvider : INamedQueryProvider
    {
        /// <summary>
        /// Gets or sets the named queries.
        /// </summary>
        /// <value>
        /// The named queries.
        /// </value>
        protected HashSet<QueryDefinition> NamedQueries { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NamedQueryProvider"/> class.
        /// </summary>
        public NamedQueryProvider()
        {
            this.NamedQueries = new HashSet<QueryDefinition>();
        }

        /// <summary>
        /// Gets the named query.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <returns></returns>
        public QueryDefinition GetNamedQuery(string namedQuery)
        {
            return this.NamedQueries.FirstOrDefault(definition => definition.Name.Equals(namedQuery, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Registers the query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        /// <exception cref="DuplicateNameException"></exception>
        public INamedQueryProvider RegisterQuery(QueryDefinition query)
        {
            query.ThrowExceptionIfNull(() => new NoQueryDefinitionException(string.Empty, "QueryDefinition must be referenced."));

            if (!this.NamedQueries.Add(query))
            {
                throw new DuplicateNameException($"There's a named query definition with the given name, {query.Name}");
            }

            return this;
        }
    }
}
