﻿using System;

namespace DataAccess.Core.Providers
{
    /// <summary>
    /// A contract used to manage contexts.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    /// <seealso cref="System.IDisposable" />
    public interface IContextProvider<out TContext> : IDisposable
        where TContext : class
    {
        /// <summary>
        /// Gets the current context.
        /// </summary>
        /// <returns></returns>
        TContext GetCurrentContext();

        /// <summary>
        /// Renews the current context, disposing (if it's possible) the old one.
        /// </summary>
        void RenewContext();
    }
}
