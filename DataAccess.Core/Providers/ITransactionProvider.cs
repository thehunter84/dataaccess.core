﻿using DataAccess.Core.Transactions;

namespace DataAccess.Core.Providers
{
    /// <summary>
    /// Represents a custom provider used to issue custom context.
    /// </summary>
    public interface ITransactionProvider<out TContext>
        where TContext : class
    {
        /// <summary>
        /// Gets the context provider.
        /// </summary>
        /// <value>
        /// The context provider.
        /// </value>
        IContextProvider<TContext> ContextProvider { get; }
        
        /// <summary>
        /// Builds the transaction.
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <returns></returns>
        ITransactionWorker BuildTransaction(TransactionDescriptor descriptor = null);

        /// <summary>
        /// Exists the specified transaction name.
        /// </summary>
        /// <param name="transactionName">Name of the transaction.</param>
        /// <returns></returns>
        bool Exists(string transactionName);
    }
}
