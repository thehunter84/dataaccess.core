﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Transactions;

namespace DataAccess.Core.Providers
{
    /// <summary>
    /// A basic implementation for generic ITransactionProvider.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    /// <seealso cref="ITransactionProvider{TContext}" />
    [DebuggerDisplay("Num. transactions: {Transactions.Count}")]
    public abstract class TransactionProviderBase<TContext> : ITransactionProvider<TContext>
        where TContext : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionProviderBase{TContext}"/> class.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        protected TransactionProviderBase(IContextProvider<TContext> contextProvider)
        {
            this.ContextProvider = contextProvider;
            this.Transactions = new List<ITransactionWorker>();
        }

        /// <summary>
        /// Verifies if exists a transaction root.
        /// </summary>
        /// <returns></returns>
        protected bool ExistsRootTransaction()
        {
            return this.Transactions.Any() || Transaction.Current != null;
        }

        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <value>
        /// The transactions.
        /// </value>
        protected List<ITransactionWorker> Transactions { get; }

        /// <summary>
        /// Gets the context provider.
        /// </summary>
        /// <value>
        /// The context provider.
        /// </value>
        public IContextProvider<TContext> ContextProvider { get; }
        
        /// <summary>
        /// Builds the transaction.
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <returns></returns>
        public ITransactionWorker BuildTransaction(TransactionDescriptor descriptor = null)
        {
            var context = this.ContextProvider.GetCurrentContext();

            if (descriptor == null)
            {
                descriptor = new TransactionDescriptor { Name = Guid.NewGuid().ToString("D") };
            }
            else
            {
                descriptor.Name ??= Guid.NewGuid().ToString("D");
            }

            descriptor.Name.ThrowException(this.Exists, s => new DataAccessException($"Exists a transaction with the given name, name: {s}"));

            ITransactionWorker info = this.OnBuildTransactionWorker(descriptor, context);

            this.Transactions.Add(info);

            return info;
        }

        /// <summary>
        /// Exists the specified transaction name.
        /// </summary>
        /// <param name="transactionName">Name of the transaction.</param>
        /// <returns></returns>
        public bool Exists(string transactionName)
        {
            return
                this.Transactions.Any(
                    worker => worker.Name.Equals(transactionName, StringComparison.InvariantCultureIgnoreCase));
        }
        
        /// <summary>
        /// Removes the transaction.
        /// </summary>
        /// <param name="transactionName">Name of the transaction.</param>
        /// <returns></returns>
        protected ITransactionWorker RemoveTransaction(string transactionName)
        {
            var transaction =
                this.Transactions.Find(worker => worker.Name.Equals(transactionName, StringComparison.InvariantCultureIgnoreCase));

            this.Transactions.Remove(transaction);

            return transaction;
        }

        /// <summary>
        /// Called when [build transaction worker].
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        protected abstract ITransactionWorker OnBuildTransactionWorker(TransactionDescriptor descriptor, TContext context);
    }
}
