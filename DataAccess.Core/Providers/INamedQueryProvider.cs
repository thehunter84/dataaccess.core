﻿namespace DataAccess.Core.Providers
{
    /// <summary>
    /// Represents a contract which manages query definitions used to work with native or custom query definitions.
    /// </summary>
    public interface INamedQueryProvider
    {
        /// <summary>
        /// Gets the named query.
        /// </summary>
        /// <param name="namedQuery">The named query.</param>
        /// <returns></returns>
        QueryDefinition GetNamedQuery(string namedQuery);

        /// <summary>
        /// Registers the query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        INamedQueryProvider RegisterQuery(QueryDefinition query);
    }
}
