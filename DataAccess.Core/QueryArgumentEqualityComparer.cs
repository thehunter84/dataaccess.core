﻿using System.Collections.Generic;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a common comparer for IQueryArgument instances.
    /// </summary>
    /// <seealso cref="IQueryArgument" />
    public class QueryArgumentEqualityComparer
        : IEqualityComparer<IQueryArgument>
    {
        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <paramref name="x" /> to compare.</param>
        /// <param name="y">The second object of type <paramref name="y" /> to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(IQueryArgument x, IQueryArgument y)
        {
            return x?.Name == y?.Name;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public int GetHashCode(IQueryArgument obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
