﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a data facade which is able to make persistent instances into underlying storage.
    /// </summary>
    public interface IPersisterDataFacade
        : ITransactionalDataFacade, IContextObserver
    {
        /// <summary>
        /// Adds the given instance into underlying store.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Insert<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Adds the given instances into underlying store.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entities"></param>
        void Insert<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

        /// <summary>
        /// Adds the given instance into underlying store.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        object Insert(object entity);

        /// <summary>
        /// Adds the given instances into underlying store.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        void Insert(IEnumerable<object> entities);

        /// <summary>
        /// Makes the persistent the given instance into underlying storage.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>Returns the given instance with a persistence status.</returns>
        TEntity MakePersistent<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Makes persistent the given entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entity.</param>
        void MakePersistent<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

        /// <summary>
        /// Makes the persistent the given instance into underlying storage.
        /// </summary>
        /// <param name="entity">The entity to make persistent.</param>
        /// <returns>Returns the given instance with a persistence status.</returns>
        object MakePersistent(object entity);

        /// <summary>
        /// Makes the persistent.
        /// </summary>
        /// <param name="entities">The entities.</param>
        void MakePersistent(IEnumerable<object> entities);

        /// <summary>
        /// Makes transient an entity associated to the given identifier.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="identifier">The identifier.</param>
        void MakeTransient<TEntity>(object identifier) where TEntity : class;

        /// <summary>
        /// Makes transient the given instance, so deleting it from underlying storage.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>Returns the given instance.</returns>
        TEntity MakeTransient<TEntity>(TEntity entity) where TEntity : class;
        
        /// <summary>
        /// Makes transient the given entities.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entities">The entities.</param>
        void MakeTransient<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

        /// <summary>
        /// Makes transient entities related to the given condition.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="condition">The condition.</param>
        void MakeTransient<TEntity>(Expression<Func<TEntity, bool>> condition) where TEntity : class;
    }
}
