﻿using System.Collections.Generic;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents an executable query which materialize result.
    /// </summary>
    /// <seealso cref="IQueryDescriptor" />
    public interface IQueryMaterializer
         : IQueryDescriptor
    {
        /// <summary>
        /// Executes the underlying query and return a collection of TResult type.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns></returns>
        IEnumerable<TResult> Execute<TResult>();
        
        /// <summary>
        /// Executes the underlying query and returns a a single or default instance, otherwise it throws an exception.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns></returns>
        TResult UniqueResult<TResult>();
    }
}
