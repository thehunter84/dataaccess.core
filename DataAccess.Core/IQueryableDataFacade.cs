﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.Core.Paging;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a generic data facade which is able to extract data from storage. 
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IQueryableDataFacade
    {
        /// <summary>
        /// Gets an instance using the specified identifier.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="identifier">The identifier (key) used to retrieve an instance.</param>
        /// <returns></returns>
        TEntity Get<TEntity>(object identifier)
            where TEntity : class;

        /// <summary>
        /// Verifies if exists instances with the respect the given expression.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="expression">The expression used to understand if exists instances with the given criteria (expression).</param>
        /// <returns></returns>
        bool Exists<TEntity>(Expression<Func<TEntity, bool>> expression)
            where TEntity : class;

        /// <summary>
        /// Gets an unique instance using the given expression.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        TEntity UniqueResult<TEntity>(Expression<Func<TEntity, bool>> expression)
            where TEntity : class;

        /// <summary>
        /// Filters instances using Where clause.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="expression">The expression used as a filter.</param>
        /// <returns>A collection of entities filtered by the given expression.</returns>
        IEnumerable<TEntity> ApplyWhere<TEntity>(Expression<Func<TEntity, bool>> expression)
            where TEntity : class;

        /// <summary>
        /// Gets a collection of instances using the given expression filter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="queryExpression">The queryable function.</param>
        /// <returns></returns>
        IEnumerable<TEntity> ExecuteExpression<TEntity>(Func<IQueryable<TEntity>, IQueryable<TEntity>> queryExpression)
            where TEntity : class;

        /// <summary>
        /// Gets a generic result using the given expression filter.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="queryExpression">The query expr.</param>
        /// <returns></returns>
        TResult ExecuteExpression<TEntity, TResult>(Func<IQueryable<TEntity>, TResult> queryExpression)
            where TEntity : class;

        /// <summary>
        /// Makes the query paging.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        IQueryPaging<TEntity> MakeQueryPaging<TEntity>(Func<IQueryable<TEntity>, IQueryable<TEntity>> queryExpression,
            int pageSize = 10) where TEntity : class;

        /// <summary>
        /// Makes the query paging which can be used for paging result.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        IQueryPaging<TResult> MakeQueryPaging<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryExpression,
            int pageSize = 10) where TEntity : class;

        /// <summary>
        /// Makes a <see cref="IFutureQueryBatch"/> instance.
        /// </summary>
        /// <returns>a <see cref="IFutureQueryBatch"/> instance</returns>
        IFutureQueryBatch MakeFutureQueryBatch();
    }
}
