﻿using System.Transactions;
using DataAccess.Core.Transactions;

namespace DataAccess.Core
{
    /// <summary>
    /// Represents a generic way for making transactions.
    /// </summary>
    public interface ITransactionalDataFacade
    {
        /// <summary>
        /// Begins the transaction using the given parameters.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="isolation">The isolation.</param>
        /// <returns></returns>
        ITransactionWorker BeginTransaction(string name, IsolationLevel? isolation = null);

        /// <summary>
        /// Begins the transaction using the given descriptor.
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <returns></returns>
        ITransactionWorker BeginTransaction(TransactionDescriptor descriptor = null);
    }
}
