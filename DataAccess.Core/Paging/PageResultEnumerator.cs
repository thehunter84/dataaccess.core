﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Core.Paging
{
    /// <summary>
    /// A custom paging result for <see cref="IQueryable{TInstance}"/> instances.
    /// </summary>
    /// <typeparam name="TInstance"></typeparam>
    public class PageResultEnumerator<TInstance> : IEnumerator<PageResult<TInstance>>
    {
        private readonly long totalItems;
        private int index;
        private IQueryPaging queryPaging;
        private Func<IQueryable<TInstance>> queryFunc;

        /// <summary>
        /// Creates a new <see cref="PageResultEnumerator{TInstance}"/> instance.
        /// </summary>
        /// <param name="queryPaging"></param>
        /// <param name="queryFunc"></param>
        public PageResultEnumerator(IQueryPaging queryPaging, Func<IQueryable<TInstance>> queryFunc)
        {
            this.index = 0;
            this.totalItems = queryPaging.TotalItems;
            this.queryPaging = queryPaging;
            this.queryFunc = queryFunc;
        }

        /// <inheritdoc />
        public PageResult<TInstance> Current { get; private set; }

        object IEnumerator.Current => this.Current;

        /// <inheritdoc />
        public bool MoveNext()
        {
            if(!this.IsValidEnumerator())
            {
                throw new InvalidOperationException($"Invalid enumerator because the original items counter has changed, current: {this.queryPaging.TotalItems}, original: {this.totalItems}");
            }

            if (this.index >= this.queryPaging.PageCount)
            {
                return this.MoveNextRare();
            }

            this.Current = new PageResult<TInstance>(this.index, this.GetPageResult(this.index, this.queryPaging.PageSize));

            this.index++;

            return true;
        }

        /// <inheritdoc />
        public void Reset()
        {
            this.index = 0;
            this.Current = null;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.queryPaging = null;
            this.queryFunc = () => Enumerable.Empty<TInstance>().AsQueryable();
        }

        private bool MoveNextRare()
        {
            this.index = this.queryPaging.PageSize + 1;
            this.Current = null;
            return false;
        }

        private List<TInstance> GetPageResult(int pageIndex, int pageSize)
        {
            var startIndex = pageIndex * pageSize;

            return this.queryFunc.Invoke()
                .Skip(startIndex)
                .Take(pageSize)
                .ToList();
        }

        private bool IsValidEnumerator()
        {
            return this.totalItems == this.queryPaging.TotalItems;
        }
    }
}
