﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Core.Paging
{
    /// <summary>
    /// Represents a custom query paging logic to extract data from <see cref="IQueryable{TInstance}"/> functions.
    /// </summary>
    /// <typeparam name="TInstance"></typeparam>
    public class QueryablePaging<TInstance> : IQueryPaging<TInstance>
    {
        private readonly Func<IQueryable<TInstance>> queryFunc;

        /// <summary>
        /// Creates a new <see cref="QueryablePaging{TInstance}"/> instance.
        /// </summary>
        /// <param name="queryFunc"></param>
        /// <param name="pageSize"></param>
        public QueryablePaging(Func<IQueryable<TInstance>> queryFunc, int pageSize)
        {
            this.queryFunc = queryFunc;
            this.PageSize = pageSize;

            //this.PageCountAsLong = this.TotalItems / this.PageSize;

            //if (this.TotalItems % this.PageSize > 0)
            //    this.PageCountAsLong++;
        }

        /// <inheritdoc />
        public long TotalItems => queryFunc().LongCount();

        /// <inheritdoc />
        public int PageSize { get; }

        /// <inheritdoc />
        public int PageCount
        {
            get
            {
                if (this.PageCountAsLong <= int.MaxValue)
                {
                    return Convert.ToInt32(this.PageCountAsLong);
                }

                throw new OverflowException($"The current page counter (value: {this.PageCountAsLong}) cannot be converted to int32");
            }
        }

        /// <inheritdoc />
        public long PageCountAsLong => (this.TotalItems / this.PageSize) + (this.TotalItems % this.PageSize > 0 ? 1 : 0);

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <inheritdoc />
        public IEnumerator<PageResult<TInstance>> GetEnumerator()
        {
            return new PageResultEnumerator<TInstance>(this, this.queryFunc);
        }
    }
}
