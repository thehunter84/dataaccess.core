﻿using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Core.Paging
{
    /// <summary>
    /// Represents a paged result from paging extraction.
    /// </summary>
    /// <typeparam name="TInstance"></typeparam>
    public class PageResult<TInstance>
    {
        /// <summary>
        /// Create a new <see cref="PageResult{TInstance}"/> instance.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="result"></param>
        public PageResult(long index, IEnumerable<TInstance> result)
        {
            this.Index = index;
            this.Result = result.ToList();
        }

        /// <summary>
        /// Gets the result index page about the current page result.
        /// </summary>
        public long Index { get; }

        /// <summary>
        /// Gets the result related to index paging.
        /// </summary>
        public IEnumerable<TInstance> Result { get; }
    }
}
