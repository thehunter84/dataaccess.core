﻿using System.Collections.Generic;

namespace DataAccess.Core.Paging
{
    /// <summary>
    /// Represents a custom contract used to get paged entity collection results.
    /// </summary>
    /// <typeparam name="TInstance">The type of the entity.</typeparam>
    public interface IQueryPaging<TInstance> : IEnumerable<PageResult<TInstance>>, IQueryPaging
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IQueryPaging
    {
        /// <summary>
        /// Gets the total items related to the underlying query made it.
        /// </summary>
        /// <value>
        /// The total items.
        /// </value>
        long TotalItems { get; }

        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        int PageSize { get; }

        /// <summary>
        /// Gets the page count.
        /// </summary>
        /// <value>
        /// The page count.
        /// </value>
        int PageCount { get; }

        /// <summary>
        /// Gets the page count as long.
        /// </summary>
        /// <value>
        /// The page count as long.
        /// </value>
        long PageCountAsLong { get; }
    }
}
