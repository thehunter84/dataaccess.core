﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Nh.Providers;
using DataAccess.Core.Providers;
using DataAccess.Core.Test.Impl.Nh;
using DataAccess.Core.Test.Impl.Nh.Mappings.Fluent;
using NHibernate;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Providers
{
    public class StatelessSessionContextProviderTest
    {
        private readonly ITestOutputHelper output;

        public StatelessSessionContextProviderTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Create()
        {
            var sessionFactory = this.GetSessionFactory();

            IContextProvider<ISession> contextProvider = new StatelessSessionContextProvider(sessionFactory);
            Assert.NotNull(contextProvider);
        }

        [Fact]
        public void GetCurrentContextTest()
        {
            var sessionFactory = this.GetSessionFactory();
            IContextProvider<ISession> contextProvider = new StatelessSessionContextProvider(sessionFactory);

            var session = contextProvider.GetCurrentContext();
            Assert.NotNull(session);
        }

        private ISessionFactory GetSessionFactory()
        {
            var connectionStringProvider = ConnectionStringProvider.Instance;
            var connectionStr = connectionStringProvider.GetConnectionString("devtest");

            var config = new NHibernate.Cfg.Configuration { Interceptor = new XUnitSqlStatementsInterceptor(output) }
                //.Dialect<NHibernate.Dialect.MsSql2012Dialect>()
                .Dialect<NHibernate.Dialect.MsSql2008Dialect>()
                .ConnectionString(connectionStr)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator()
                .AddFluentMappings(typeof(UserMap),
                    typeof(MessageMap),
                    typeof(EntityWithComposedKeyMap),
                    typeof(EntityWithStringKeyMap),
                    typeof(EmployeeMap),
                    typeof(StateEmployeeMap),
                    typeof(PrivateEmployeeMap),
                    typeof(EntityPriceMap),
                    typeof(JobMap));

            return config.BuildSessionFactory();
        }
    }
}
