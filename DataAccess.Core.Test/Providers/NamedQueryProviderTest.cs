﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using DataAccess.Core.Model.DbTest;
using DataAccess.Core.Model.DbTest.Dto;
using DataAccess.Core.Providers;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Providers
{
    /// <summary>
    /// Represents a class for testing named queries for both providers.
    /// </summary>
    public class NamedQueryProviderTest
    {
        private readonly IDataFacadeFactoryProvider devTestDataFacadeFactoryProvider;
        private readonly IDataFacadeFactoryProvider utentiDataFacadeFactoryProvider;

        public NamedQueryProviderTest(ITestOutputHelper output)
        {
            this.devTestDataFacadeFactoryProvider = new DbTestDataFacadeFactoryProvider(output);
            this.utentiDataFacadeFactoryProvider= new DbUtentiDataFacadeFactoryProvider(output);
        }

        [Theory]
        [InlineData(ProviderTypes.NHibernate)]
        [Description("A simple hql query materializer, this test is valid for NHibernate only")]
        public void RegisterHqlQueryTest(ProviderTypes provider)
        {
            var factory = this.devTestDataFacadeFactoryProvider.Build(provider);
            var queryName = "ReadEmployee";

            var queryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "from Employee", QueryType.Materializer));

            var facade = factory.BuildQueryDataFacade(queryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            var result = query.Execute<Employee>();
            Assert.NotNull(result);
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        [Description("A simple sql query materializer.")]
        public void RegisterSqlQueryTest(ProviderTypes provider)
        {
            var factory = this.devTestDataFacadeFactoryProvider.Build(provider);
            var queryName = "ReadComposedkeyEntity";

            var queryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "select * from EntityWithComposedKey", QueryType.Materializer));

            var facade = factory.BuildQueryDataFacade(queryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            var result = query.Execute<EntityWithComposedKey>();
            Assert.NotNull(result);
        }

        [Fact]
        public void RegisterQueryTest()
        {
            var queryName0 = "querycustom0";
            var queryName1 = "querycustom1";

            var provider = new NamedQueryProvider()
                    .RegisterQuery(new QueryDefinition(queryName0, "select * from core.clienti", QueryType.Materializer))
                    .RegisterQuery(new QueryDefinition(queryName1, "select * from core.clienti where id=:idutente", QueryType.Materializer))
                ;

            var query0 = provider.GetNamedQuery(queryName0);
            Assert.NotNull(query0);

            var query1 = provider.GetNamedQuery(queryName1);
            Assert.NotNull(query1);

            Assert.NotNull(query1.Parameters.FirstOrDefault(argument => argument.Name.Equals("idutente", StringComparison.InvariantCultureIgnoreCase)));
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void ExecuteQueryWithNullValue(ProviderTypes provider)
        {
            var namedQuery = "query";
            
            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(namedQuery, "SELECT top 1 * FROM [dbo].[User] WHERE :id is null", QueryType.Materializer));

            var factory = devTestDataFacadeFactoryProvider.Build(provider);
            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            
            var query = facade.GetQueryMaterializer(namedQuery);

            query.SetParameter("id", null);

            var result = query.Execute<User>();
            
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void ExecuteDynamicQueryWithNullValue(ProviderTypes provider)
        {
            var namedQuery = "query";

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(namedQuery, "SELECT top 1 * FROM [dbo].[User] WHERE :id is null", QueryType.Materializer));

            var factory = devTestDataFacadeFactoryProvider.Build(provider);
            var facade = factory.BuildQueryDataFacade(namedQueryProvider);

            var query = facade.GetQueryMaterializer(namedQuery);

            query.SetParameter("id", null);

            var result = query.Execute<dynamic>();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterQueryWithResult(ProviderTypes provider)
        {
            var queryName = "ReadGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);
            
            var namedQueryProvider = new NamedQueryProvider()
                    .RegisterQuery(new QueryDefinition(queryName, "EXEC dbo.GetGroupsById @GroupId=:idGroup", QueryType.Materializer));
            
            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            // It doesn't throw any exception.
            query.SetParameter("idGroup", null);
            query.SetParameter("idGroup", 200);

            var result = query.Execute<GroupDto>();

            Assert.NotNull(result);
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterQueryTest7(ProviderTypes provider)
        {
            var queryName = "ReadGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "EXEC dbo.[GetGroupsById_V4] :udt", QueryType.Materializer, new[] { QueryArgument.MakeInstance("udt", "[dbo].[UserDefineType]") }))
                ;

            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            var table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("GroupId", typeof(int));

            table.Rows.Add(1, 100);

            query.SetParameter("udt", table);
            var result = query.Execute<GroupDto>();
            Assert.NotNull(result);
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterQueryTest8(ProviderTypes provider)
        {
            var queryName = "ReadGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "EXEC dbo.[GetGroupsById_V4] :udt", QueryType.Materializer, new[] { QueryArgument.MakeInstance("udt", "[dbo].[UserDefineType]") }))
                ;

            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            query.SetParameter("udt", new[] { new { ID = 1, GroupId = 100 } });
            var result0 = query.Execute<GroupDto>();
            Assert.NotNull(result0);

            query.SetParameter("udt", (new[] { new { ID = 5, GroupId = 200 } }).ToList());
            var result1 = query.Execute<GroupDto>();
            Assert.NotNull(result1);

            query.SetParameter("udt", (new[] { new { ID = (int?)10, GroupId = 50 } }).ToList());
            var result2 = query.Execute<GroupDtoV2>();
            Assert.NotNull(result2);

            query.SetParameter("udt", (new[] { new { ID = (int?)null, GroupId = 11 } }).ToList());
            var result3 = query.Execute<GroupDtoV2>();
            Assert.NotNull(result3);
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterQueryTest9(ProviderTypes provider)
        {
            var queryName = "ReadGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "EXEC dbo.[GetGroupsById_V5] :udt",
                    QueryType.Materializer, new[] { QueryArgument.MakeInstance("udt", "[dbo].[UserDefineType_v2]") }))
                ;

            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            query.SetParameter("udt", new[] { new { ID = 1 }, new { ID = 5 } });
            var result0 = query.Execute<GroupDto>();
            Assert.NotNull(result0);
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterQueryTest12(ProviderTypes provider)
        {
            var queryName = "ReadGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "EXEC dbo.[GetGroupsById_Ids_only] @id=:idGrouP",
                    QueryType.Materializer))
                ;

            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            query.SetParameter("idGroup", 100);
            var result = query.Execute<int>();

            Assert.NotNull(result);
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterQueryTest14(ProviderTypes provider)
        {
            var queryName = "ReadGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "EXEC dbo.[GetGroupsById_Names_only] @id=:ID_G", QueryType.Materializer));

            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryMaterializer(queryName);
            Assert.NotNull(query);

            query.SetParameter("ID_G", 100);
            var result = query.Execute<string>();

            Assert.NotNull(result);
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void RegisterScalarProcedure(ProviderTypes provider)
        {
            var queryName = "CountGroup";

            var factory = utentiDataFacadeFactoryProvider.Build(provider);

            var namedQueryProvider = new NamedQueryProvider()
                .RegisterQuery(new QueryDefinition(queryName, "exec [dbo].[GetGroupCounter] @groupId=:ID_G, @name=:name", QueryType.Command));

            var facade = factory.BuildQueryDataFacade(namedQueryProvider);
            var query = facade.GetQueryCommand(queryName);
            Assert.NotNull(query);

            query.SetParameter("ID_G", 100)
                .SetParameter("name", "Gr Name 1 - demo");

            var result = query.Execute();

            Assert.True(result != -1);
        }
    }
}
