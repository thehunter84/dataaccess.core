﻿using System.Collections;
using System.Collections.Generic;

namespace DataAccess.Core.Test
{
    public class ProvidersNoStatelessTestData : IEnumerable<object[]>
    {
        private readonly List<object[]> list;

        public ProvidersNoStatelessTestData()
        {
            this.list = new List<object[]>
            {
                new object[]{ ProviderTypes.NHibernate },
                new object[]{ ProviderTypes.EntityFramework }
            };
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
