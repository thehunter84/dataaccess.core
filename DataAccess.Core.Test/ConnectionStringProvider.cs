﻿#if NETFRAMEWORK
using System.Configuration;

namespace DataAccess.Core.Test
{
    public class ConnectionStringProvider
    {
        private ConnectionStringProvider()
        {
        }

        public string GetConnectionString(string connectionName)
        {
            return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }

        public static ConnectionStringProvider Instance => new ConnectionStringProvider();
    }
}
#else
using System.IO;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Core.Test
{
    public class ConnectionStringProvider
    {
        private readonly IConfiguration config;

        private ConnectionStringProvider()
        {
            var configBuilder = new ConfigurationBuilder();

            configBuilder
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            this.config = configBuilder.Build();
        }

        public string GetConnectionString(string connectionName)
        {
            return config.GetConnectionString(connectionName);
        }

        public static ConnectionStringProvider Instance => new ConnectionStringProvider();
    }
}
#endif