﻿using DataAccess.Core.Test.DemoNuo.Model;

namespace DataAccess.Core.Test.DemoNuo.Mapping
{
    public class NordexAlarmMapping : AlarmMapping<NordexAlarm>
    {
        public NordexAlarmMapping()
        {
            //([site], [db_name], [dt_AlmLog_TimeStamp], [AlmLog_ID], [AlmLog_State], [id])
            this.ComposedId(mapper =>
            {
                mapper.Property(alarm => alarm.Facility, colMapper => colMapper.Column("site"));
                mapper.Property(alarm => alarm.Source, colMapper => colMapper.Column("db_name"));
                mapper.Property(alarm => alarm.UtcRecordDate, colMapper => colMapper.Column("dt_AlmLog_TimeStamp"));
                mapper.Property(alarm => alarm.LogId, colMapper => colMapper.Column("AlmLog_ID"));
                mapper.Property(alarm => alarm.State, colMapper => colMapper.Column("AlmLog_State"));
                mapper.Property(alarm => alarm.OriginId, colMapper => colMapper.Column("id"));
            });

            this.Property(alarm => alarm.SubSite, mapper => mapper.Column("sub_site"));
            this.Property(alarm => alarm.Timestamp, mapper => mapper.Column("AlmLog_TimeStamp"));
            this.Property(alarm => alarm.Milliseconds, mapper => mapper.Column("AlmLog_Milliseconds"));
            this.Property(alarm => alarm.OperatorId, mapper => mapper.Column("AlmLog_OperatorID"));

            this.Schema("[dbo]");
            this.Table("[nordex_raw_alarms]");
        }
    }
}
