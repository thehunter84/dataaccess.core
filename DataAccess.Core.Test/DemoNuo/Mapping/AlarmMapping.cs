﻿using DataAccess.Core.Test.DemoNuo.Model;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.DemoNuo.Mapping
{
    public abstract class AlarmMapping<TAlarm> : ClassMapping<TAlarm>
        where TAlarm : AlarmLog
    {
        protected AlarmMapping()
        {
            this.Property(log => log.Created, mapper =>
            {
                mapper.Update(false);
                mapper.NotNullable(true);
                mapper.Column("creation_date");
            });

            this.Property(log => log.Updated, mapper =>
            {
                mapper.NotNullable(true);
                mapper.Column("update_date");
            });

            this.Property(log => log.SourceUpdated, mapper =>
            {
                mapper.NotNullable(true);
                mapper.Column("source_update_date");
            });
        }
    }
}
