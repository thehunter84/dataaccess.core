﻿#if NETFRAMEWORK
using System.Linq;
using NHibernate.Linq;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Test.DemoNuo.Mapping;
using DataAccess.Core.Test.DemoNuo.Model;
using DataAccess.Core.Test.Impl.Nh;
using NHibernate;
using Xunit;
using Xunit.Abstractions;
#else
using System.Linq;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Test.DemoNuo.Mapping;
using DataAccess.Core.Test.DemoNuo.Model;
using DataAccess.Core.Test.Impl.Nh;
using NHibernate;
using Xunit;
using Xunit.Abstractions;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Transactions;
using DataAccess.Core.Nh;
using DataAccess.Core.Nh.Providers;

namespace DataAccess.Core.Test.DemoNuo
{
    public class DemoTest
    {
        private ITestOutputHelper output;

        public DemoTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Demo()
        {
            using (var sessionFactory = this.GetSessionFactory())
            {
                using (var session = sessionFactory.OpenSession())
                {
                    var items = session.Query<NordexAlarm>()
                        .Where(alarm => true)
                        .Take(10);

                    Assert.NotNull(items);
                }
            }
        }

        [Fact]
        public void TestConnection()
        {
            var conn =
                @"Server=10.100.60.10;Database=NUO-KS;User ID=sa;Password=mysqlserver-pwd;Application Name=TestConnection;Pooling=true;Min Pool Size=2;Max Pool Size=4;Connection Lifetime=180";

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
            }

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
            }

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
            }

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
            }

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
            }
            
            Console.WriteLine("cione");
        }

        [Fact]
        public void Insert01()
        {
            var stopwatch = new Stopwatch();
            var existWatch = new Stopwatch();

            using (var sessionFactory = this.GetSessionFactory())
            {
                using (var session = sessionFactory.OpenSession())
                {
                    var recordDate = new DateTime(2020, 1, 1, 0, 0, 0);
                    for (int i = 0; i < 10; i++)
                    {
                        stopwatch.Start();

                        using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                        {
                            var alarms = this.GetNordexAlarms(100, recordDate.AddDays(i));

                            foreach (var alarm in alarms)
                            {
                                var exist = session.Query<NordexAlarm>().Count(nordexAlarm => nordexAlarm == alarm) > 0;

                                if (!exist)
                                {
                                    session.Save(alarm);
                                }
                            }

                            session.Flush();

                            tran.Complete();
                        }

                        stopwatch.Stop();
                    }
                }
            }

            this.output.WriteLine($"elapsed: {stopwatch.Elapsed.TotalSeconds}, existence check time: {existWatch.Elapsed.TotalSeconds}");
        }

        [Fact]
        public void Insert_Facade()
        {
            var stopwatch = new Stopwatch();
            var existWatch = new Stopwatch();

            using (var sessionFactory = this.GetSessionFactory())
            {
                using (var sessionContextProvider = new SessionContextProvider(sessionFactory))
                {
                    var tranProvider = new NhTransactionProvider(sessionContextProvider);
                    var facade = new NhQueryableDataFacadeService(tranProvider);

                    var recordDate = new DateTime(2020, 1, 1, 0, 0, 0);
                    for (int i = 0; i < 1; i++)
                    {
                        stopwatch.Start();

                        //using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                        using (var tran = facade.BeginTransaction())
                        {
                            var alarms = this.GetNordexAlarms(10, recordDate.AddDays(i));

                            foreach (var alarm in alarms)
                            {
                                //var exists = facade.Exists<NordexAlarm>(al => al == alarm);

                                //if (!exists)
                                //{
                                //    facade.Insert(alarm);
                                //}

                                var al = facade.UniqueResult<NordexAlarm>(nordexAlarm => nordexAlarm == alarm);

                                if (al == null)
                                {
                                    facade.Insert(alarm);
                                }
                                else
                                {
                                    al.Updated = DateTime.UtcNow;
                                }
                            }

                            //tran.Complete();
                            tran.Commit();
                        }

                        stopwatch.Stop();
                    }
                }
            }

            this.output.WriteLine($"elapsed: {stopwatch.Elapsed.TotalSeconds}, existence check time: {existWatch.Elapsed.TotalSeconds}");
        }

        [Fact]
        public void DemoBatch()
        {
            using (var sessionFactory = this.GetSessionFactory())
            {
                using (var sessionContextProvider = new SessionContextProvider(sessionFactory))
                {
                    var tranProvider = new NhTransactionProvider(sessionContextProvider);
                    var queryBatcher = new NhFutureQueryBatch(sessionContextProvider);

                    var facade = new NhQueryableDataFacadeService(tranProvider);

                    var created = DateTime.UtcNow;
                    var recordDate = DateTime.UtcNow;

                    var alarm = new NordexAlarm
                    {
                        Facility = "demo",
                        Source = "DB09WEA86000",
                        Created = created,
                        Updated = created,
                        SourceUpdated = created,
                        UtcRecordDate = recordDate.AddMinutes(1),
                        LogId = 1 + 1,
                        State = 11,
                        OriginId = 1,
                        Milliseconds = 999
                    };

                    queryBatcher.AddFutureValue((IQueryable<NordexAlarm> qq) => qq.Count(al => al == alarm));
                    queryBatcher.AddFuture((IQueryable<NordexAlarm> qq) => qq.Where(al => al.LogId > 0).Take(10));

                    queryBatcher.Execute();
                }
            }
        }

        [Fact]
        public void Insert_Facade_Future()
        {
            var stopwatch = new Stopwatch();
            
            using (var sessionFactory = this.GetSessionFactory())
            {
                using (var sessionContextProvider = new SessionContextProvider(sessionFactory))
                {
                    var queryBatcher = new NhFutureQueryBatch(sessionContextProvider);
                    var localCache = new Dictionary<string, NordexAlarm>();

                    var tranProvider = new NhTransactionProvider(sessionContextProvider);
                    var facade = new NhQueryableDataFacadeService(tranProvider);

                    var recordDate = new DateTime(2020, 1, 1, 0, 0, 0);
                    for (int i = 0; i < 10; i++)
                    {
                        stopwatch.Start();

                        using (var tran = facade.BeginTransaction())
                        {
                            var alarms = this.GetNordexAlarms(100, recordDate.AddDays(i));

                            foreach (var alarm in alarms)
                            {
                                var key = queryBatcher.AddFutureValue((IQueryable<NordexAlarm> qq) => qq.Count(al => al == alarm));

                                localCache.Add(key, alarm);
                            }

                            this.output.WriteLine("####### before accessing future values #######");

                            var items = localCache.Where(pair => queryBatcher.GetValueResult<int>(pair.Key) == 0)
                                .Select(pair => pair.Value)
                                .Where(item => item != null)
                                .ToList();

                            if (items.Any())
                            {
                                facade.Insert<NordexAlarm>(items);
                            }

                            this.output.WriteLine("####### after accessing future values #######");
                            
                            tran.Commit();
                        }

                        localCache.Clear();
                        queryBatcher.Reset();

                        facade.Clear();

                        stopwatch.Stop();
                    }
                }
            }

            this.output.WriteLine($"elapsed: {stopwatch.Elapsed.TotalSeconds}");
        }

        [Fact]
        public void Insert02()
        {
            var stopwatch = new Stopwatch();

            using (var sessionFactory = this.GetSessionFactory())
            {
                //using (var session = sessionFactory.OpenSession())
                using (var session = sessionFactory.OpenStatelessSession())
                {
                    var recordDate = new DateTime(2020, 1, 1, 0, 0, 0);
                    for (int i = 0; i < 10; i++)
                    {
                        stopwatch.Start();

                        //using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                        using (var tran = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            var alarms = this.GetNordexAlarms(100, recordDate.AddDays(i));

                            foreach (var alarm in alarms)
                            {
                                var exist = session.Query<NordexAlarm>().Count(nordexAlarm => nordexAlarm == alarm) > 0;

                                if (!exist)
                                {
                                    session.Insert(alarm);
                                }

                                //session.Insert(alarm);
                            }

                            //session.Flush();

                            tran.Commit();
                        }

                        stopwatch.Stop();
                    }
                }
            }

            this.output.WriteLine($"elapsed: {stopwatch.Elapsed.TotalSeconds}");
        }

        [Fact]
        public void ReadFutureQueries()
        {
            using (var sessionFactory = this.GetSessionFactory())
            {
                using (var sessionContextProvider = new SessionContextProvider(sessionFactory))
                {
                    var queryBatcher = new NhFutureQueryBatch(sessionContextProvider);
                    var tranProvider = new NhTransactionProvider(sessionContextProvider);
                    
                    this.output.WriteLine("before future...");
                    this.output.WriteLine(string.Empty);

                    queryBatcher.AddFuture((IQueryable<NordexAlarm> qq) => qq.Where(al => al.Source != null).Take(5), "1");
                    queryBatcher.AddFuture((IQueryable<NordexAlarm> qq) => qq.Where(al => al.LogId > 1).Take(10), "2");
                    //queryBatcher.AddFutureValue((IQueryable<NordexAlarm> qq) => qq.FirstOrDefault(al => al.Facility == "demo"), "3");
                    queryBatcher.AddFutureValue((IQueryable<NordexAlarm> qq) => qq.Count(al => al.Facility == "demo"), "4");

                    this.output.WriteLine("before read first futures values...");
                    this.output.WriteLine(string.Empty);

                    var res1 = queryBatcher.GetResult<NordexAlarm>("1");

                    this.output.WriteLine("before read second futures values...");
                    this.output.WriteLine(string.Empty);

                    var res2 = queryBatcher.GetResult<NordexAlarm>("2");
                    //var res3 = queryBatcher.GetValueResult<NordexAlarm>("3");
                    var res4 = queryBatcher.GetValueResult<int>("4");

                    Assert.NotNull(res1);
                    Assert.NotNull(res2);
                    //Assert.NotNull(res3);
                    Assert.True(res4 > 0);
                }
            }
        }

        private List<NordexAlarm> GetNordexAlarms(int counter, DateTime recordDate)
        {
            var list = new List<NordexAlarm>();
            var created = DateTime.UtcNow;

            for (var i = 0; i < counter; i++)
            {
                list.Add(
                    new NordexAlarm
                    {
                        Facility = "demo",
                        Source = "DB09WEA86000",
                        Created = created,
                        Updated = created,
                        SourceUpdated = created,
                        UtcRecordDate = recordDate.AddMinutes(i),
                        LogId = 1 + 1,
                        State = 11,
                        OriginId = i,
                        Milliseconds = 999
                    });
            }

            return list;
        }

        private ISessionFactory GetSessionFactory()
        {
            var connectionStr = "Server=10.100.60.10;Database=NUO-KS;User ID=sa;Password=mysqlserver-pwd;Application Name=Facade.demo";

            var config = new NHibernate.Cfg.Configuration
                {
                    Interceptor = new XUnitSqlStatementsInterceptor(this.output)
                }
                .Dialect<NHibernate.Dialect.MsSql2012Dialect>()
                .ConnectionString(connectionStr)
                .GenerateStatistics(false)
                .ShowSql(false)
                .UseProxyValidator()
                .BatchSize(100)
                .AddFluentMappings(typeof(NordexAlarmMapping));

            return config.BuildSessionFactory();
        }
    }
}
