﻿using System.Collections.Generic;

namespace DataAccess.Core.Test.DemoNuo.Model
{
    public class NordexAlarm : AlarmLog
    {
        public short LogId { get; set; }

        public string Source { get; set; }

        public byte State { get; set; }

        public string SubSite
        {
            get => this.Facility;
            private set { }
        }

        /// <summary>
        /// Gets or sets the UTC record date expressed as UNIX format
        /// </summary>
        public int Timestamp { get; set; }

        public int OriginId { get; set; }

        public short Milliseconds { get; set; }

        public short OperatorId { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj is NordexAlarm alarm &&
                   this.Facility == alarm.Facility &&
                   this.Source == alarm.Source &&
                   this.UtcRecordDate == alarm.UtcRecordDate &&
                   this.LogId == alarm.LogId &&
                   this.State == alarm.State &&
                   this.OriginId == alarm.OriginId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = -1202207664;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(this.Facility);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(this.Source);
                hashCode = hashCode * -1521134295 + this.UtcRecordDate.GetHashCode();
                hashCode = hashCode * -1521134295 + this.LogId.GetHashCode();
                hashCode = hashCode * -1521134295 + this.State.GetHashCode();
                hashCode = hashCode * -1521134295 + this.OriginId.GetHashCode();
                return hashCode;
            }
        }
    }
}
