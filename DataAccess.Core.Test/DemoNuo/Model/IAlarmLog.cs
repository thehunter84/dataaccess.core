﻿using System;

namespace DataAccess.Core.Test.DemoNuo.Model
{
    public interface IAlarmLog
    {
        /// <summary>
        /// Gets or sets the facility name.
        /// </summary>
        string Facility { get; set; }

        /// <summary>
        /// Gets or sets the data when this instance was signed up.
        /// </summary>
        DateTime UtcRecordDate { get; }

        /// <summary>
        /// Gets the date of creation of this instance
        /// </summary>
        DateTime? Created { get; }

        /// <summary>
        /// Gets the update date of this instance
        /// </summary>
        DateTime? Updated { get; }

        /// <summary>
        /// Gets the update date from source.
        /// </summary>
        DateTime? SourceUpdated { get; }
    }
}
