﻿using System;

namespace DataAccess.Core.Test.DemoNuo.Model
{
    public class AlarmLog : IAlarmLog
    {
        public string Facility { get; set; }

        public DateTime UtcRecordDate { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Updated { get; set; }

        public DateTime? SourceUpdated { get; set; }
    }
}
