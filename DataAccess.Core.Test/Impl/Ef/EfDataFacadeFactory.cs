﻿#if NETFRAMEWORK
using System.Data.Entity;
using DataAccess.Core.EF;
using DataAccess.Core.EF.Providers;
using DataAccess.Core.Providers;
#else
using DataAccess.Core.EF;
using DataAccess.Core.EF.Providers;
using DataAccess.Core.Providers;
using Microsoft.EntityFrameworkCore;
#endif

namespace DataAccess.Core.Test.Impl.Ef
{
    public class EfDataFacadeFactory : IDataFacadeFactory
    {
        private readonly DbContext dbContext;
        
        public EfDataFacadeFactory(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            dbContext?.Dispose();
        }

        public IQueryableDataFacadeService BuildQueryableDataFacadeService()
        {
            return new EfQueryableDataFacadeService(this.MakeTransactionProvider());
        }

        public IQueryDataFacade BuildQueryDataFacade(INamedQueryProvider queryProvider)
        {
            return new EfQueryDataFacade(this.MakeTransactionProvider(), queryProvider);
        }

        protected IContextProvider<DbContext> MakeContextProvider()
        {
            return new DbContextProvider(() => this.dbContext);
        }

        protected ITransactionProvider<DbContext> MakeTransactionProvider()
        {
            return new EfTransactionProvider(this.MakeContextProvider());
        }
    }
}
