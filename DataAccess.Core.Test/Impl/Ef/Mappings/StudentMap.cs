﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class StudentMap : EntityTypeConfiguration<Student>
    {
        public StudentMap()
        {
            this.ToTable("Student");
            this.HasKey(student => student.Id);
            this.Property(student => student.Name);
            this.Property(student => student.Surname);

            // TODO: verify the component why It's not considered on mapping?
            ////this.HasOptional(student => student.Address).WithRequired().Map(configuration => configuration.MapKey("studentid"));
            ////this.HasOptional(student => student.Address).WithRequired(address => address.Id);
            ////this.HasOptional(student => student.Address);

            this.Ignore(student => student.Address);
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class StudentMap : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("Student");
            builder.HasKey(student => student.Id);
            builder.Property(student => student.Name);
            builder.Property(student => student.Surname);

            // TODO: verify the component why It's not considered on mapping?
            ////builder.HasOptional(student => student.Address).WithRequired().Map(configuration => configuration.MapKey("studentid"));
            ////builder.HasOptional(student => student.Address).WithRequired(address => address.Id);
            ////builder.HasOptional(student => student.Address);

            builder.Ignore(student => student.Address);
        }
    }
}
#endif