﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class EmployeeMap : EntityTypeConfiguration<Employee>
    {
        public EmployeeMap()
        {
            this.HasKey(employee => employee.Id);
            this.Property(employee => employee.Id).HasColumnName("Id");
            this.Property(employee => employee.IndustryType).HasColumnName("IndustryTypeId");
            this.Property(employee => employee.Name);
            this.Property(employee => employee.Surname);
            this.Property(employee => employee.Taxcode);

            this.ToTable("[Employee]");
        }
    }

    public class StateEmployeeMap : EntityTypeConfiguration<StateEmployee>
    {
        public StateEmployeeMap()
        {
            //this.HasKey(employee => employee.Id).Map(cfg => cfg.);
            //this.Property(employee => employee.Id).HasColumnName("StateEmployeeId");
            this.Property(employee => employee.MinimalRal);

            this.ToTable("[StateEmployee]");
        }
    }

    public class PrivateEmployeeMap : EntityTypeConfiguration<PrivateEmployee>
    {
        public PrivateEmployeeMap()
        {
            //this.HasKey(employee => employee.Id);
            //this.Property(employee => employee.Id).HasColumnName("PrivateEmployeeId");
            this.Property(employee => employee.MinimalBonus);

            this.ToTable("[PrivateEmployee]");
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class EmployeeMap : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(employee => employee.Id);
            builder.Property(employee => employee.Id).HasColumnName("Id");
            builder.Property(employee => employee.IndustryType).HasColumnName("IndustryTypeId");
            builder.Property(employee => employee.Name);
            builder.Property(employee => employee.Surname);
            builder.Property(employee => employee.Taxcode);

            builder.ToTable("[Employee]");
        }
    }

    ////public class StateEmployeeMap : EntityTypeConfiguration<StateEmployee>
    ////{
    ////    public StateEmployeeMap()
    ////    {
    ////        //this.HasKey(employee => employee.Id).Map(cfg => cfg.);
    ////        //this.Property(employee => employee.Id).HasColumnName("StateEmployeeId");
    ////        this.Property(employee => employee.MinimalRal);

    ////        this.ToTable("[StateEmployee]");
    ////    }
    ////}

    ////public class PrivateEmployeeMap : EntityTypeConfiguration<PrivateEmployee>
    ////{
    ////    public PrivateEmployeeMap()
    ////    {
    ////        //this.HasKey(employee => employee.Id);
    ////        //this.Property(employee => employee.Id).HasColumnName("PrivateEmployeeId");
    ////        this.Property(employee => employee.MinimalBonus);

    ////        this.ToTable("[PrivateEmployee]");
    ////    }
    ////}
}
#endif
