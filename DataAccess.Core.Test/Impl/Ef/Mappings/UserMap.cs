﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.ToTable("User");
            this.HasKey(user => user.Id);

            this.Property(user => user.Id);
            this.Property(user => user.Name);
            this.Property(user => user.Birthdate);
            this.HasMany(user => user.Messages).WithRequired().Map(cfg => cfg.MapKey("UserId"));
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");
            builder.HasKey(user => user.Id);
            builder.Property(user => user.Id).ValueGeneratedOnAdd();
            builder.Property(user => user.Name);
            builder.Property(user => user.Birthdate);

            //this.HasMany(user => user.Messages).WithRequired().Map(cfg => cfg.MapKey("UserId"));
            builder.HasMany(user => user.Messages)
                .WithOne()
                .IsRequired()
                .HasForeignKey("UserId");
        }
    }
}
#endif