﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class EntityWithStringKeyMap : EntityTypeConfiguration<EntityWithStringKey>
    {
        public EntityWithStringKeyMap()
        {
            this.ToTable("EntityWithStringKey");
            this.HasKey(benefit => benefit.Id);
            this.Property(benefit => benefit.Id);
            this.Property(benefit => benefit.CurrentVal);
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class EntityWithStringKeyMap : IEntityTypeConfiguration<EntityWithStringKey>
    {
        public void Configure(EntityTypeBuilder<EntityWithStringKey> builder)
        {
            builder.ToTable("EntityWithStringKey");
            builder.HasKey(benefit => benefit.Id);
            builder.Property(benefit => benefit.Id);
            builder.Property(benefit => benefit.CurrentVal);
        }
    }
}

#endif