﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            this.ToTable("UserMessage");
            this.HasKey(message => message.Id);
            this.Property(message => message.Body).HasColumnName("Message");
            this.Property(message => message.SendDate);
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class MessageMap : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.ToTable("UserMessage");
            builder.HasKey(message => message.Id);
            builder.Property(message => message.Body).HasColumnName("Message");
            builder.Property(message => message.SendDate);
        }
    }
}

#endif