﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class UserBenefitMap : EntityTypeConfiguration<UserBenefit>
    {
        public UserBenefitMap()
        {
            this.ToTable("UserBenefit");
            this.HasKey(userBenefit => userBenefit.Id);

            this.HasRequired(userBenefit => userBenefit.User).WithMany().Map(cfg => cfg.MapKey("UserId"));
            this.HasRequired(userBenefit => userBenefit.Benefit).WithMany().Map(cfg => cfg.MapKey("BenefitId"));
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class UserBenefitMap : IEntityTypeConfiguration<UserBenefit>
    {
        public void Configure(EntityTypeBuilder<UserBenefit> builder)
        {
            builder.ToTable("UserBenefit");
            builder.HasKey(userBenefit => userBenefit.Id);

            //builder.HasRequired(userBenefit => userBenefit.User).WithMany().Map(cfg => cfg.MapKey("UserId"));
            builder.HasOne(userBenefit => userBenefit.User).WithMany().HasForeignKey("UserId");

            //builder.HasRequired(userBenefit => userBenefit.Benefit).WithMany().Map(cfg => cfg.MapKey("BenefitId"));
            builder.HasOne(userBenefit => userBenefit.Benefit).WithMany().HasForeignKey("BenefitId");
        }
    }
}
#endif