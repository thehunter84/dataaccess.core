﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class StudentAddressMap : EntityTypeConfiguration<StudentAddress>
    {
        public StudentAddressMap()
        {
            this.ToTable("StudentAddress");
            this.HasKey(address => address.Id);

            this.Property(address => address.Id).HasColumnName("studentid");
            this.Property(address => address.Address);
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class StudentAddressMap : IEntityTypeConfiguration<StudentAddress>
    {
        public void Configure(EntityTypeBuilder<StudentAddress> builder)
        {
            builder.ToTable("StudentAddress");
            builder.HasKey(address => address.Id);

            builder.Property(address => address.Id).HasColumnName("studentid");
            builder.Property(address => address.Address);
        }
    }
}
#endif
