﻿#if NETFRAMEWORK
using System.Data.Entity.ModelConfiguration;
using DataAccess.Core.Model.DbTest;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class BenefitMap : EntityTypeConfiguration<Benefit>
    {
        public BenefitMap()
        {
            this.ToTable("Benefit");
            this.HasKey(benefit => benefit.Id);
            this.Property(benefit => benefit.Id);
            this.Property(benefit => benefit.Name);
        }
    }
}
#else
using DataAccess.Core.Model.DbTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Core.Test.Impl.Ef.Mappings
{
    public class BenefitMap : IEntityTypeConfiguration<Benefit>
    {
        public void Configure(EntityTypeBuilder<Benefit> builder)
        {
            builder.ToTable("Benefit");
            builder.HasKey(benefit => benefit.Id);
            builder.Property(benefit => benefit.Id);
            builder.Property(benefit => benefit.Name);
        }
    }
}
#endif