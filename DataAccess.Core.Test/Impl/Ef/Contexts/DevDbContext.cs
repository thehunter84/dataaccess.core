﻿#if NETFRAMEWORK
using System.Data.Entity;
using DataAccess.Core.Test.Impl.Ef.Mappings;
#else
using DataAccess.Core.Test.Impl.Ef.Mappings;
using Microsoft.EntityFrameworkCore;
#endif

namespace DataAccess.Core.Test.Impl.Ef.Contexts
{
    public class DevDbContext : DbContext
    {
#if NETFRAMEWORK
        public DevDbContext(string connectionStr)
            : base(connectionStr)
        {
            Database.SetInitializer<DevDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new MessageMap());
            modelBuilder.Configurations.Add(new BenefitMap());
            modelBuilder.Configurations.Add(new UserBenefitMap());
            modelBuilder.Configurations.Add(new EntityWithStringKeyMap());

            modelBuilder.Configurations.Add(new StudentMap());
            modelBuilder.Configurations.Add(new StudentAddressMap());

            //TODO: implemented these mappings, but there aren't working due to primary key column names of derived tables.

            ////modelBuilder.Configurations.Add(new EmployeeMap());
            ////modelBuilder.Configurations.Add(new StateEmployeeMap());
            ////modelBuilder.Configurations.Add(new PrivateEmployeeMap());
        }
#else
        public DevDbContext(DbContextOptions<DevDbContext> options)
            : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new UserMap())
                .ApplyConfiguration(new MessageMap())
                .ApplyConfiguration(new EntityWithStringKeyMap());
        }
#endif
    }
}
