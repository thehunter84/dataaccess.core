﻿#if NETFRAMEWORK
using System.Data.Entity;
#else
using Microsoft.EntityFrameworkCore;
#endif

namespace DataAccess.Core.Test.Impl.Ef.Contexts
{
    public class CustomerDbContext : DbContext
    {
#if NETFRAMEWORK
        public CustomerDbContext(string connectionStr)
            : base(connectionStr)
        {
            Database.SetInitializer<CustomerDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // todo: add new mappings...
        }
#else
        public CustomerDbContext(DbContextOptions<CustomerDbContext> options)
            : base(options)
        {
        }
#endif
    }
}