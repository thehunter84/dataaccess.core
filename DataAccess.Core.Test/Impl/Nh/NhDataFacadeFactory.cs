﻿using System.Collections.Generic;
using DataAccess.Core.Nh;
using DataAccess.Core.Nh.Providers;
using DataAccess.Core.Providers;
using NHibernate;

namespace DataAccess.Core.Test.Impl.Nh
{
    public class NhDataFacadeFactory : IDataFacadeFactory
    {
        private readonly ISessionFactory sessionFactory;
        private readonly bool useStateLessSession;
        private readonly List<IContextProvider<ISession>> contextProviders;
        
        public NhDataFacadeFactory(ISessionFactory sessionFactory, bool useStateLessSession = false)
        {
            this.sessionFactory = sessionFactory;
            this.useStateLessSession = useStateLessSession;
            this.contextProviders = new List<IContextProvider<ISession>>();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            foreach (var contextProvider in contextProviders)
            {
                contextProvider.Dispose();
            }

            sessionFactory?.Dispose();
        }

        public IQueryableDataFacadeService BuildQueryableDataFacadeService()
        {
            return new NhQueryableDataFacadeService(this.MakeTransactionProvider());
        }

        public IQueryDataFacade BuildQueryDataFacade(INamedQueryProvider queryProvider)
        {
            return new NhQueryDefinitionDataFacade(this.MakeTransactionProvider(), queryProvider);
        }
        
        protected ITransactionProvider<ISession> MakeTransactionProvider()
        {
            return new NhTransactionProvider(this.MakeContextProvider());
        }

        protected IContextProvider<ISession> MakeContextProvider()
        {
            var context =  this.useStateLessSession
                ? (IContextProvider<ISession>)new StatelessSessionContextProvider(this.sessionFactory)
                : new SessionContextProvider(this.sessionFactory);

            this.contextProviders.Add(context);

            return context;
        }
    }
}
