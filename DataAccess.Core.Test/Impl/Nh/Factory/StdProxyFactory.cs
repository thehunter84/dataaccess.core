﻿using System;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Intercept;
using NHibernate.Proxy;
using NHibernate.Proxy.DynamicProxy;

namespace DataAccess.Core.Test.Impl.Nh.Factory
{
    public class StdProxyFactory : AbstractProxyFactory
    {
#pragma warning disable CS0618 // Type or member is obsolete
        private readonly ProxyFactory factory = new ProxyFactory();
#pragma warning restore CS0618 // Type or member is obsolete

        public override INHibernateProxy GetProxy(object id, ISessionImplementor session)
        {
            try
            {
#pragma warning disable 618
                DefaultLazyInitializer defaultLazyInitializer = new DefaultLazyInitializer(this.EntityName, this.PersistentClass, id, this.GetIdentifierMethod, this.SetIdentifierMethod, this.ComponentIdType, session, this.OverridesEquals);
#pragma warning restore 618
                return this.IsClassProxy ? (INHibernateProxy)this.factory.CreateProxy(this.PersistentClass, defaultLazyInitializer, this.Interfaces) : (INHibernateProxy)this.factory.CreateProxy(this.Interfaces[0], defaultLazyInitializer, this.Interfaces);
            }
            catch (Exception ex)
            {
                throw new HibernateException("Creating a proxy instance failed", ex);
            }
        }
        
#if NETCOREAPP2_2
#else
        public override object GetFieldInterceptionProxy(object instanceToWrap)
        {
            return this.factory.CreateProxy(this.PersistentClass, new DefaultDynamicLazyFieldInterceptor(), typeof(IFieldInterceptorAccessor));
        }
#endif
    }
}
