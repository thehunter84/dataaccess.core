﻿using System;
using NHibernate.Proxy;

namespace DataAccess.Core.Test.Impl.Nh.Factory
{
    public class StdProxyFactoryFactory : NHibernate.Bytecode.IProxyFactoryFactory
    {
        public IProxyFactory BuildProxyFactory()
        {
            return new CustomProxyFactory();
        }

        public bool IsInstrumented(Type entityClass)
        {
            return true;
        }

        public bool IsProxy(object entity)
        {
            return entity is INHibernateProxy;
        }

        public IProxyValidator ProxyValidator => new DynProxyTypeValidator();
    }
}
