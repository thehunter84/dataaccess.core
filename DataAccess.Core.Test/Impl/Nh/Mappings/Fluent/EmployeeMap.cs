﻿using DataAccess.Core.Model.DbTest;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class EmployeeMap : ClassMapping<Employee>
    {
        public EmployeeMap()
        {
            Id(employee => employee.Id, mapper => mapper.Generator(Generators.Native));
            Property(employee => employee.IndustryType, mapper => mapper.Column("IndustryTypeId"));
            Property(employee => employee.Name);
            Property(employee => employee.Surname);
            Property(employee => employee.Taxcode);
            Abstract(true);
        }
    }

    public class StateEmployeeMap : JoinedSubclassMapping<StateEmployee>
    {
        public StateEmployeeMap()
        {
            Key(mapper =>
            {
                mapper.Column("StateEmployeeId");
                mapper.NotNullable(true);
                mapper.Unique(true);
                mapper.Update(true);
            });

            Property(employee => employee.MinimalRal);
        }
    }

    public class PrivateEmployeeMap : JoinedSubclassMapping<PrivateEmployee>
    {
        public PrivateEmployeeMap()
        {
            Key(mapper =>
            {
                mapper.Column("PrivateEmployeeId");
                mapper.NotNullable(true);
                mapper.Unique(true);
                mapper.Update(true);
            });

            Property(employee => employee.MinimalBonus);
        }
    }
}
