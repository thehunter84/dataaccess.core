﻿using DataAccess.Core.Model.DbTest;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class EntityWithStringKeyMap : ClassMapping<EntityWithStringKey>
    {
        public EntityWithStringKeyMap()
        {
            Id(key => key.Id, mapper => mapper.Generator(Generators.Assigned));
            Property(key => key.CurrentVal);
        }
    }
}
