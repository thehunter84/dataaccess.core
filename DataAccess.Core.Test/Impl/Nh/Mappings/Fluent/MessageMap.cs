﻿using DataAccess.Core.Model.DbTest;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class MessageMap : ClassMapping<Message>
    {
        public MessageMap()
        {
            Id(message => message.Id, mapper =>
            {
                mapper.Generator(Generators.Native);
                mapper.Type(NHibernateUtil.Int32);
            });
            Property(message => message.Body, mapper => mapper.Column("Message"));
            Property(message => message.SendDate);

            Table("[UserMessage]");
        }
    }
}
