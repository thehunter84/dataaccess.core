﻿using DataAccess.Core.Model.DbTest;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class UserMap : ClassMapping<User>
    {
        public UserMap()
        {
            Id(user => user.Id, mapper => mapper.Generator(Generators.Native));
            Property(user => user.Name);
            Property(user => user.Birthdate);
            Set(user => user.Messages, mapper =>
            {
                mapper.BatchSize(10);
                //mapper.Lazy(CollectionLazy.NoLazy);
                //mapper.Inverse(false);                // optional!!!
                mapper.Cascade(Cascade.All);
                mapper.Table("UserMessage");
                mapper.Key(keyMapper =>
                {
                    keyMapper.Column("UserId");
                    keyMapper.NotNullable(true);
                });

            }, relation => relation.OneToMany());

            Table("[User]");
        }
    }
}
