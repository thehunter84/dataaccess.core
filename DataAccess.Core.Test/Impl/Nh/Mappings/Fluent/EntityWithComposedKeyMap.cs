﻿using DataAccess.Core.Model.DbTest;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class EntityWithComposedKeyMap : ClassMapping<EntityWithComposedKey>
    {
        public EntityWithComposedKeyMap()
        {
            ComponentAsId(key => key.Id, mapper =>
            {
                mapper.Property(key => key.Identifier, propertyMapper => propertyMapper.Column("Id"));
                mapper.Property(key => key.Type);
            });

            Property(key => key.Name);

            OneToOne(key => key.Price, mapper =>
            {
                mapper.Cascade(Cascade.All);
                // Don't need to specify the any foreign key!!
            });

            Set(key => key.Jobs, mapper =>
            {
                mapper.Inverse(true);
                mapper.Cascade(Cascade.All);

                mapper.Key(keyMapper =>
                {
                    keyMapper.Columns(columnMapper =>
                        {
                            columnMapper.Name("EntityId");
                            //columnMapper.NotNullable(true);
                        },
                        columnMapper =>
                        {
                            columnMapper.Name("EntityType");
                            //columnMapper.NotNullable(true);
                        });

                });
            }, relation => relation.OneToMany());

            Table("[EntityWithComposedKey]");
        }
    }
}
