﻿using DataAccess.Core.Model.DbTest;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class EntityPriceMap : ClassMapping<EntityPrice>
    {
        public EntityPriceMap()
        {
            ComponentAsId(price => price.Id, mapper =>
            {
                mapper.Property(key => key.Identifier, propertyMapper => propertyMapper.Column("Id"));
                mapper.Property(key => key.Type);
            });

            Property(price => price.Price);

            Table("EntityPrice");
            //Catalog();
            //Schema();
        }
    }
}
