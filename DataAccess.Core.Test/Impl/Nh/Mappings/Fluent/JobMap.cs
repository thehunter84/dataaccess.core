﻿using DataAccess.Core.Model.DbTest;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataAccess.Core.Test.Impl.Nh.Mappings.Fluent
{
    public class JobMap : ClassMapping<Job>
    {
        public JobMap()
        {
            Id(job => job.Id, mapper => mapper.Generator(Generators.Native));
            Property(job => job.Description);

            ManyToOne(job => job.Parent, mapper =>
            {
                mapper.Cascade(Cascade.None);
                mapper.Columns(columnMapper => columnMapper.Name("EntityId"), columnMapper => columnMapper.Name("EntityType"));
            });

            Table("Job");
        }
    }
}
