﻿using NHibernate;
using NHibernate.SqlCommand;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Impl.Nh
{
    public class XUnitSqlStatementsInterceptor : EmptyInterceptor
    {
        public XUnitSqlStatementsInterceptor(ITestOutputHelper output)
        {
            this.Output = output;
        }

        public ITestOutputHelper Output { get; set; }

        public override SqlString OnPrepareStatement(SqlString sql)
        {
            this.Output.WriteLine(sql.ToString());

            return sql;
        }
    }
}
