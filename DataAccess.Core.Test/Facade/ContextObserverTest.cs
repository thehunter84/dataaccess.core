﻿using System.Transactions;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Facade
{
    public class ContextObserverTest : DbTestDataFacadeInitializer
    {
        public ContextObserverTest(ITestOutputHelper output)
            : base(output)
        {
        }

        [Theory]
        [ClassData(typeof(ProvidersNoStatelessTestData))]
        public void TestHasChanges(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadUncommitted;
                
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUser(isolation);
                    
                    user.Name += "_changed";

                    // asserts
                    Assert.True(facade.HasChanges());
                    // no commits is done.. so no insertions were made.
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersNoStatelessTestData))]
        public void TestCached(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadUncommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUser(isolation);

                    // asserts
                    Assert.True(facade.Cached(user));

                    // act
                    facade.Evict(user);

                    // asserts
                    Assert.False(facade.Cached(user));
                    // no commits is done.. so no insertions were made.
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersNoStatelessTestData))]
        public void TestEvict(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadUncommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUser(isolation);

                    // asserts
                    Assert.True(facade.Cached(user));

                    // act
                    facade.Evict(user);

                    // asserts
                    Assert.False(facade.Cached(user));
                    // no commits is done.. so no insertions were made.
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void TestRestore(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadUncommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUser(isolation);

                    var originalname = user.Name;
                    user.Name += "_changed";

                    // asserts
                    Assert.NotEqual(user.Name, originalname);

                    // act
                    facade.Restore(user);

                    Assert.Equal(user.Name, originalname);
                    // no commits is done.. so no insertions were made.
                }
            }
        }
    }
}
