﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Transactions;
using DataAccess.Core.Model.DbTest;
using DataAccess.Core.Transactions;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Facade
{
    public class QueryableDataFacadeTest : DbTestDataFacadeInitializer
    {
        public QueryableDataFacadeTest(ITestOutputHelper output)
            : base(output)
        {
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void GetTest(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(60) }))
                {
                    // act
                    var user = facade.MakePersistentUser();

                    // todo: verificare questa implementazione.
                    facade.Clear();

                    var userFromSource = facade.Get<User>(user.Id);

                    // asserts
                    Assert.NotNull(userFromSource);
                    Assert.NotEqual(user, userFromSource);
                    Assert.NotSame(user, userFromSource);
                }
            }
        }

        [Theory]
        [InlineData(ProviderTypes.NHibernate)]
        [Description("Saves am instance which key is a complex type, not implemented by EF")]
        public void GetTestWithComposedKey(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                EntityWithComposedKey instance;

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // acts
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Isolation = isolation }))
                    {
                        instance = new EntityWithComposedKey
                        {
                            Id = new ComposedKey { Identifier = 10, Type = 'B' },
                            Name = Guid.NewGuid().ToString("D")
                        };

                        facade.MakePersistent(instance);

                        tran.Commit();
                    }

                    // act
                    // no commits is done.. no insertions were made on data source.
                    facade.Evict(instance);
                }

                // asserts
                Assert.False(facade.Exists<EntityWithComposedKey>(entity => entity == instance));
            }
        }
        
        [Theory]
        [ClassData(typeof(ProvidersNoStatelessTestData))]
        [Description("The instance get by facade is the same from original 'cause session caches the original instance.")]
        public void GetTestFromContext(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUser();
                    var userFromContext = facade.Get<User>(user.Id);

                    // asserts (same references)
                    Assert.NotNull(userFromContext);
                    Assert.Equal(user, userFromContext);
                    Assert.Same(user, userFromContext);
                }
            }
        }

        [Theory]
        [Description("This test is not supported by EF because primary keys of derived tables are named different than base table primary key.")]
        [InlineData(ProviderTypes.NHibernate)]
        //[InlineData(ProviderTypes.EntityFramework)]
        public void GetTestPolymorphicEmployee(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                Employee privateEmployee;
                Employee stateEmployee;

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // acts
                    privateEmployee = new PrivateEmployee
                    {
                        IndustryType = IndustryType.Chemistry,
                        MinimalBonus = 2500,
                        Name = "name_private",
                        Surname = "surname_private",
                        Taxcode = "12345678910"
                    };

                    stateEmployee = new StateEmployee
                    {
                        IndustryType = IndustryType.Engineering,
                        MinimalRal = 4000,
                        Name = "name_state",
                        Surname = "surname_state",
                        Taxcode = "4545612311"
                    };

                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Isolation = IsolationLevel.ReadCommitted }))
                    {
                        facade.MakePersistent(privateEmployee);
                        facade.MakePersistent(stateEmployee);
                        tran.Commit();
                    }

                    facade.Evict(privateEmployee);
                    facade.Evict(stateEmployee);

                    // asserts
                    Assert.True(facade.Exists<Employee>(employee => employee == privateEmployee));
                    Assert.True(facade.Exists<Employee>(employee => employee == stateEmployee));
                }

                // asserts
                Assert.False(facade.Exists<Employee>(employee => employee == privateEmployee));
                Assert.False(facade.Exists<Employee>(employee => employee == stateEmployee));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void GetTestEntityWithAssignedKey(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                EntityWithStringKey instance;

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // acts
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Isolation = IsolationLevel.ReadCommitted }))
                    {
                        instance = new EntityWithStringKey { Id = Guid.NewGuid().ToString("D"), CurrentVal = "val" };
                        facade.MakePersistent(instance);

                        tran.Commit();
                    }

                    // asserts
                    // TODO: supported in Nh only
                    //Assert.True(facade.Exists<EntityWithStringKey>(entity => entity == instance));
                    Assert.True(facade.Exists<EntityWithStringKey>(entity => entity.Id == instance.Id));
                }

                // asserts
                // TODO: supported in Nh only
                //Assert.False(facade.Exists<EntityWithStringKey>(entity => entity == instance));
                Assert.False(facade.Exists<EntityWithStringKey>(entity => entity.Id == instance.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void ExistsTest(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                User user;
                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    user = facade.MakePersistentUser();

                    // asserts (same references)
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                }

                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void UniqueResultTest(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                User user;
                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    user = facade.MakePersistentUser();
                    facade.Clear();

                    User usFromSource = facade.UniqueResult<User>(us => us.Id == user.Id);

                    // asserts
                    Assert.NotNull(usFromSource);
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void ApplyWhereTest(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();
                    
                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers();
                    var useIds = users.Select(n => n.Id).ToList();

                    facade.Clear();

                    // not supported by EF
                    //var userFromSorce = facade.ApplyWhere<User>(us => users.Contains(us)).ToList();
                    var userFromSorce = facade.ApplyWhere<User>(us => useIds.Contains(us.Id)).ToList();

                    // asserts
                    Assert.NotNull(userFromSorce);
                    Assert.NotEmpty(userFromSorce);
                    Assert.Equal(users.Count, userFromSorce.Count);
                }
            }
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void ExecuteExpressionTest(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers();
                    facade.Clear();

                    var userIds = users.Select(n => n.Id);

                    // TODO: Its' supported by NHibernate, but for EF no.. ( that's yuck !!!!! )
                    //var userFromSorce = facade.ExecuteExpression<User>(query => query.Where(user => users.Contains(user))).ToList();
                    var userFromSorce = facade.ExecuteExpression<User>(query => query.Where(user => userIds.Contains(user.Id))).ToList();

                    // asserts
                    Assert.NotNull(userFromSorce);
                    Assert.NotEmpty(userFromSorce);
                    Assert.Equal(users.Count, userFromSorce.Count);
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void ExecuteExpressionTestWithCustomResult(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers();

                    var userIds = users.Select(n => n.Id);

                    // TODO: Its' supported by NHibernate, but for EF no.. ( that's yuck !!!!! )
                    //var userFromSorce = facade.ExecuteExpression<User, List<string>>(
                    //    query => query.Where(user => users.Contains(user)).Select(user => user.Name)
                    //        .ToList());

                    var userFromSource = facade.ExecuteExpression<User, List<string>>(
                        query => query.Where(user => userIds.Contains(user.Id)).Select(user => user.Name)
                            .ToList());

                    // asserts
                    Assert.NotNull(userFromSource);
                    Assert.NotEmpty(userFromSource);
                    Assert.Equal(users.Count, userFromSource.Count);

                    // act
                    // result as anonymous !!
                    var userWithAnonymous = facade.ExecuteExpression((IQueryable<User> query) =>
                        query.Where(user => userIds.Contains(user.Id))
                            .Select(user => new { user.Name, user.Birthdate })
                            .ToList()
                        );

                    // asserts
                    Assert.NotNull(userWithAnonymous);
                    Assert.NotEmpty(userWithAnonymous);
                    Assert.Equal(users.Count, userWithAnonymous.Count);
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakeQueryPagingTest(ProviderTypes provider)
        {
            int numItems = 9;
            int pageSize = 2;

            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();
                var numPages = (numItems % pageSize > 0 ? 1 : 0) + numItems / pageSize;

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers(numItems);
                    facade.Clear();

                    var userIds = users.Select(n => n.Id);

                    var userFromSource = new List<User>();

                    // TODO: Its' supported by NHibernate, but for EF no.. ( that's yuck !!!!! )
                    //var queryPaging = facade.MakeQueryPaging<User>(query => query.Where(user => users.Contains(user)), pageSize);
                    var queryPaging = facade.MakeQueryPaging<User>(query => query.Where(user => userIds.Contains(user.Id)).OrderBy(user => user.Id), pageSize);

                    // asserts
                    Assert.NotNull(queryPaging);
                    Assert.Equal(numPages, queryPaging.PageCount);
                    Assert.Equal(pageSize, queryPaging.PageSize);
                    
                    foreach (var pageResult in queryPaging)
                    {
                        userFromSource.AddRange(pageResult.Result);
                    }
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        [Description("It's needed to review 'case it's got error on execution.")]
        public void MakeQueryPagingTestCustomResult(ProviderTypes provider)
        {
            int numItems = 17;
            int pageSize = 4;
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var numPages = (numItems % pageSize > 0 ? 1 : 0) + numItems / pageSize;

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers(numItems);

                    var userIds = users.Select(n => n.Id);

                    var userFromSource = new List<dynamic>();

                    // TODO: Its' supported by NHibernate, but for EF no.. ( that's yuck !!!!! )
                    //var queryPaging = facade.MakeQueryPaging<User, dynamic>(query => query.Where(user => users.Contains(user)).Select(user => new { user.Name, user.Birthdate }), pageSize);
                    var queryPaging = facade.MakeQueryPaging<User, dynamic>(query => query.Where(user => userIds.Contains(user.Id)).Select(user => new { user.Name, user.Birthdate }).OrderBy(t => t.Name), pageSize);

                    // asserts
                    Assert.NotNull(queryPaging);
                    Assert.Equal(numPages, queryPaging.PageCount);
                    Assert.Equal(pageSize, queryPaging.PageSize);
                    
                    foreach (var pageResult in queryPaging)
                    {
                        userFromSource.AddRange(pageResult.Result);
                    }

                    // asserts
                    Assert.Equal(users.Count, userFromSource.Count);
                }
            }
        }
    }
}
