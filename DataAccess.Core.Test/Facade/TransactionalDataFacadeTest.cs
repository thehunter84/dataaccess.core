﻿using System;
using System.Linq;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Model.DbTest;
using DataAccess.Core.Transactions;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Facade
{
    public class TransactionalDataFacadeTest : DbTestDataFacadeInitializer
    {
        public TransactionalDataFacadeTest(ITestOutputHelper output)
            : base(output)
        {
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithNoParams(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                // acts
                using (var tran = facade.BeginTransaction())
                {
                    // asserts
                    Assert.NotNull(tran);
                    Assert.NotNull(tran.Name);
                    Assert.NotEmpty(tran.Name);
                    Assert.Null(tran.Isolation);
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithAnyParams(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                // acts
                using (var tran = facade.BeginTransaction())
                {
                    // asserts
                    Assert.NotNull(tran);
                    Assert.NotNull(tran.Name);
                    Assert.NotEmpty(tran.Name);
                    Assert.Null(tran.Isolation);
                }

                // acts
                var tranName = "my-transaction";
                using (var tran = facade.BeginTransaction(tranName))
                {
                    // asserts
                    Assert.NotNull(tran);
                    Assert.NotNull(tran.Name);
                    Assert.NotEmpty(tran.Name);
                    Assert.Equal(tranName, tran.Name);
                    Assert.Null(tran.Isolation);
                }

                // acts
                var isolation = IsolationLevel.Chaos;
                using (var tran = facade.BeginTransaction(tranName, isolation))
                {
                    // asserts
                    Assert.NotNull(tran);
                    Assert.NotNull(tran.Name);
                    Assert.NotEmpty(tran.Name);
                    Assert.Equal(tranName, tran.Name);
                    Assert.Equal(isolation, tran.Isolation);
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTest(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1980, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    // acts
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Isolation = isolation }))
                    {
                        facade.MakePersistent(user);
                        tran.Commit();
                    }

                    this.Output.WriteLine($"No commit on the root transaction: {rootTran}");

                    // asserts
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithTransactionScope(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1990, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = isolation }))
                    {
                        facade.MakePersistent(user);
                        tran.Complete();
                    }

                    this.Output.WriteLine($"No commit on the root transaction: {rootTran}");

                    // asserts
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithTransactionScopeNested(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1990, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolation }))
                {
                    using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                    {
                        using (var tran1 = facade.BeginTransaction())
                        {
                            facade.MakePersistent(user);
                            tran1.Commit();
                        }

                        tran.Complete();
                    }

                    this.Output.WriteLine($"No commit on the root transaction: {rootTran}");
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithTransactionScopeNestedAndNoCommit(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1990, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    // asserts
                    Assert.Throws<InnerRollBackException>(() =>
                    {
                        using (var tran1 = facade.BeginTransaction())
                        {
                            facade.MakePersistent(user);

                            // asserts
                            Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                            this.Output.WriteLine($"No commit on the transaction: {tran1.Name}");
                        }
                    });

                    Assert.False(tran.CanBeCompleted());
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithManyTransactionScopeNestedAndNoCommit(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1990, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    // asserts
                    Assert.Throws<InnerRollBackException>(() =>
                    {
                        using (var tran1 = facade.BeginTransaction(new TransactionDescriptor { Name = "tran1" }))
                        {
                            using (var tran2 = facade.BeginTransaction(new TransactionDescriptor { Name = "tran2" }))
                            {
                                facade.MakePersistent(user);

                                // asserts
                                Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                                this.Output.WriteLine($"No commit on the transaction: {tran2.Name}");
                            }

                            this.Output.WriteLine($"No commit on the transaction: {tran1.Name}");
                        }
                    });

                    Assert.False(tran.CanBeCompleted());
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithManyTransactionScopeNestedAndNoCommitOnRoot(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1990, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                using (var rootTran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    // acts
                    using (var tran1 = facade.BeginTransaction(new TransactionDescriptor { Name = "tran1" }))
                    {
                        // acts
                        using (var tran2 = facade.BeginTransaction(new TransactionDescriptor { Name = "tran2" }))
                        {
                            facade.MakePersistent(user);

                            // asserts
                            Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                            tran2.Commit();
                        }

                        // asserts
                        Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                        tran1.Commit();
                    }

                    // asserts
                    Assert.True(rootTran.CanBeCompleted());
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithTransactionScopeAndNoCommit(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1990, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                // acts
                using (var tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    facade.MakePersistent(user);
                    
                    this.Output.WriteLine($"No commit on the transaction: {tran}");
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionTestWithNoCommit(ProviderTypes provider)
        {
            var user = new User { Name = Guid.NewGuid().ToString("D"), Birthdate = new DateTime(1980, 10, 10) };
            
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                using (var rootTran = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions {IsolationLevel = IsolationLevel.ReadCommitted}))
                {
                    // acts
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Isolation = IsolationLevel.ReadCommitted }))
                    {
                        facade.MakePersistent(user);
                        tran.Commit();
                    }

                    // asserts
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                    this.Output.WriteLine($"No commit on the transaction: {rootTran}");
                }
                
                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void BeginTransactionWithInnerExceptionOnInnerTransaction(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                // arrange (tries to load an instance, but if there's no instance to DB o It will be created a new one.)
                var user = facade.ExecuteExpression<User, User>(users => users.FirstOrDefault())
                           ?? facade.MakePersistentUser();

                var originalName = user.Name;

                using (var rootTran = facade.BeginTransaction("root", IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        using (var innerTran1 = facade.BeginTransaction("innerTran1", IsolationLevel.ReadCommitted))
                        {
                            user.Name = "DEMO - " + user.Name;

                            Assert.NotNull(user);

                            facade.MakePersistent(user);

                            innerTran1.Commit();
                        }

                        using (var innerTran2 = facade.BeginTransaction("innerTran2", IsolationLevel.ReadCommitted))
                        {
                            throw new Exception("this is a inner exception for demo..");
                        }
                    }
                    catch (Exception e)
                    {
                        Assert.IsType<InnerRollBackException>(e);
                        rootTran.Rollback();
                    }
                }

                var currentName = facade.ExecuteExpression<User, string>(users => users.Select(u => new { u.Id, u.Name }).First(us => us.Id == user.Id).Name);

                Assert.Equal(originalName, currentName);
            }
        }
    }
}
