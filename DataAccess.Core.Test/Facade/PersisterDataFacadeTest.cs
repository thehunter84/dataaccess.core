﻿using System;
using System.Collections.Generic;
using System.Transactions;
using DataAccess.Core.Model.DbTest;
using DataAccess.Core.Transactions;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Facade
{
    public class PersisterDataFacadeTest : DbTestDataFacadeInitializer
    {
        public PersisterDataFacadeTest(ITestOutputHelper output)
            : base(output)
        {            
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakePersistentTest(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUser();

                    // asserts
                    Assert.NotEqual(default(int), user.Id);
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakePersistentTestWithObjectTypeReference(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var user = facade.MakePersistentUserAsObject();

                    // asserts
                    Assert.NotEqual(default(int), user.Id);
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                    //Assert.True(user.Messages.All(message => message.Id.HasValue));

                    // act (delete instances created before)
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Name = "delete-instance", Isolation = IsolationLevel.ReadCommitted }))
                    {
                        facade.MakeTransient(user);
                        tran.Commit();
                    }
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakePersistentTestWithObjectTypeReferenceV2(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                User user;
                var isolation = IsolationLevel.ReadCommitted;
                using (var tran = facade.BeginTransaction("persist", isolation))
                {
                    // act
                    user = MockInstances.GetUser();
                    object objToPersist = user;
                    facade.MakePersistent(objToPersist);

                    // asserts
                    Assert.NotEqual(default(int), user.Id);
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                    tran.Commit();
                }

                using (
                    var tran = facade.BeginTransaction("persist", isolation))
                {
                    facade.MakeTransient(user);
                    tran.Commit();
                }

                // asserts
                Assert.True(!facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakePersistentTestMany(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers();

                    foreach (var user in users)
                    {
                        // asserts
                        Assert.NotEqual(default(int), user.Id);
                        Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                        //Assert.True(user.Messages.All(message => message.Id.HasValue));
                    }
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakePersistentTestManyWithObjectTypeReference(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsersAsObject();

                    foreach (var user in users)
                    {
                        // asserts
                        Assert.NotEqual(default(int), user.Id);
                        Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                        //Assert.True(user.Messages.All(message => message.Id.HasValue));
                    }
                }
            }
        }

        [Theory]
        [InlineData(ProviderTypes.NHibernate)]
        public void MakePersistentTestWithComposedKey(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var id = new ComposedKey{ Identifier = 10, Type = 'B' };

                var instance = new EntityWithComposedKey
                {
                    Id = id,
                    Name = Guid.NewGuid().ToString("N"),
                    Price = new EntityPrice { Id = id, Price = 15.36 }
                };

                instance.Jobs = new List<Job>
                {
                    new Job {Description = "desc 01", Parent = instance},
                    new Job {Description = "desc 02", Parent = instance},
                    new Job {Description = "desc 03", Parent = instance}
                };

                var isolation = IsolationLevel.ReadCommitted;

                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.Required,
                        new TransactionOptions {IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(240)}))
                {
                    using (var tran1 = new TransactionScope(TransactionScopeOption.Required,
                        new TransactionOptions { IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(240) }))
                    {
                        facade.MakePersistent(instance);
                        tran1.Complete();
                    }
                    
                    rootTran.Complete();
                }
                
                Assert.True(facade.Exists<EntityWithComposedKey>(key => key.Id == id));

                using (var tran1 = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(240) }))
                {
                    facade.MakeTransient(instance);
                    tran1.Complete();
                }

                Assert.False(facade.Exists<EntityWithComposedKey>(key => key.Id == id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakeTransientTestById(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                User user;
                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(30) }))
                {
                    // act
                    user = facade.MakePersistentUser();

                    // asserts
                    Assert.NotEqual(default(int), user.Id);
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                    // act (delete instances created before)
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Name = "delete-instance", Isolation = isolation }))
                    {
                        facade.MakeTransient<User>(user.Id);
                        tran.Commit();
                    }

                    // asserts
                    Assert.False(facade.Exists<User>(us => us.Id == user.Id));
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakeTransientTestByInstance(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                User user;
                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(30) }))
                {
                    // act
                    user = facade.MakePersistentUser();

                    // asserts
                    Assert.NotEqual(default(int), user.Id);
                    Assert.True(facade.Exists<User>(us => us.Id == user.Id));

                    // act (delete instances created before)
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Name = "delete-instance", Isolation = isolation }))
                    {
                        facade.MakeTransient(user);
                        tran.Commit();
                    }

                    // asserts
                    Assert.False(facade.Exists<User>(us => us.Id == user.Id));
                }

                // asserts
                Assert.False(facade.Exists<User>(us => us.Id == user.Id));
            }
        }
        
        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakeTransientTestByInstances(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers();

                    foreach (var user in users)
                    {
                        // asserts
                        Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                    }

                    // act (delete instances created before)
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Name = "delete-instance", Isolation = isolation }))
                    {
                        facade.MakeTransient<User>(users);
                        tran.Commit();
                    }

                    // asserts
                    foreach (var user in users)
                    {
                        Assert.False(facade.Exists<User>(us => us.Id == user.Id));
                    }
                }
            }
        }

        [Theory]
        [ClassData(typeof(ProvidersTestData))]
        public void MakeTransientTestByExpression(ProviderTypes provider)
        {
            using (var factory = this.DataFacadeFactoryProvider.Build(provider))
            {
                var facade = factory.BuildQueryableDataFacadeService();

                var isolation = IsolationLevel.ReadCommitted;
                using (
                    var rootTran = new TransactionScope(TransactionScopeOption.RequiresNew,
                        new TransactionOptions { IsolationLevel = isolation, Timeout = TimeSpan.FromSeconds(60) }))
                {
                    // act
                    var users = facade.MakePersistentManyUsers();

                    foreach (var user in users)
                    {
                        // asserts
                        Assert.True(facade.Exists<User>(us => us.Id == user.Id));
                    }

                    // act (delete instances created before)
                    using (var tran = facade.BeginTransaction(new TransactionDescriptor { Name = "delete-instances", Isolation = isolation }))
                    {
                        // TODO: Its' supported by NHibernate, but for EF no.. ( that's yuck !!!!! )
                        //facade.MakeTransient<User>(us => users.Contains(us));

                        foreach (var user in users)
                        {
                            facade.MakeTransient<User>(us => us.Id == user.Id);
                        }

                        tran.Commit();
                    }

                    // asserts
                    foreach (var user in users)
                    {
                        Assert.False(facade.Exists<User>(us => us.Id == user.Id));
                    }
                }
            }
        }
    }
}
