﻿namespace DataAccess.Core.Test
{
    /// <summary>
    /// Providers for all DAL implementations.
    /// </summary>
    public enum ProviderTypes
    {
        /// <summary>
        /// Specific EntityFramework implementation.
        /// </summary>
        EntityFramework = 1,

        /// <summary>
        /// Specific NHibernate implementation.
        /// </summary>
        NHibernate = 2,

        /// <summary>
        /// Specific NHibernate implementation, with stateless session.
        /// </summary>
        NHibernateStateless = 3
    }
}
