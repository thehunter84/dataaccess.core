﻿using System.Collections;
using System.Collections.Generic;

namespace DataAccess.Core.Test
{
    public class ProvidersTestData : IEnumerable<object[]>
    {
        private readonly List<object[]> list;

        public ProvidersTestData()
        {
            this.list = new List<object[]>
            {
                new object[]{ ProviderTypes.NHibernate },
                new object[]{ ProviderTypes.EntityFramework },
                new object[]{ ProviderTypes.NHibernateStateless }
            };
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
