﻿using System;
using System.ComponentModel;
using System.Linq;
using DataAccess.Core.Model.DbTest;
using DataAccess.Core.Model.Other;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Test.Impl.Nh;
using DataAccess.Core.Test.Impl.Nh.Mappings.Fluent;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Linq;
using NHibernate.Mapping.ByCode;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Extensions.Nh
{
    public class ConfigurationExtensionsTest
    {
        private readonly ITestOutputHelper output;
        private readonly IInterceptor interceptor;
        private readonly ConnectionStringProvider connectionStringProvider;

        public ConfigurationExtensionsTest(ITestOutputHelper output)
        {
            this.output = output;
            this.interceptor = new XUnitSqlStatementsInterceptor(this.output);
            this.connectionStringProvider = ConnectionStringProvider.Instance;
        }

        private string DevTestConnString => this.connectionStringProvider.GetConnectionString("devtest");

        [Fact]
        public void BuildConfigTest()
        {
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .ConnectionString(this.DevTestConnString)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            config.AddFluentMappings(typeof(UserMap),
                typeof(MessageMap),
                typeof(EntityWithComposedKeyMap),
                typeof(EntityWithStringKeyMap),
                typeof(EmployeeMap),
                typeof(StateEmployeeMap),
                typeof(PrivateEmployeeMap),
                typeof(EntityPriceMap),
                typeof(JobMap));

            Assert.NotNull(config.BuildSessionFactory());
        }

        [Fact]
        public void BuildConfigWithBatchMappingResolution()
        {
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .ConnectionString(this.DevTestConnString)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            config.AddFluentMappings(typeof(UserMap).Assembly);
            
            // arrange
            var sessionFactory = config.BuildSessionFactory();

            Assert.NotNull(sessionFactory);
        }

        [Fact]
        [Description("Test using odbc")]
        public void BuildConfigWithBatchMappingResolutionOdbc()
        {
            var connStr =
                "Server=devtest.csf0qmbajvt7.us-east-1.rds.amazonaws.com,1433;Database=DevTest;Uid=admin;Pwd=Latinboy1984_11;Driver={SQL Server Native Client 11.0}";

            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .Driver<NHibernate.Driver.OdbcDriver>()
                //.ConnectionString(this.DevTestConnString)
                .ConnectionString(connStr)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            config.AddFluentMappings(typeof(UserMap).Assembly);

            // arrange
            var sessionFactory = config.BuildSessionFactory();

            using (var session = sessionFactory.OpenSession())
            {
                var user = session.Query<User>().FirstOrDefault();
                Assert.NotNull(user);

                IQuery query = session.CreateSQLQuery("select * from [User]");
                query.SetFirstResult(0);
                query.SetMaxResults(1);

                var res = query.List();

                Assert.NotNull(res);

            }

            Assert.NotNull(sessionFactory);
        }

        [Fact]
        public void BuildConfigTestWithoutMappings()
        {
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .ConnectionString(this.DevTestConnString)
                .UseProxyValidator();

            Assert.NotNull(config.BuildSessionFactory());
        }

        [Fact]
        [Description("No connection name is indicated, so the below code throws an exception, but then It's fixed in order to build it.")]
        public void BuildConfigWithErrors()
        {
            // arranges
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .UseProxyValidator();

            // asserts
            Assert.ThrowsAny<InvalidOperationException>(() => config.BuildSessionFactory());

            // arranges
            config.ConnectionString(this.DevTestConnString);

            // asserts
            Assert.NotNull(config.BuildSessionFactory());
        }

        [Fact]
        [Description("No all mappers was added, so the below code throws an exception, but then It's set the connection name in order to fix it.")]
        public void BuildConfigWithMappings()
        {
            // arranges
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .ConnectionString(this.DevTestConnString)
                .UseProxyValidator();

            config.AddFluentMappings(typeof(UserMap));

            Assert.ThrowsAny<Exception>(() => config.BuildSessionFactory());

            config.AddFluentMappings(typeof(MessageMap));

            Assert.NotNull(config.BuildSessionFactory());
        }

        [Fact]
        [Description("No all mappers was added, so the below code throws an exception, but then It's set the connection name in order to fix it.")]
        public void BuildConfigWithMappingsTwoWaysTest()
        {
            // arranges
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .ConnectionString(this.DevTestConnString)
                .UseProxyValidator();

            config.AddFluentMappings(typeof(UserMap));
            
            config.AddFluentMappings(typeof(MessageMap));

            Assert.NotNull(config.BuildSessionFactory());
        }

        [Fact]
        public void BuildConfigUsingModelMapper()
        {
            ConventionModelMapper mapper = new ConventionModelMapper();

            // arranges
            var config = new Configuration();
            config.Dialect<MsSql2012Dialect>()
                .ConnectionString(this.DevTestConnString)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            mapper.AddMapping<UserMap>();
            mapper.AddMapping<MessageMap>();

            config.UseModelMapper(mapper);

            // asserts
            Assert.NotNull(config.BuildSessionFactory());
        }

        [Fact]
        [Description("No conventions is applied.. but at the end mapping created on the fly can be overriden by ClassMapping.")]
        public void BuildConfigUsingModelMapperWithConventions()
        {
            var mapper = new ModelMapper();

            // arranges
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .SetInterceptor(this.interceptor)
                .ConnectionString(this.DevTestConnString)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            mapper.AddMapping<UserMap>();

            // adding a new mapping in this way, It's configuring with default behaviour
            // mapper.Class<Message>(map => {});
            
            // this mapping reports a wrong table name, but the next mapping of the same instance overrides it.
            mapper.Class<Message>(map =>
            {
                map.Id(message => message.Id);
                map.Table("[UserMessage2]");
            });

            // The last entity mapping overrides previous definition.
            mapper.AddMapping<MessageMap>();

            config.UseModelMapper(mapper);

            // arranges
            var sessionFactory = config.BuildSessionFactory();

            // asserts
            Assert.NotNull(sessionFactory);

            using (sessionFactory)
            {
                using (var session = sessionFactory.OpenSession())
                {
                    var query = session
                        .Query<User>()
                        .First();

                    Assert.NotNull(query);

                    foreach (var message in query.Messages)
                    {
                        Assert.NotNull(message);
                    }
                }
            }
        }

        [Fact]
        public void BuildConfigUsingModelMapperWithMixStrategies()
        {
            var modelMapper = new ModelMapper();

            // arranges
            var config = new Configuration();

            config.Dialect<MsSql2012Dialect>()
                .SetInterceptor(this.interceptor)
                .ConnectionString(this.DevTestConnString)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            modelMapper.AddMapping<UserMap>();
            modelMapper.Class<Message>(map =>
            {
                map.Id(message => message.Id, mapper =>
                {
                    mapper.Generator(Generators.Native);
                    mapper.Type(NHibernateUtil.Int32);
                });
                map.Property(message => message.Body, mapper => mapper.Column("Message"));
                map.Property(message => message.SendDate);
                map.Table("[UserMessage]");
            });
            
            config.UseModelMapper(modelMapper);

            // arranges
            var sessionFactory = config.BuildSessionFactory();

            // asserts
            Assert.NotNull(sessionFactory);

            using (sessionFactory)
            {
                using (var session = sessionFactory.OpenSession())
                {
                    var query = session
                        .Query<User>()
                        .First();

                    Assert.NotNull(query);

                    foreach (var message in query.Messages)
                    {
                        Assert.NotNull(message);
                    }
                }
            }
        }

        [Fact]
        public void TestPostgreSql()
        {
            var connectionStr = "Server=salt.db.elephantsql.com;Database=zrtibyqv;User ID=zrtibyqv;Password=r3493xqSo55eO11YX_6pCMeU6TCNM4cv;Enlist=true;";

            var config = new NHibernate.Cfg.Configuration { Interceptor = new XUnitSqlStatementsInterceptor(this.output) }
                .Dialect<NHibernate.Dialect.PostgreSQLDialect>()
                //.Driver<NHibernate.Driver.NpgsqlDriver>()
                .ConnectionString(connectionStr)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator();

            var mapper = new ModelMapper();
            mapper.Class<Demo>(map =>
            {
                map.Id(demo => demo.Id, idMapper =>
                {
                    idMapper.Column("tstamp");
                    idMapper.Generator(Generators.Native);
                });

                map.Property(demo => demo.Val1, propertyMapper => propertyMapper.Column("fld1"));

                map.Property(demo => demo.Val2, propertyMapper => propertyMapper.Column("fld2"));

                map.Schema("db01cwe5879");
                map.Table("ad10min1_average");
            });

            config.UseModelMapper(mapper);

            var sessionfacotyr = config.BuildSessionFactory();

            Assert.NotNull(sessionfacotyr);

            using (sessionfacotyr)
            {
                using (var session = sessionfacotyr.OpenSession())
                {
                    var query = session.Query<Demo>();
                    var result = query.Skip(10).Take(10).ToList();

                    Assert.NotNull(result);
                }
            }
        }
    }
}
