﻿using System.Data;
using DataAccess.Core.Nh;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Test.Impl.Nh.Factory;
using DataAccess.Core.Test.Impl.Nh.Linq;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Driver;
using NHibernate.Exceptions;
using NHibernate.Transaction;
using Xunit;

namespace DataAccess.Core.Test.Extensions.Nh
{
    public class ConfigPropertiesExtensionsTest
    {
        [Fact]
        public void AllExtensionMethodsTest()
        {
            var config = new Configuration();

            // arrange
            config.Dialect<NHibernate.Dialect.MsSql2012Dialect>()
                .ConnectionName("connname")
                .ConnectionString("myconn")
                .KeywordsAutoImport(Hbm2DDLKeyWords.Keywords)
                .ShowSql()
                .FormatSql()
                .ConnectionProvider<DriverConnectionProvider>()
                .Driver<SqlServerCeDriver>()
                .IsolationLevel(IsolationLevel.Snapshot)
                .ConnectionReleaseMode(ConnectionReleaseMode.AfterTransaction)
                .BatcherFactory<CustomBatcherFactory>()
                .BatchSize(10)
                .OrderInserts()
                .OrderUpdates()
                .TransactionFactory<AdoNetTransactionFactory>()
                .PrepareSql()
                .Timeout(10)
                .SqlExceptionConverter<SQLStateConverter>()
                .AutoCommentSql()
                .HqlToSqlSubstitutions("cdfdf")
                .MaxFetchDepth()
                .SchemaAction(SchemaAutoAction.Validate)
                .QueryModelRewriterFactory<CustomQueryModelRewriterFactory>()
                .GenerateStatistics()
                .UseProxyValidator()
                .ProxyFactory<StdProxyFactoryFactory>();

            var props = config.Properties;

            // asserts
            Assert.True(props.ContainsKey(CfgProperties.Dialect));
            Assert.True(props.ContainsKey(CfgProperties.ConnectionName));
            Assert.True(props.ContainsKey(CfgProperties.ConnectionString));
            Assert.True(props.ContainsKey(CfgProperties.Hbm2DdlKeywords));
            Assert.True(props.ContainsKey(CfgProperties.ShowSql));
            Assert.True(props.ContainsKey(CfgProperties.FormatSql));
            Assert.True(props.ContainsKey(CfgProperties.ConnectionProvider));
            Assert.True(props.ContainsKey(CfgProperties.Driver));
            Assert.True(props.ContainsKey(CfgProperties.Isolation));
            Assert.True(props.ContainsKey(CfgProperties.ConnectionReleaseMode));
            Assert.True(props.ContainsKey(CfgProperties.BatcherFactory));
            Assert.True(props.ContainsKey(CfgProperties.BatchSize));
            Assert.True(props.ContainsKey(CfgProperties.OrderInserts));
            Assert.True(props.ContainsKey(CfgProperties.OrderUpdates));
            Assert.True(props.ContainsKey(CfgProperties.TransactionFactory));
            Assert.True(props.ContainsKey(CfgProperties.PrepareSql));
            Assert.True(props.ContainsKey(CfgProperties.CommandTimeout));
            Assert.True(props.ContainsKey(CfgProperties.SqlExceptionConverter));
            Assert.True(props.ContainsKey(CfgProperties.AutoCommentSql));
            Assert.True(props.ContainsKey(CfgProperties.HqlToSqlSubstitutions));
            Assert.True(props.ContainsKey(CfgProperties.MaxFetchDepth));
            Assert.True(props.ContainsKey(CfgProperties.SchemaAction));
            Assert.True(props.ContainsKey(CfgProperties.QueryRewriterFactory));
            Assert.True(props.ContainsKey(CfgProperties.GenerateStatistics));
            Assert.True(props.ContainsKey(CfgProperties.UseProxyValidator));

            // everything is OK !!!
        }

        [Fact]
        public void ConnectionNameTest()
        {
            var config = new Configuration();
            var connectionName = "mycurrentconnection";

            // arranges
            config.ConnectionName(connectionName);

            // asserts
            Assert.Equal(config.Properties[CfgProperties.ConnectionName], connectionName);

            // arranges
            connectionName = "mycurrentconnection_modified";
            config.ConnectionName(connectionName);

            // asserts
            Assert.Equal(config.Properties[CfgProperties.ConnectionName], connectionName);
        }
    }
}
