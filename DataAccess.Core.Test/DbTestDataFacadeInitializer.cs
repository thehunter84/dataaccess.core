﻿using Xunit.Abstractions;

namespace DataAccess.Core.Test
{
    public class DbTestDataFacadeInitializer
    {
        public DbTestDataFacadeInitializer(ITestOutputHelper output)
        {
            this.Output = output;
            this.DataFacadeFactoryProvider = new DbTestDataFacadeFactoryProvider(output);
        }

        protected ITestOutputHelper Output { get; }

        protected IDataFacadeFactoryProvider DataFacadeFactoryProvider { get; }
    }
}
