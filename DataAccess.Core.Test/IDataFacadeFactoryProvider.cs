﻿namespace DataAccess.Core.Test
{
    public interface IDataFacadeFactoryProvider
    {
        IDataFacadeFactory Build(ProviderTypes provider);
    }
}
