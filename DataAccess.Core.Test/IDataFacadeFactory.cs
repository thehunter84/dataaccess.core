﻿using System;
using DataAccess.Core.Providers;

namespace DataAccess.Core.Test
{
    public interface IDataFacadeFactory : IDisposable
    {
        IQueryableDataFacadeService BuildQueryableDataFacadeService();

        IQueryDataFacade BuildQueryDataFacade(INamedQueryProvider queryProvider);
    }
}
