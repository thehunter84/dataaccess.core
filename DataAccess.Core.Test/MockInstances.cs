﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using DataAccess.Core.Model.DbTest;
using DataAccess.Core.Transactions;

namespace DataAccess.Core.Test
{
    internal static class MockInstances
    {
        public static User GetUser()
        {
            return new User
            {
                Name = Guid.NewGuid().ToString("D"),
                Birthdate = new DateTime(1980, 10, 10),
                Messages = new List<Message>
                {
                    new Message
                    {
                        Body = "Custom Message from mock *******" + Guid.NewGuid().ToString("N"),
                        SendDate = DateTime.Now
                    },
                    new Message
                    {
                        Body = "Custom Message from mock *******" + Guid.NewGuid().ToString("N"),
                        SendDate = DateTime.Now
                    }
                }
            };
        }

        internal static User MakePersistentUser(this IQueryableDataFacadeService facade, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var user = MockInstances.GetUser();

            using (var transaction = facade.BeginTransaction(new TransactionDescriptor { Isolation = isolationLevel }))
            {
                facade.MakePersistent(user);
                transaction.Commit();
            }

            return user;
        }

        internal static List<User> MakePersistentManyUsers(this IQueryableDataFacadeService facade, int counter = 3)
        {
            var users = new List<User>();

            for (int i = 0; i < counter; i++)
            {
                users.Add(MockInstances.GetUser());
            }

            using (var transaction = facade.BeginTransaction(new TransactionDescriptor { Isolation = IsolationLevel.ReadCommitted }))
            {
                facade.MakePersistent<User>(users);
                transaction.Commit();
            }

            return users;
        }

        internal static User MakePersistentUserAsObject(this IQueryableDataFacadeService facade)
        {
            var user = MockInstances.GetUser();
            object objToPersist = user;

            using (var transaction = facade.BeginTransaction(new TransactionDescriptor { Isolation = IsolationLevel.ReadCommitted }))
            {
                facade.MakePersistent(objToPersist);
                transaction.Commit();
            }

            return user;
        }

        internal static List<User> MakePersistentManyUsersAsObject(this IQueryableDataFacadeService facade)
        {
            var users = new List<User>
            {
                MockInstances.GetUser(),
                MockInstances.GetUser(),
                MockInstances.GetUser()
            };

            IEnumerable<object> userAsObject = users.Select<User, object>(us => us).ToList();

            using (var transaction = facade.BeginTransaction(new TransactionDescriptor { Isolation = IsolationLevel.ReadCommitted }))
            {
                //facade.MakePersistent<object>(userAsObject);
                facade.MakePersistent(userAsObject);
                transaction.Commit();
            }

            return users;
        }
    }
}
