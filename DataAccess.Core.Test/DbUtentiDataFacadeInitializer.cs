﻿using Xunit.Abstractions;

namespace DataAccess.Core.Test
{
    public class DbUtentiDataFacadeInitializer
    {
        public DbUtentiDataFacadeInitializer(ITestOutputHelper output)
        {
            this.Output = output;
            this.DataFacadeFactoryProvider = new DbUtentiDataFacadeFactoryProvider(output);
        }

        protected ITestOutputHelper Output { get; }

        protected IDataFacadeFactoryProvider DataFacadeFactoryProvider { get; }
    }
}
