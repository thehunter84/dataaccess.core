﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.Core.Paging;
using Xunit;
using Xunit.Abstractions;

namespace DataAccess.Core.Test.Paging
{
    public class QueryablePagingTest
    {
        private readonly ITestOutputHelper output;

        public QueryablePagingTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void TotalItemsTest()
        {
            var l1 = new List<string>
            {
                "01", "02", "03", "04", "05", "06", "07", "08", "09", "10"
            };

            var q = new Core.Paging.QueryablePaging<string>(() => l1.AsQueryable(), 4);

            Assert.Equal(4, q.PageSize);
            Assert.Equal(3, q.PageCount);
            Assert.Equal(3, q.PageCountAsLong);
            Assert.Equal(10, q.TotalItems);

            l1.Add("11");
            l1.Add("12");
            l1.Add("13");

            Assert.Equal(4, q.PageSize);
            Assert.Equal(4, q.PageCount);
            Assert.Equal(4, q.PageCountAsLong);
            Assert.Equal(13, q.TotalItems);
        }

        [Fact]
        public void TotalItemsWithEmptyQueryableTest()
        {
            var q = new Core.Paging.QueryablePaging<string>(() => Enumerable.Empty<string>().AsQueryable(), 4);

            Assert.Equal(4, q.PageSize);
            Assert.Equal(0, q.PageCount);
            Assert.Equal(0, q.PageCountAsLong);
            Assert.Equal(0, q.TotalItems);
        }
        
        [Fact]
        public void ForeachTest()
        {
            var l1 = new List<string>
            {
                "01", "02", "03", "04", "05", "06", "07", "08", "09", "10"
            };

            var q = new Core.Paging.QueryablePaging<string>(() => l1.AsQueryable(), 4);

            // asserts
            Assert.Equal(4, q.PageSize);
            Assert.Equal(3, q.PageCount);
            Assert.Equal(3, q.PageCountAsLong);
            Assert.Equal(10, q.TotalItems);

            var counter = 0;
            foreach (var pageResult in q)
            {
                this.PrintPageResult(pageResult);
                counter++;
            }

            // asserts
            Assert.Equal(3, counter);

            // arranges
            l1.Add("11");
            l1.Add("12");
            l1.Add("13");

            counter = 0;
            foreach (var pageResult in q)
            {
                this.PrintPageResult(pageResult);
                counter++;
            }

            // asserts
            Assert.Equal(4, counter);
        }

        [Fact]
        public void WrongForeachTest()
        {
            var l1 = new List<string>
            {
                "01", "02", "03", "04", "05", "06", "07", "08", "09", "10"
            };

            var q = new Core.Paging.QueryablePaging<string>(() => l1.AsQueryable(), 4);

            Assert.Equal(4, q.PageSize);
            Assert.Equal(3, q.PageCount);
            Assert.Equal(3, q.PageCountAsLong);
            Assert.Equal(10, q.TotalItems);

            // asserts
            Assert.Throws<InvalidOperationException>(() =>
            {
                foreach (var pageResult in q)
                {
                    this.PrintPageResult(pageResult);

                    // arranges
                    l1.RemoveAt(0);
                }
            });
        }

        private void PrintPageResult<TResult>(PageResult<TResult> pageResult)
        {
            this.output.WriteLine($"This is the page number: {pageResult.Index}");

            foreach (var result in pageResult.Result)
            {
                this.output.WriteLine($"{result}");
            }

            this.output.WriteLine(string.Empty);
        }
    }
}
