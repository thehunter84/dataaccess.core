﻿#if NETFRAMEWORK
using System;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Test.Impl.Ef;
using DataAccess.Core.Test.Impl.Ef.Contexts;
using DataAccess.Core.Test.Impl.Nh;
using DataAccess.Core.Test.Impl.Nh.Mappings.Fluent;
using Xunit.Abstractions;
#else
using System;
using DataAccess.Core.Nh.Extensions;
using DataAccess.Core.Test.Impl.Ef;
using DataAccess.Core.Test.Impl.Ef.Contexts;
using DataAccess.Core.Test.Impl.Nh;
using DataAccess.Core.Test.Impl.Nh.Mappings.Fluent;
using Microsoft.EntityFrameworkCore;
using Xunit.Abstractions;
#endif

namespace DataAccess.Core.Test
{
    public class DbTestDataFacadeFactoryProvider : IDataFacadeFactoryProvider
    {
        private readonly ITestOutputHelper output;
        private readonly ConnectionStringProvider connectionStringProvider;

        public DbTestDataFacadeFactoryProvider(ITestOutputHelper output)
        {
            this.output = output;
            this.connectionStringProvider = ConnectionStringProvider.Instance;
        }

        public IDataFacadeFactory Build(ProviderTypes provider)
        {
            var connectionStr = this.connectionStringProvider.GetConnectionString("devtest");

            switch (provider)
            {
                case ProviderTypes.EntityFramework:
                {
                    return this.MakeEfDataFacadeFactory(connectionStr);
                }
                case ProviderTypes.NHibernate:
                {
                    return this.MakeNhDataFacadeFactory(connectionStr);
                }
                case ProviderTypes.NHibernateStateless:
                {
                    return this.MakeNhDataFacadeFactory(connectionStr, true);
                }
                default:
                {
                    throw new NotImplementedException();
                }
            }
        }

        private IDataFacadeFactory MakeNhDataFacadeFactory(string connectionStr, bool useStateless = false)
        {
            var config = new NHibernate.Cfg.Configuration { Interceptor = new XUnitSqlStatementsInterceptor(this.output) }
                .Dialect<NHibernate.Dialect.MsSql2012Dialect>()
                //.Dialect<NHibernate.Dialect.MsSql2008Dialect>()
                .ConnectionString(connectionStr)
                .GenerateStatistics()
                .ShowSql()
                .UseProxyValidator()
                .AddFluentMappings(typeof(UserMap),
                    typeof(MessageMap),
                    typeof(EntityWithComposedKeyMap),
                    typeof(EntityWithStringKeyMap),
                    typeof(EmployeeMap),
                    typeof(StateEmployeeMap),
                    typeof(PrivateEmployeeMap),
                    typeof(EntityPriceMap),
                    typeof(JobMap));

            return new NhDataFacadeFactory(config.BuildSessionFactory(), useStateless);
        }

        private IDataFacadeFactory MakeEfDataFacadeFactory(string connectionStr)
        {
#if NETFRAMEWORK
            var dbContext = new DevDbContext(connectionStr)
            {
                Database =
                {
                    Log = output.WriteLine
                }
            };

            return new EfDataFacadeFactory(dbContext);
#else
            var optionsBuilder = new DbContextOptionsBuilder<DevDbContext>()
                .UseSqlServer(connectionStr, builder =>
                {
                    //builder.UseRowNumberForPaging();
                })
                .UseLazyLoadingProxies();

            return new EfDataFacadeFactory(new DevDbContext(optionsBuilder.Options));
#endif
        }

    }
}
