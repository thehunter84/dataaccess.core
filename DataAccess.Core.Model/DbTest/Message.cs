﻿using System;

namespace DataAccess.Core.Model.DbTest
{
    public class Message
    {
        public int? Id { get; set; }

        public string Body { get; set; }

        public DateTime? SendDate { get; set; }
    }
}
