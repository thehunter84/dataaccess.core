﻿namespace DataAccess.Core.Model.DbTest
{
    public abstract class Employee
    {
        public int? Id { get; set; }

        public IndustryType IndustryType { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Taxcode { get; set; }
    }
}
