﻿namespace DataAccess.Core.Model.DbTest
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public virtual StudentAddress Address { get; set; }
    }
}
