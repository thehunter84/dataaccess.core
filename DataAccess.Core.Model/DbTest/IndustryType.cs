﻿namespace DataAccess.Core.Model.DbTest
{
    public enum IndustryType
    {
        Engineering = 1,

        Chemistry = 2,

        Pharmaceutical = 3,

        Telecommunications = 4
    }
}
