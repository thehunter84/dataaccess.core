﻿using System;
using System.Collections.Generic;

namespace DataAccess.Core.Model.DbTest
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Birthdate { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
