﻿using System.Collections.Generic;

namespace DataAccess.Core.Model.DbTest
{
    public class EntityWithComposedKey
    {
        public ComposedKey Id { get; set; }

        public string Name { get; set; }

        public EntityPrice Price { get; set; }

        public virtual ICollection<Job> Jobs { get; set; } = new List<Job>();

        public EntityWithComposedKey AddJob(Job job)
        {
            job.Parent = this;
            this.Jobs.Add(job);
            
            return this;
        }
    }

    public class ComposedKey
    {
        public int? Identifier { get; set; }

        public char? Type { get; set; }

        /// <summary>
        /// Serves as the default hash function. 
        /// </summary>
        /// <returns>
        /// A hash code for the current object.
        /// </returns>
        public override int GetHashCode()
        {
            return 13 * (this.Identifier.GetValueOrDefault() - this.Type.GetValueOrDefault().GetHashCode());
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <returns>
        /// true if the specified object  is equal to the current object; otherwise, false.
        /// </returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals(object obj)
        {
            if (obj is ComposedKey)
            {
                return this.GetHashCode() == obj.GetHashCode();
            }

            return false;
        }
    }
}
