﻿namespace DataAccess.Core.Model.DbTest
{
    public class UserBenefit
    {
        public int? Id { get; set; }
        public virtual User User { get; set; }
        public virtual Benefit Benefit { get; set; }
    }
}
