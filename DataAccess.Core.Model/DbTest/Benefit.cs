﻿namespace DataAccess.Core.Model.DbTest
{
    public class Benefit
    {
        public byte? Id { get; set; }
        public string Name { get; set; }
    }
}
