﻿namespace DataAccess.Core.Model.DbTest
{
    public class EntityPrice
    {
        public ComposedKey Id { get; set; }

        public double Price { get; set; }
    }
}
