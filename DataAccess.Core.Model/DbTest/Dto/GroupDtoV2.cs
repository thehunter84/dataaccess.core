﻿namespace DataAccess.Core.Model.DbTest.Dto
{
    public class GroupDtoV2
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? IdGruppo { get; set; }

        public string MacroGroup { get; set; }
    }
}
