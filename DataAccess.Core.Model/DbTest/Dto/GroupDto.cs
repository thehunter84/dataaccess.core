﻿namespace DataAccess.Core.Model.DbTest.Dto
{
    public class GroupDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int GroupId { get; set; }
        
        public string MacroGroup { get; set; } 
    }
}
