﻿namespace DataAccess.Core.Model.DbTest
{
    public class Job
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public virtual EntityWithComposedKey Parent { get; set; }
    }
}
