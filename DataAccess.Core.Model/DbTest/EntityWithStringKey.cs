﻿namespace DataAccess.Core.Model.DbTest
{
    public class EntityWithStringKey
    {
        public string Id { get; set; }

        public string CurrentVal { get; set; }
    }
}
