﻿namespace DataAccess.Core.Model.DbUtenti
{
    public class MacroGroup
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
