﻿namespace DataAccess.Core.Model.DbUtenti
{
    /// <summary>
    /// 
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public byte Id { get; set; }

        /// <summary>
        /// Gets or sets the denomination.
        /// </summary>
        /// <value>
        /// The denomination.
        /// </value>
        public string Denomination { get; set; }
    }
}
