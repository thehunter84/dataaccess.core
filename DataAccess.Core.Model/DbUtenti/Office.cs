﻿namespace DataAccess.Core.Model.DbUtenti
{
    public class Office
    {
        public int? Id { get; set; }

        public string Country { get; set; }

        public string Province { get; set; }

        public string City { get; set; }
    }
}
