﻿namespace DataAccess.Core.Model.DbUtenti
{
    /// <summary>
    /// Rappresents the User activation status.
    /// </summary>
    public enum ActivationStatus : byte
    {
        /// <summary>
        /// Indicates that an user is just created, and It needs to be created in ActiveDirectory.
        /// </summary>
        JustCreated = 0,

        /// <summary>
        /// Indicates that an user is work in progress by another proccess in order to create it in ActiveDirectory.
        /// </summary>
        InProgress = 1,

        /// <summary>
        /// Indicates an user was modified, so It's needed to be synchronized with ActiveDirectory.
        /// </summary>
        Modified = 4,

        /// <summary>
        /// Indicates that the synchronization with ActiveDirectory was failed.
        /// </summary>
        SyncError = 9,

        /// <summary>
        /// Indicates an user is synchronized with ActiveDirectory and viceversa.
        /// </summary>
        Synchronized = 10
    }
}
