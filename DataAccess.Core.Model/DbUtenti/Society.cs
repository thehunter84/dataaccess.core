﻿namespace DataAccess.Core.Model.DbUtenti
{
    public class Society
    {
        public int? Id { get; set; }

        public string Name { get; set; }
    }
}
