﻿namespace DataAccess.Core.Model.DbUtenti
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainControllerCfg
    {
        /// <summary>
        /// Gets or sets the identifier company.
        /// </summary>
        /// <value>
        /// The identifier company.
        /// </value>
        public byte IdCompany { get; set; }

        /// <summary>
        /// Gets or sets the domain controller.
        /// </summary>
        /// <value>
        /// The domain controller.
        /// </value>
        public string DomainController { get; set; }

        /// <summary>
        /// Gets or sets the organization unit.
        /// </summary>
        /// <value>
        /// The organization unit.
        /// </value>
        public string OrganizationUnit { get; set; }

        /// <summary>
        /// Gets the fullname container rappresentation.
        /// </summary>
        /// <returns></returns>
        public string GetContainer()
        {
            return string.Format("{0},{1}", this.OrganizationUnit, this.DomainController);
        }
    }
}
