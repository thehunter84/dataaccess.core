﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Core.Model.Other
{
    public class Demo
    {
        public long Id { get; set; }

        public double Val1 { get; set; }

        public double Val2 { get; set; }
    }
}
