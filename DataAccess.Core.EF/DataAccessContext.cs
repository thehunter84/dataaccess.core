﻿#if !NETSTANDARD2_0
using System;
using System.Data.Entity;
using System.Diagnostics;
using DataAccess.Core.Extensions;

namespace DataAccess.Core.EF
{
    /// <summary>
    /// A basic DbContext used to deduce connection string name based on the given derived DbContext name.
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    /// <seealso cref="DbContext" />
    public abstract class DataAccessContext : DbContext
    {
        private const string DbContextClass = nameof(DbContext);

        /// <summary>
        /// Create an instance with default connection string name inferred by class name.
        /// </summary>
        protected DataAccessContext()
            : this(GetConnectionString())
        {
        }

        /// <summary>
        /// Create an instance with the connectionString or connectionStringName.
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        protected DataAccessContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        private static string GetConnectionString()
        {
            var stackFrame = new StackFrame(2);
            var declaringTypeName = stackFrame.GetMethod()?
                .DeclaringType?
                .Name;

            if (declaringTypeName == null || !declaringTypeName.EndsWith(DbContextClass, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new InvalidOperationException($"The derived DbContext class must contain DbContext suffix, class name: {declaringTypeName}");
            }

            var connectionName = declaringTypeName.Substring(0, declaringTypeName.Length - DbContextClass.Length);

            connectionName.ThrowException(string.IsNullOrWhiteSpace, s => new InvalidOperationException($"The connection name is null or empty, class name: {declaringTypeName}"));

            return connectionName;
        }
    }
}
#endif