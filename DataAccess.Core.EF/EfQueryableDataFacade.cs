﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Paging;
using DataAccess.Core.Providers;
using Microsoft.EntityFrameworkCore;
#else
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Paging;
using DataAccess.Core.Providers;
#endif

namespace DataAccess.Core.EF
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IQueryableDataFacade" />
    public class EfQueryableDataFacade
        : IQueryableDataFacade
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EfQueryableDataFacade"/> class.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        public EfQueryableDataFacade(IContextProvider<DbContext> contextProvider)
        {
            this.ContextProvider = contextProvider;
        }

        /// <summary>
        /// Gets the context provider.
        /// </summary>
        /// <value>
        /// The context provider.
        /// </value>
        protected IContextProvider<DbContext> ContextProvider { get; }

        /// <inheritdoc/>
        public TEntity Get<TEntity>(object identifier) where TEntity : class
        {
            var set = this.ContextProvider.Set<TEntity>();

            dynamic id = identifier;
            if (!(identifier is string) && identifier.IsCollection())
            {
                id = Enumerable.ToArray(Enumerable.Cast<object>(id));
            }

            return set.Find(id);
        }

        /// <inheritdoc/>
        public bool Exists<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
        {
            return this.ContextProvider.Set<TEntity>()
                .Count(expression) > 0;
        }

        /// <inheritdoc/>
        public TEntity UniqueResult<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
        {
            return this.ContextProvider.Set<TEntity>()
                .SingleOrDefault(expression);
        }

        /// <inheritdoc/>
        public IEnumerable<TEntity> ApplyWhere<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
        {
            return this.ContextProvider.Set<TEntity>()
                .Where(expression)
                .ToList();
        }

        /// <inheritdoc/>
        public IEnumerable<TEntity> ExecuteExpression<TEntity>(Func<IQueryable<TEntity>, IQueryable<TEntity>> queryExpression) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();
            return queryExpression.Invoke(queryable)
                .ToList();
        }

        /// <inheritdoc/>
        public TResult ExecuteExpression<TEntity, TResult>(Func<IQueryable<TEntity>, TResult> queryExpression) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();

            return queryExpression.Invoke(queryable);
        }

        /// <inheritdoc/>
        public IQueryPaging<TEntity> MakeQueryPaging<TEntity>(Func<IQueryable<TEntity>, IQueryable<TEntity>> queryExpression, int pageSize = 10) where TEntity : class
        {
            return MakeQueryPaging<TEntity, TEntity>(queryExpression, pageSize);
        }

        /// <inheritdoc/>
        public IQueryPaging<TResult> MakeQueryPaging<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryExpression, int pageSize = 10) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();

            return new QueryablePaging<TResult>(() => queryExpression.Invoke(queryable), pageSize);
        }

        /// <inheritdoc/>
        public IFutureQueryBatch MakeFutureQueryBatch()
        {
            throw new NotImplementedException("This feature is not implemented yet !!!");
        }
    }
}
