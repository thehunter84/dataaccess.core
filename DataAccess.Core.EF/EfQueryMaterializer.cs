﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Providers;
using Microsoft.EntityFrameworkCore;
#else
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Providers;
#endif

namespace DataAccess.Core.EF
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="EfQueryDescriptor" />
    /// <seealso cref="IQueryMaterializer" />
    public class EfQueryMaterializer
        : EfQueryDescriptor, IQueryMaterializer
    {
        private readonly IContextProvider<DbContext> contextProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="EfQueryMaterializer"/> class.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="name">The name.</param>
        /// <param name="statement">The statement.</param>
        /// <param name="parameters">The parameters.</param>
        public EfQueryMaterializer(IContextProvider<DbContext> contextProvider, string name, string statement, IEnumerable<IQueryArgument> parameters)
            : base(name, statement, parameters)
        {
            this.contextProvider = contextProvider;
        }

        /// <inheritdoc />
        public IEnumerable<TResult> Execute<TResult>()
        {
            return this.ExecuteQuery<TResult, IEnumerable<TResult>>(results => results.ToList());
        }
        
        /// <inheritdoc />
        public TResult UniqueResult<TResult>()
        {
            return this.ExecuteQuery<TResult, TResult>(results => results.SingleOrDefault());
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="executor">The executor.</param>
        /// <returns></returns>
        private TResult ExecuteQuery<TInstance, TResult>(Func<IEnumerable<TInstance>, TResult> executor)
        {
            try
            {
                this.SetDefaultMissingParameters();
                
                var result =
                    executor(this.contextProvider.MakeQuery<TInstance>(this.Statement, this.SqlParameters.Values.ToArray()));

                this.SqlParameters.Clear();
                return result;
            }
            catch (Exception)
            {
                this.SqlParameters.Clear();
                throw;
            }
        }
    }
}

