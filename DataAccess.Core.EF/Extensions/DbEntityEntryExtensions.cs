﻿#if NETSTANDARD2_0
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DataAccess.Core.EF.Extensions
{
    internal static class DbEntityEntryExtensions
    {
        internal static void Detach(this EntityEntry entry)
        {
            if (entry != null && entry.State != EntityState.Detached)
            {
                entry.State = EntityState.Detached;
            }
        }

        internal static void Restore(this EntityEntry entry)
        {
            if (entry != null && entry.State == EntityState.Modified)
            {
                entry.Reload();
            }
        }

        internal static bool IsDirty(this EntityEntry entry)
        {
            return entry != null && entry.State == EntityState.Modified;
        }

        internal static bool IsCached(this EntityEntry entry)
        {
            return entry != null && entry.State != EntityState.Detached;
        }
    }
}

#else

using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DataAccess.Core.EF.Extensions
{
    internal static class DbEntityEntryExtensions
    {
        internal static void Detach(this DbEntityEntry entry)
        {
            if (entry != null && entry.State != EntityState.Detached)
            {
                entry.State = EntityState.Detached;
            }
        }

        internal static void Restore(this DbEntityEntry entry)
        {
            if (entry != null && entry.State == EntityState.Modified)
            {
                entry.Reload();
            }
        }

        internal static bool IsDirty(this DbEntityEntry entry)
        {
            return entry != null && entry.State == EntityState.Modified;
        }

        internal static bool IsCached(this DbEntityEntry entry)
        {
            return entry != null && entry.State != EntityState.Detached;
        }
    }
}
#endif
