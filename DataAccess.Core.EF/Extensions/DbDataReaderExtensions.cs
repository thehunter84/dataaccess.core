﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using DataAccess.Core.Extensions;

namespace DataAccess.Core.EF.Extensions
{
    internal static class DbDataReaderExtensions
    {
        internal static List<PropertyTypeColumn> GetPropertyColumns(this DbDataReader reader)
        {
            var cols = new List<PropertyTypeColumn>();

            for (var i = 0; i < reader.FieldCount; i++)
            {
                cols.Add(new PropertyTypeColumn
                {
                    Index = i,
                    Name = reader.GetName(i),
                    Type = reader.GetFieldType(i)
                });
            }

            return cols;
        }

        internal static IEnumerable<TInstance> Materialize<TInstance>(this DbDataReader reader)
        {
            var columns = reader.GetPropertyColumns();

            if (columns.Count == 0)
            {
                return Enumerable.Empty<TInstance>();
            }

            return columns.Count == 1 ? reader.Read<TInstance>(columns.First()) : reader.Read<TInstance>(columns);
        }

        private static List<TInstance> Read<TInstance>(this DbDataReader reader, PropertyTypeColumn column)
        {
            List<TInstance> list = new List<TInstance>();

            while (reader.Read())
            {
                var val = reader.GetValue(column.Index);

                // verify if the result is DBNull
                list.Add(val as dynamic);
            }

            return list;
        }

        private static List<TInstance> Read<TInstance>(this DbDataReader reader, List<PropertyTypeColumn> columns)
        {
            var type = typeof(TInstance);
            List<TInstance> list = new List<TInstance>();

            while (reader.Read())
            {
                TInstance instance = reader.MakeInstance(columns, type) as dynamic;
                list.Add(instance);
            }

            return list;
        }

        private static object MakeInstance(this DbDataReader reader, List<PropertyTypeColumn> columns, Type type)
        {
            var obj = Activator.CreateInstance(type);
            var allProperties = type.GetProperties()
                .Where(info => !info.PropertyType.IsComplex());

            foreach (var property in allProperties)
            {
                var column = columns.FirstOrDefault(typeColumn =>
                    typeColumn.Name.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase));

                if (column != null)
                {
                    var val = reader.GetValue(column.Index);
                    property.SetValue(obj, val);
                }
            }

            return obj;
        }
    }
}
