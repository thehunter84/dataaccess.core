﻿#if NETSTANDARD2_0
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataAccess.Core.EF.Extensions
{
    internal static class DbSetExtensions
    {
        internal static void AddOrUpdate<TInstance>(this DbSet<TInstance> dbSet, IKey keyInfo, params TInstance[] instances)
            where TInstance : class
        {
            foreach (var instance in instances)
            {
                var key = keyInfo.GetIdentity(instance);

                var v = dbSet.Find(key.ToArray());
                if (v != null)
                {
                    dbSet.Update(instance);
                }
                else
                {
                    dbSet.Add(instance);
                }
            }
        }
    }
}
#endif