﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Core.EF.Extensions
{
    internal static class ModelExtensions
    {
        internal static IKey GetPrimaryKey(this IModel model, Type currentType)
        {
            return model.FindEntityType(currentType)
                .FindPrimaryKey();
        }

        internal static IEnumerable<object> GetIdentity(this IKey keyInfo, object instance)
        {
            var list = keyInfo.Properties;

            var values = list.Select(property => property.GetGetter().GetClrValue(instance))
                .ToList();

            return values;
        }
    }
}

#endif