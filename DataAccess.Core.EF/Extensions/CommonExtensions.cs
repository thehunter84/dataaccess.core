﻿using System.Data;
using DataAccess.Core.Extensions;
#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DataAccess.Core.Exceptions;
using Microsoft.Data.SqlClient;
#else
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using DataAccess.Core.Exceptions;
#endif

namespace DataAccess.Core.EF.Extensions
{
    internal static class CommonExtensions
    {
        /// <summary>
        /// Normalizes the SQL statement replacing the standard prefix ":" to "@" prefix.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        internal static string NormalizeSqlStatement(this string statement, IEnumerable<IQueryArgument> parameters)
        {
            foreach (var arg in parameters)
            {
                statement = Regex.Replace(statement, $":{arg.Name}", $"@{arg.Name}", RegexOptions.IgnoreCase);
            }

            return statement;
        }

        /// <summary>
        /// Creates a Sql parameter starting to the given <see cref="IQueryArgument"/> instance, applying the given value if it's indicated.
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static object CreateSqlParameter(this IQueryArgument parameter, object value = null)
        {
            var sqlParameter = new SqlParameter
            {
                ParameterName = parameter.Name.AsSqlParameterName()
            };

            if (value == null)
            {
                sqlParameter.Value = DBNull.Value;
            }
            else
            {
                var data = value.AsDataTable();
                if (data != null)
                {
                    parameter.TypeName.ThrowException(string.IsNullOrWhiteSpace, s => new QueryParameterException("The current parameter is a complex data, so the type name of this instance cannot be empty or null.", parameter.Name));

                    sqlParameter.SqlDbType = SqlDbType.Structured;
                    sqlParameter.TypeName = parameter.TypeName;
                    sqlParameter.Value = data;
                }
                else
                {
                    sqlParameter.Value = value;
                }
            }

            return sqlParameter;
        }

#if NETSTANDARD2_0
        /// <summary>
        /// Set validation errors if the method SaveChanges of DbContext generates an exception.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="originalEx"></param>
        internal static void SetValidationErrors(this FlushFailedException ex, Exception originalEx)
        {
            // todo: try to implement this logic.
        }
#else
        /// <summary>
        /// Set validation errors if the method SaveChanges of DbContext generates an exception.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="originalEx"></param>
        internal static void SetValidationErrors(this FlushFailedException ex, Exception originalEx)
        {
            if (originalEx is DbEntityValidationException realEx)
            {
                ex.ValidationErrors = realEx.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
            }
        }
#endif
    }
}
