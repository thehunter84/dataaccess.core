﻿using System;

namespace DataAccess.Core.EF.Extensions
{
    /// <summary>
    /// Represents a DTO used to configure properties with related column.
    /// </summary>
    public class PropertyTypeColumn
    {
        /// <summary>
        /// Gets o Sets the index of this mapping
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or Sets Column name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the underlying type for this instance.
        /// </summary>
        public Type Type { get; set; }
    }
}