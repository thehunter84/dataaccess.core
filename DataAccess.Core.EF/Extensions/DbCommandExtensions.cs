﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Data.Common;
using DataAccess.Core.Exceptions;

namespace DataAccess.Core.EF.Extensions
{
    internal static class DbCommandExtensions
    {
        internal static void AddParameters(this DbCommand command, params object[] parameters)
        {
            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
        }

        internal static DbDataReader ExecuteCommand(this DbCommand command, params object[] parameters)
        {
            command.AddParameters(parameters);

            try
            {
                return command.ExecuteReader();
            }
            catch (Exception e)
            {
                throw new DataAccessException(e.Message, nameof(ExecuteCommand), e);
            }
        }

        internal static IEnumerable<TInstance> Materialize<TInstance>(this DbCommand command, params object[] parameters)
        {
            var dbReader = command.ExecuteCommand(parameters);

            return dbReader.Materialize<TInstance>();
        }
    }
}
#endif