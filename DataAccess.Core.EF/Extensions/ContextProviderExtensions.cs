﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
#else
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Reflection;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
#endif

namespace DataAccess.Core.EF.Extensions
{
    /// <summary>
    /// Custom extension class for <see cref="IContextProvider{DbContext}"/> instances.
    /// </summary>
    public static class ContextProviderExtensions
    {
        private static readonly MethodInfo DbSetGenericMethod;
        private static readonly Dictionary<Type, MethodInfo> DbSetGenericMethods;

        static ContextProviderExtensions()
        {
            DbSetGenericMethod =
                typeof (DbContext).GetMethods().First(info => info.Name.Equals("Set") && info.IsGenericMethod);

            DbSetGenericMethods = new Dictionary<Type, MethodInfo>();
        }

        /// <summary>
        /// Get a dynamic DbSet reference related to the given entity type.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns></returns>
        public static dynamic DynamicDbSet(this IContextProvider<DbContext> contextProvider, Type entityType)
        {
            var context = contextProvider.GetCurrentContext();

            var method = GetDbSetGenericMethod(entityType);
            // return context.Set(entityType);

            return method.Invoke(context, null);
        }

        /// <summary>
        /// Sets the specified context provider.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <param name="contextProvider">The context provider.</param>
        /// <returns></returns>
        public static DbSet<TInstance> Set<TInstance>(this IContextProvider<DbContext> contextProvider)
            where TInstance : class
        {
            return contextProvider.GetCurrentContext()
                .Set<TInstance>();
        }
        
        /// <summary>
        /// Calls the SaveChanges method if It exists a transaction in the current operation.
        /// </summary>
        /// <param name="contextProvider"></param>
        public static void FlushOnActiveTransaction(this IContextProvider<DbContext> contextProvider)
        {
            if (contextProvider != null && Transaction.Current.CanBeCompleted())
            {
                var context = contextProvider.GetCurrentContext();
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var exception = new FlushFailedException($"Error when the current flush action was synchronized with data source.", nameof(FlushOnActiveTransaction), ex);

                    exception.SetValidationErrors(ex);

                    throw exception;
                }
            }
        }

        /// <summary>
        /// Determines whether [is default value] [the specified key value].
        /// </summary>
        /// <param name="keyValue">The key value.</param>
        /// <returns></returns>
        // ReSharper disable once UnusedMember.Local
        private static bool IsDefaultValue(object keyValue)
        {
            return keyValue == null
                   || (keyValue.GetType().IsValueType
                       && Equals(Activator.CreateInstance(keyValue.GetType()), keyValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static MethodInfo GetDbSetGenericMethod(Type type)
        {
            if (DbSetGenericMethods.ContainsKey(type))
            {
                return DbSetGenericMethods[type];
            }
            else
            {
                var genericMethod = DbSetGenericMethod.MakeGenericMethod(type);
                DbSetGenericMethods.Add(type, genericMethod);

                return genericMethod;
            }
        }

#if NETSTANDARD2_0
        /// <summary>
        /// Executes the sql statement.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static int ExecuteStatement(this IContextProvider<DbContext> contextProvider, string sqlStatement, params object[] parameters)
        {
            return contextProvider.GetCurrentContext()
                .Database
                .ExecuteSqlRaw(sqlStatement, parameters);
                //.ExecuteSqlCommand(sqlStatement, parameters);
        }

        /// <summary>
        /// Makes the query.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static IEnumerable<TInstance> MakeQuery<TInstance>(this IContextProvider<DbContext> contextProvider, string sqlStatement, params object[] parameters)
        {
            var dbContext = contextProvider.GetCurrentContext();

            var conn = dbContext.Database.GetDbConnection();
            var wasClosed = conn.State == ConnectionState.Closed;
            
            using (var command = conn.CreateCommand())
            {
                command.CommandText = sqlStatement;
                
                try
                {
                    if (wasClosed)
                    {
                        conn.Open();
                    }

                    return command.Materialize<TInstance>(parameters);
                }
                catch (Exception e)
                {
                    throw new DataAccessException($"Error on materialize the current command, entity: {nameof(TInstance)}", nameof(MakeQuery), e);
                }
                finally
                {
                    if (wasClosed)
                    {
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the current model for the given contextProvider.
        /// </summary>
        /// <param name="contextProvider"></param>
        /// <returns>Returns the current IModel reference for the given instance.</returns>
        public static IModel GetModel(this IContextProvider<DbContext> contextProvider)
        {
            return contextProvider
                .GetCurrentContext()
                .Model;
        }

        /// <summary>
        /// Makes persistent the given instances into underlying storage.
        /// </summary>
        /// <param name="contextProvider"></param>
        /// <param name="instances"></param>
        public static void MakePersistent(this IContextProvider<DbContext> contextProvider, params object[] instances)
        {
            IDictionary<Type, dynamic> dbSets = new Dictionary<Type, dynamic>();

            foreach (var entity in instances)
            {
                var currentType = entity.GetType();
                var keyInfo = contextProvider
                    .GetModel()
                    .GetPrimaryKey(currentType);

                dynamic queryable;

                if (dbSets.ContainsKey(currentType))
                {
                    queryable = dbSets[currentType];
                }
                else
                {
                    queryable = contextProvider.DynamicDbSet(entity.GetType());
                    dbSets.Add(currentType, queryable);
                }

                DbSetExtensions.AddOrUpdate(queryable, keyInfo, entity as dynamic);
            }
        }

        /// <summary>
        /// Makes persistent the given instances into underlying storage.
        /// </summary>
        /// <typeparam name="TInstance"></typeparam>
        /// <param name="contextProvider"></param>
        /// <param name="instances"></param>
        public static void MakePersistent<TInstance>(this IContextProvider<DbContext> contextProvider, params TInstance[] instances)
            where TInstance : class
        {
            var queryable = contextProvider.Set<TInstance>();
            var keyInfo = contextProvider
                .GetModel()
                .GetPrimaryKey(typeof(TInstance));

            queryable.AddOrUpdate(keyInfo, instances);
        }
#else
        /// <summary>
        /// Executes the sql statement.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static int ExecuteStatement(this IContextProvider<DbContext> contextProvider, string sqlStatement, params object[] parameters)
        {
            return contextProvider.GetCurrentContext()
                .Database
                .ExecuteSqlCommand(sqlStatement, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        internal static IEnumerable<string> KeysPropertiesFor(this DbContext context, Type entityType)
        {
            entityType = ObjectContext.GetObjectType(entityType);

            var metadataWorkspace =
                ((IObjectContextAdapter)context).ObjectContext.MetadataWorkspace;

            var objectItemCollection =
                (ObjectItemCollection)metadataWorkspace.GetItemCollection(DataSpace.OSpace);

            var ospaceType = metadataWorkspace
                .GetItems<EntityType>(DataSpace.OSpace)
                .SingleOrDefault(t => objectItemCollection.GetClrType(t) == entityType);

            // ospaceType.DeclaredNavigationProperties

            if (ospaceType == null)
            {
                // ReSharper disable once LocalizableElement
                throw new ArgumentException($"The type '{entityType.Name}' is not mapped as an entity type.", nameof(entityType));
            }

            return ospaceType.KeyMembers.Select(k => k.Name);
        }

        /// <summary>
        /// Keys the values for.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public static object[] KeyValuesFor(this DbContext context, object entity)
        {
            var entry = context.Entry(entity);
            return context.KeysPropertiesFor(entity.GetType())
                .Select(k => entry.Property(k).CurrentValue)
                .ToArray();
        }

        /// <summary>
        /// Makes the query.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static DbRawSqlQuery<TInstance> MakeQuery<TInstance>(this IContextProvider<DbContext> contextProvider, string sqlStatement, params object[] parameters)
        {
            return contextProvider.GetCurrentContext()
                .Database
                .SqlQuery<TInstance>(sqlStatement, parameters);
        }

        /// <summary>
        /// Makes persistent the given instances into underlying storage.
        /// </summary>
        /// <param name="contextProvider"></param>
        /// <param name="instances"></param>
        public static void MakePersistent(this IContextProvider<DbContext> contextProvider, params object[] instances)
        {
            IDictionary<Type, dynamic> dbSets = new Dictionary<Type, dynamic>();

            foreach (var entity in instances)
            {
                var currentType = entity.GetType();
                dynamic queryable;

                if (dbSets.ContainsKey(currentType))
                {
                    queryable = dbSets[currentType];
                }
                else
                {
                    queryable = contextProvider.DynamicDbSet(entity.GetType());
                    dbSets.Add(currentType, queryable);
                }

                DbSetMigrationsExtensions.AddOrUpdate(queryable, entity as dynamic);
            }
        }

        /// <summary>
        /// Makes persistent the given instances into underlying storage.
        /// </summary>
        /// <typeparam name="TInstance"></typeparam>
        /// <param name="contextProvider"></param>
        /// <param name="instances"></param>
        public static void MakePersistent<TInstance>(this IContextProvider<DbContext> contextProvider, params TInstance[] instances)
            where TInstance : class
        {
            var queryable = contextProvider.Set<TInstance>();
            queryable.AddOrUpdate(instances);
        }

        /// <summary>
        /// Gets the object context.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        /// <returns></returns>
        public static ObjectContext GetObjectContext(this IContextProvider<DbContext> contextProvider)
        {
            return ((IObjectContextAdapter)contextProvider.GetCurrentContext()).ObjectContext;
        }

        /// <summary>
        /// Gets the object set.
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <param name="contextProvider">The context provider.</param>
        /// <returns></returns>
        public static ObjectSet<TInstance> GetObjectSet<TInstance>(this IContextProvider<DbContext> contextProvider)
            where TInstance : class
        {
            return contextProvider.GetObjectContext().CreateObjectSet<TInstance>();
        }
#endif
    }
}
