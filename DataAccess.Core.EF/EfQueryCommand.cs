﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Providers;
using Microsoft.EntityFrameworkCore;
#else
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Providers;
#endif

namespace DataAccess.Core.EF
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="EfQueryDescriptor" />
    /// <seealso cref="IQueryCommand" />
    public class EfQueryCommand
        : EfQueryDescriptor, IQueryCommand
    {
        private readonly IContextProvider<DbContext> contextProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="EfQueryCommand"/> class.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        /// <param name="name">The name.</param>
        /// <param name="statement">The statement.</param>
        /// <param name="parameters">The parameters.</param>
        public EfQueryCommand(IContextProvider<DbContext> contextProvider, string name, string statement, IEnumerable<IQueryArgument> parameters)
            : base(name, statement, parameters)
        {
            this.contextProvider = contextProvider;
        }

        /// <inheritdoc />
        public int Execute()
        {
            try
            {
                this.SetDefaultMissingParameters();
                
                var rowsAffected = this.contextProvider.ExecuteStatement(this.Statement, this.SqlParameters.Values.ToArray());
                this.SqlParameters.Clear();
                return rowsAffected;
            }
            catch (Exception)
            {
                this.SqlParameters.Clear();
                throw;
            }
        }
    }
}
