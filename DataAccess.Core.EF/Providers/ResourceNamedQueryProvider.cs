﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;

namespace DataAccess.Core.EF.Providers
{
    /// <summary>
    /// Represents a way to load named queries present in execution assembly as embedded resources.
    /// </summary>
    /// <seealso cref="INamedQueryProvider" />
    public class ResourceNamedQueryProvider
        : NamedQueryProvider
    {
        private const string TargetNameSpace = "urn:entityframework-queries-1.0";
        private const string ResourceNamePattern = @"\..*.ef.xml$";

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceNamedQueryProvider"/> class.
        /// </summary>
        /// <param name="namespaceResources">The namespace resources.</param>
        public ResourceNamedQueryProvider(string namespaceResources)
        {
            var @namespace = namespaceResources.Replace(".", @"\.");
            var pattern = @namespace + ResourceNamePattern;

            var assembly = Assembly.GetCallingAssembly();
            
            if (assembly != null)
            {
                var resources =
                    assembly.GetManifestResourceNames()
                        .Where(s => Regex.IsMatch(s, pattern, RegexOptions.CultureInvariant))
                        .ToList();
                
                using (var memory = new MemoryStream(Properties.Resources.ef_namedqueries))
                {
                    var schemaSet = new XmlSchemaSet();
                    schemaSet.Add(TargetNameSpace, new XmlTextReader(memory));

                    foreach (var resource in resources)
                    {
                        this.RegisterResource(assembly, resource, schemaSet);
                    }
                }
            }
        }

        /// <summary>
        /// Makes the query definition.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        private static QueryDefinition MakeQueryDefinition(XElement element)
        {
            var queryStatement = element.Value.Trim();

            var queryArguments =
                element.Descendants(XName.Get("sql-param", TargetNameSpace))
                    .Select(MakeQueryArgument)
                    .ToList();
            
            var queryDefinition = new QueryDefinition(
                element.Attribute("name")?.Value,
                queryStatement,
                Enum.Parse(typeof(QueryType), element.Attribute("type").Value, true) as dynamic,
                queryArguments
                );

            return queryDefinition;
        }

        private static IQueryArgument MakeQueryArgument(XElement element)
        {
            var name = element.Attribute("name");
            var clrtype = element.Attribute("clrtype");
            var typename = element.Attribute("typename");

            name.ThrowExceptionIfNull(() => new QueryParameterException("No [name] attribute was found into xml defintion.", "name"));
            clrtype.ThrowExceptionIfNull(() => new QueryParameterException("No [clrtype] attribute was found into xml defintion.", "clrtype"));
            
            // ReSharper disable once PossibleNullReferenceException
            return QueryArgument.MakeInstance(name.Value, clrtype.Value.GetTypeFrom(), typename?.Value);
        }

        /// <summary>
        /// Registers the resource.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="resourceName">Name of the resource.</param>
        /// <param name="schemaSet">The schema set.</param>
        /// <exception cref="DuplicateNameException">There's a named query definition with the given name</exception>
        /// <exception cref="InvalidQueryException">The stream resource which defines named queries cannot be null</exception>
        private void RegisterResource(Assembly assembly, string resourceName, XmlSchemaSet schemaSet)
        {
            try
            {
                using (var stream = assembly.GetManifestResourceStream(resourceName))
                {
                    stream.ThrowExceptionIfNull(() => new InvalidQueryException("The stream resource which defines named queries cannot be null."));

                    using (var reader = new XmlTextReader(stream))
                    {
                        var doc = XDocument.Load(reader);
                        
                        doc.Validate(schemaSet,
                            (sender, args) =>
                            {
                                if (args.Exception != null)
                                {
                                    throw args.Exception;
                                }
                            });

                        XNamespace ns = TargetNameSpace;

                        var sqlQueries =
                        doc.Descendants(ns + "sql-query").Select(MakeQueryDefinition).ToList();

                        foreach (var query in sqlQueries)
                        {
                            if (!this.NamedQueries.Add(query))
                            {
                                throw new DuplicateNameException($"There's a named query definition with the given name, {query.Name}, Resource: {resourceName}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidQueryException($"Error on loading the given resource (name: {resourceName}), see inner exception for details", ex);
            }
        }
    }
}
