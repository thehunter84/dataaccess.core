﻿#if NETSTANDARD2_0
using System;
using System.Transactions;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using Microsoft.EntityFrameworkCore;
#else
using System;
using System.Data.Entity;
using System.Transactions;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
#endif

namespace DataAccess.Core.EF.Providers
{
    /// <summary>
    /// A basic implementation for transaction provider.
    /// </summary>
    /// <seealso cref="TransactionProviderBase{TContext}" />
    public class EfTransactionProvider
        : TransactionProviderBase<DbContext>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EfTransactionProvider"/> class.
        /// </summary>
        /// <param name="contextProvider">The context provider.</param>
        public EfTransactionProvider(IContextProvider<DbContext> contextProvider) : base(contextProvider)
        {
        }
        
        /// <summary>
        /// Called when [build transaction worker].
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        protected override ITransactionWorker OnBuildTransactionWorker(TransactionDescriptor descriptor, DbContext context)
        {
            ITransactionWorker info;

            if (this.ExistsRootTransaction())
            {
                /* Inner Transaction */
                info = new TransactionWorkerImpl(descriptor,
                    // ReSharper disable once RedundantArgumentName
                    onBegin: (caller, desc) =>
                    {
                    },
                    // ReSharper disable once RedundantArgumentName
                    onCommit: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);

                        try
                        {
                            caller.Context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            var exception = new CommitFailedException($"Error when the current session tries to commit the current transaction (name: {desc.Name}).", "CommitTransaction", ex);

                            exception.SetValidationErrors(ex);

                            throw exception;
                        }
                    },
                    // ReSharper disable once RedundantArgumentName
                    onRollback: (caller, desc) =>
                    {
                        var tran = this.RemoveTransaction(desc.Name);

                        Transaction.Current?.Rollback();

                        throw new InnerRollBackException("An inner rollback transaction has occurred.", tran);
                    },
                    // ReSharper disable once RedundantArgumentName
                    onDispose: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);
                    },
                    // ReSharper disable once RedundantArgumentName
                    initializer: caller =>
                    {
                        caller.Context = context;
                    },
                    // ReSharper disable once RedundantArgumentNameForLiteralExpression
                    begin: false);
            }
            else
            {
                /* First transaction */
                info = new TransactionWorkerImpl(descriptor,
                    // ReSharper disable once RedundantArgumentName
                    onBegin: (caller, desc) =>
                    {
                        caller.Transaction = descriptor.AsTransactionScope();
                    },
                    // ReSharper disable once RedundantArgumentName
                    onCommit: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);

                        try
                        {
                            caller.Context.SaveChanges();

                            TransactionScope scope = caller.Transaction;
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            var exception = new CommitFailedException($"Error when the current session tries to commit the current transaction (name: {desc.Name}).", "CommitTransaction", ex);

                            exception.SetValidationErrors(ex);

                            throw exception;
                        }
                    },
                    // ReSharper disable once RedundantArgumentName
                    onRollback: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);
                    },
                    // ReSharper disable once RedundantArgumentName
                    onDispose: (caller, desc) =>
                    {
                        this.RemoveTransaction(desc.Name);

                        TransactionScope scope = caller.Transaction;
                        scope.Dispose();
                    },
                    // ReSharper disable once RedundantArgumentName
                    initializer: caller =>
                    {
                        caller.Context = context;
                    },
                    // ReSharper disable once RedundantArgumentNameForLiteralExpression
                    begin: false);
            }

            return info;
        }
    }
}
