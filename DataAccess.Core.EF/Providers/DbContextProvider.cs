﻿#if NETSTANDARD2_0
using System;
using DataAccess.Core.Providers;
using Microsoft.EntityFrameworkCore;
#else
using System;
using System.Data.Entity;
using DataAccess.Core.Providers;
#endif

namespace DataAccess.Core.EF.Providers
{
    /// <summary>
    /// IContextProvider for DbContext
    /// </summary>
    /// <seealso cref="IContextProvider{TContext}" />
    public class DbContextProvider
        : IContextProvider<DbContext>
    {
        private readonly Func<DbContext> dbContextFactory;
        private DbContext currentContext;
        private bool isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextProvider"/> class.
        /// </summary>
        /// <param name="dbContextFactory">The currentContext.</param>
        public DbContextProvider(Func<DbContext> dbContextFactory)
        {
            this.dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// Gets the current DbContext.
        /// </summary>
        /// <returns></returns>
        public DbContext GetCurrentContext()
        {
            this.EnsureNotDisposed();

            return this.currentContext ??= this.dbContextFactory();
        }

        /// <summary>
        /// Renews the current DbContext, disposing (if it's possible) the old one.
        /// </summary>
        public void RenewContext()
        {
            this.EnsureNotDisposed();

            try
            {
                this.currentContext?.Dispose();
            }
            catch
            {
                // ignored
            }

            this.currentContext = this.dbContextFactory();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.isDisposed)
                return;

            this.isDisposed = true;

            try
            {
                this.currentContext?.Dispose();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Ensures the not disposed.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">the current DbContextProvider was disposed.</exception>
        private void EnsureNotDisposed()
        {
            if (this.isDisposed)
                throw new ObjectDisposedException("the current DbContextProvider was disposed.");
        }
    }
}
