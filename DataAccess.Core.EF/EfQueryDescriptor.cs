﻿using System.Collections.Generic;
using System.Diagnostics;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;

namespace DataAccess.Core.EF
{
    /// <summary>
    /// Represents a basic implementation for query descriptors using SqlParameter instances.
    /// </summary>
    /// <seealso cref="QueryDescriptor" />
    [DebuggerDisplay("name: {this.Name}")]
    public class EfQueryDescriptor
        : QueryDescriptor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EfQueryDescriptor"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="statement">The statement.</param>
        /// <param name="queryArguments">The parameters.</param>
        protected EfQueryDescriptor(string name, string statement, IEnumerable<IQueryArgument> queryArguments)
            // ReSharper disable once PossibleMultipleEnumeration            
            : base(name, statement.NormalizeSqlStatement(queryArguments), queryArguments)
        {
            this.SqlParameters = new Dictionary<IQueryArgument, object>(new QueryArgumentEqualityComparer());
        }

        /// <inheritdoc/>
        public override IQueryDescriptor SetParameter(IQueryArgument parameter, object value)
        {
            parameter.ThrowExceptionIfNull(() => new QueryParameterException("The given parameter doesn't exists.", "SetParameter", "parameter"));
            
            if (parameter.ClrType != typeof(object))
            {
                value.ThrowException(par => !parameter.ClrType.IsInstanceOfType(par), o => new QueryParameterException("The given parameter is not compatible with the configuration type.", parameter.Name));
            }

            var sqlParameter = parameter.CreateSqlParameter(value);

            if (this.SqlParameters.ContainsKey(parameter))
            {
                this.SqlParameters[parameter] = sqlParameter;
            }
            else
            {
                this.SqlParameters.Add(parameter, sqlParameter);
            }

            return this;
        }

        /// <summary>
        /// Gets the SQL parameters.
        /// </summary>
        /// <value>
        /// The SQL parameters.
        /// </value>
        protected IDictionary<IQueryArgument, object> SqlParameters { get; }

        /// <summary>
        /// Sets the default missing parameters.
        /// </summary>
        protected void SetDefaultMissingParameters()
        {
            foreach (var parameter in this.Parameters)
            {
                if (!this.SqlParameters.ContainsKey(parameter))
                {
                    var sqlParameter = parameter.CreateSqlParameter();

                    this.SqlParameters.Add(parameter, sqlParameter);
                }
            }
        }
    }
}
