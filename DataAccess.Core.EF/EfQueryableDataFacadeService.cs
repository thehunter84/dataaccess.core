﻿#if NETSTANDARD2_0
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using Microsoft.EntityFrameworkCore;

#else
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using DataAccess.Core.EF.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
#endif

namespace DataAccess.Core.EF
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="EfQueryableDataFacade" />
    /// <seealso cref="IQueryableDataFacadeService" />
    public class EfQueryableDataFacadeService
        : EfQueryableDataFacade, IQueryableDataFacadeService
    {
        private readonly ITransactionProvider<DbContext> transactionProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="EfQueryableDataFacadeService"/> class.
        /// </summary>
        /// <param name="transactionProvider">The transaction provider.</param>
        public EfQueryableDataFacadeService(ITransactionProvider<DbContext> transactionProvider)
            : base(transactionProvider.ContextProvider)
        {
            this.transactionProvider = transactionProvider;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(string name, IsolationLevel? isolation = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(new TransactionDescriptor { Name = name, Isolation = isolation });
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(TransactionDescriptor descriptor = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(descriptor);
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public TEntity Insert<TEntity>(TEntity entity) where TEntity : class
        {
            this.ContextProvider.MakePersistent(entity);

            this.ContextProvider.FlushOnActiveTransaction();

            return entity;
        }

        /// <inheritdoc/>
        public void Insert<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            this.ContextProvider.MakePersistent(entities.ToArray());

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public object Insert(object entity)
        {
            this.ContextProvider.MakePersistent(entity);

            this.ContextProvider.FlushOnActiveTransaction();

            return entity;
        }

        /// <inheritdoc/>
        public void Insert(IEnumerable<object> entities)
        {
            this.ContextProvider.MakePersistent(entities.ToArray());

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public TEntity MakePersistent<TEntity>(TEntity entity) where TEntity : class
        {
            this.ContextProvider.MakePersistent(entity);

            this.ContextProvider.FlushOnActiveTransaction();

            return entity;
        }

        /// <inheritdoc/>
        public void MakePersistent<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            this.ContextProvider.MakePersistent(entities.ToArray());

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public object MakePersistent(object entity)
        {
            this.ContextProvider.MakePersistent(entity);

            this.ContextProvider.FlushOnActiveTransaction();

            return entity;
        }

        /// <inheritdoc/>
        public void MakePersistent(IEnumerable<object> entities)
        {
            this.ContextProvider.MakePersistent(entities.ToArray());

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public void MakeTransient<TEntity>(object identifier) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();
            var instance = this.Get<TEntity>(identifier);

            if (instance != null)
            {
                queryable.Remove(instance);
            }

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public TEntity MakeTransient<TEntity>(TEntity entity) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();

            queryable.Remove(entity);

            this.ContextProvider.FlushOnActiveTransaction();

            return entity;
        }

        /// <inheritdoc/>
        public void MakeTransient<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();
            
            queryable.RemoveRange(entities);

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public void MakeTransient<TEntity>(Expression<Func<TEntity, bool>> condition) where TEntity : class
        {
            var queryable = this.ContextProvider.Set<TEntity>();
            
            queryable.RemoveRange(queryable.Where(condition));

            this.ContextProvider.FlushOnActiveTransaction();
        }

        /// <inheritdoc/>
        public bool HasChanges()
        {
            var context = this.ContextProvider.GetCurrentContext();
            context.ChangeTracker.DetectChanges();

            return context.ChangeTracker.HasChanges();
        }

        /// <summary>
        /// Dirties the specified instances.
        /// </summary>
        /// <param name="instances">The instances.</param>
        /// <returns></returns>
        public bool Dirty(params object[] instances)
        {
            var context = this.ContextProvider.GetCurrentContext();
            context.ChangeTracker.DetectChanges();

            return instances.All(instance => context.Entry(instance).IsDirty());
        }

        /// <inheritdoc/>
        public bool Cached(params object[] instances)
        {
            var context = this.ContextProvider.GetCurrentContext();

            return instances.All(instance => context.Entry(instance).IsCached());
        }

        /// <inheritdoc/>
        public void Evict(params object[] instances)
        {
            var context = this.ContextProvider.GetCurrentContext();

            foreach (var instance in instances)
            {
                var entry = context.Entry(instance);
                
                entry.Detach();
            }
        }

        /// <inheritdoc/>
        public void Restore(params object[] instances)
        {
            var context = this.ContextProvider.GetCurrentContext();

            foreach (var instance in instances)
            {
                try
                {
                    var entry = context.Entry(instance);
                    entry.Restore();
                }
                catch (Exception)
                {
                    // nothing to do..
                }
            }
        }

        /// <inheritdoc/>
        public void Clear()
        {
            var context = this.ContextProvider.GetCurrentContext();
            var entries = context.ChangeTracker.Entries().ToList();

            foreach (var entry in entries)
            {
                entry.Detach();
            }
        }
    }
}
