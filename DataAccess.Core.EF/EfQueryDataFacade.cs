﻿#if NETSTANDARD2_0
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
using Microsoft.EntityFrameworkCore;
#else
using System.Data.Entity;
using System.Transactions;
using DataAccess.Core.Exceptions;
using DataAccess.Core.Extensions;
using DataAccess.Core.Providers;
using DataAccess.Core.Transactions;
#endif

namespace DataAccess.Core.EF
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IQueryDataFacade" />
    public class EfQueryDataFacade
         : IQueryDataFacade
    {
        private readonly INamedQueryProvider namedQueryProvider;
        private readonly ITransactionProvider<DbContext> transactionProvider;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="EfQueryDataFacade"/> class.
        /// </summary>
        /// <param name="transactionProvider">The transaction provider.</param>
        /// <param name="namedQueryProvider">The named query provider.</param>
        public EfQueryDataFacade(ITransactionProvider<DbContext> transactionProvider, INamedQueryProvider namedQueryProvider)
        {
            this.transactionProvider = transactionProvider;
            this.namedQueryProvider = namedQueryProvider;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(string name, IsolationLevel? isolation = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(new TransactionDescriptor { Name = name, Isolation = isolation });
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public ITransactionWorker BeginTransaction(TransactionDescriptor descriptor = null)
        {
            var transaction = this.transactionProvider.BuildTransaction(descriptor);
            transaction.Begin();
            return transaction;
        }

        /// <inheritdoc/>
        public IQueryCommand GetQueryCommand(string namedQuery)
        {
            var query = this.namedQueryProvider.GetNamedQuery(namedQuery);

            query.ThrowExceptionIfNull(() => new NoQueryDefinitionException(namedQuery, "No query command was found with the given query."));
            query.ThrowException(definition => definition.QueryType != QueryType.Command, definition => new NoQueryDefinitionException(namedQuery, "The given named query isn't a Command type."));
            
            return new EfQueryCommand(this.transactionProvider.ContextProvider, query.Name, query.Statement, query.Parameters);
        }

        /// <inheritdoc/>
        public IQueryMaterializer GetQueryMaterializer(string namedQuery)
        {
            var query = this.namedQueryProvider.GetNamedQuery(namedQuery);

            query.ThrowExceptionIfNull(() => new NoQueryDefinitionException(namedQuery, "No query materializer was found with the given name."));
            query.ThrowException(definition => definition.QueryType != QueryType.Materializer, definition => new NoQueryDefinitionException(namedQuery, "The given named query isn't a Materializer type."));
            
            return new EfQueryMaterializer(this.transactionProvider.ContextProvider, query.Name, query.Statement, query.Parameters);
        }
    }
}
